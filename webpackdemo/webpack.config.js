

// 定义webpack 的功能
module.exports = {
    mode:"development", //  development 开发环境   production 生产环境 
    entry:"./entry.js", // 入口文件  
    output:{   // 出口文件 
        filename:"bundle.js",
        path:__dirname,   // __dirname 根目录 
    },

    devtool:"source-map", // 方便在线调试文件  (泄露代码)

    // 模块定义 (JS/css/png)
    module:{
        rules:[  // 打包规则
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:['babel-loader']
            },
            {
                test:/\.css$/,
                use:["style-loader","css-loader"]
            },
            {
                test:/\.(scss|sass)$/,
                // Creates `style` nodes from JS strings  创建Node代码风格的样式JS字符串
                // Translates CSS into CommonJS   编译为 commonJS模块 
                // Compiles Sass to CSS    编译 scss 为 css 代码 
                use:['style-loader','css-loader','sass-loader']
            },
            {
                test:/\.less$/,
                use:['style-loader','css-loader','less-loader']
            },
        ]
    }
}