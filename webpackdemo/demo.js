
// commonJS 
// 所有的JS 文件就是默认是一个模块  
// 其他文件 PNG CSS 都需要 Loader 进行打包

// define 定义
// exports 暴露
// require  模块引入 

const word = "WH2201 - 人人早点拿 offer "


// 暴露2种 (ES5)

// 1. 直接暴露   只能默认暴露一个模块  
module.exports = word 


// // 2. 对象解构暴露
// exports.word  = word;