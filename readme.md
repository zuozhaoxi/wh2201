# WH2201    
# 毕业时间 6.17      

# Node + MongoDB
# Vue3.0 


# webpack 
# React 
# 小程序 


# 安装 Express  
# cnpm i koa-generator -g
# cnpm i express-generator -g
# express --version   
# express -e myexp   (新建项目)
# cd myexp 
# cnpm i  (安装项目所需的插件)
# npm start  (启动项目)


# bin 启动文件
# public 项目的静态文件库
# routes 路由模块
# views  视图页面  
# app.js 启动的主文件  




# 安装 vite 项目 

yarn create vite myvite --template vue
cd myvite 
cnpm i 
npm run dev 



# 项目启动
# Vue3.0 + Vite + Node + MongoDB + Express  (千锋成绩管理系统)
# 全栈项目  (前后端分离的项目)

# 登录注册 找回密码 修改密码
# 错误页面
# 起始页
# 主页  (头部 侧边栏  底部Foot)
# 首页
# 角色管理  (学员  讲师  管理员)
# 个人中心
# 成绩管理
# 班级管理
# 项目管理
# 学科管理 



# vue 3.0 

# setup    组合式api 封装业务代码  最终混入到 vue组件实例内部  this 

# ref       绑定一个变量  响应式   通过 xxx.value 去修改

# reactive   响应式数据类型   只能绑定对象和数组

# computed    设置计算属性

# watch(()=>)  监听值得主动改变
  
# toRefs   复制 reactive 里面的属性然后转成 ref，而且它既保留了响应式，也保留了引用 

# toRef   复制 reactive 里面的一个属性然后转成 ref，而且它既保留了响应式，也保留了引用 

# onMounted   组件初始化载入  

# refs 


# 网易云
# http://47.104.209.44:3333/ 




# webpack 
# 项目自动化管理 和 项目模块化开发  


# commonJS  模块化开发规范 
# AMD   requireJS
# CMD   seaJS  

# 实现 模块化开发  

# coffeeScript   CoffeeScript是一套JavaScript的转译语言   它是- JavaScript 的不那么铺张的小兄弟 
# 因为 CoffeeScript 会将类似 Ruby 语法的代码编译成 JavaScript，而且大部分结构都相似，但不同的是 CoffeeScript 拥有更严格的语法


# commonJS 规范
# 模块定义  define       define 
# 模块引入  require      import 
# 模板暴露  exports      export 


# 反向安装     
# npm install -g cnpm --registry=https://registry.npm.taobao.org   (cnpm)


# rimraf  node_modules 
# cnpm i 


# vue.config.js

# vite.config.js 

# ES6 模块化要配置 loader  babel 

# EcmaScript11

# 高级浏览器 内置 babel 语法插件  转译 ES6 为 ES5 

# cnpm i style-loader css-loader -D 

# cnpm  i less less-loader -D 

# cnpm i sass sass-loader -D

# cnpm i source-map -D 

# cnpm i babel-loader@7 babel-core@6 babel-preset-env -D

# cnpm i webpack-cli -D


# mycli 手动配置脚手架

# 开发 React 代码 

# cnpm i html-webpack-plugin -D 

# cnpm i webpack-open-browser-plugin -D

# cnpm i url-loader file-loader -D

# cnpm i mini-css-extract-plugin style-loader css-loader sass sass-loader less less-loader -D

# cnpm i react react-dom -S 

# cnpm i babel-preset-stage-0 -D

# cnpm i prop-types -D 

# cnpm i swiper@4 -S

# cnpm i babel-plugin-transform-runtime -D 

# cnpm i react-router-dom -S

# cnpm i redux-thunk redux-promise -S 

# cnpm install babel-plugin-transform-decorators-legacy --save-dev

# cnpm i immutable redux-immutable -S

# cnpm i mobx@5 mobx-react@6 -S


# react
# react 有几层境界
# 1 纯 react UI + API
# 2 redux(vuex)
# 3 react-redux + immutable
# 4 mobx + immutable
# 5 hooks (纯函数写代码)
# 6 hooks + mobx + react-redux + immutable
# 7 hooks + mobx + react-redux + immutable + typscript + dva + umi
# 8 升天...





# 安装react 脚手架

cnpm i typescript -g

安装脚手架
# create-react-app  
# https://create-react-app.bootcss.com/
cnpm i create-react-app -g     
create-react-app --version    
create-react-app react-app 安装项目   npx create-react-app@4.0.3 react-app
cd react-app     
npm start    
启动项目 http://localhost:3000/

npx create-react-app my-app --template typescript
npx create-react-app react-app 
create-react-app react-app --template typescript
npx create-react-app react-app --template typescript
# dva (https://dvajs.com/guide/getting-started.html)
cnpm install dva-cli -g 
dva -v 
dva new dva-app 
cd dva-app 
npm start
http://localhost:8000/

# umi (https://umijs.org/zh-CN/docs/getting-started)
mkdir umiapp 
cd umiapp 
npx @umijs/create-umi-app
yarn create @umijs/umi-app
cnpm i   反向安装 
npm start

# vite - react 
yarn create @vitejs/app
yarn create vite myvite --template react
yarn create vite myapp --template react-ts
cd myapp
cnpm i
npm start 



# 网易适配
# 等比缩放 得到不同屏幕的rem 
# 网易适配  750px 1rem = 100px
# 计算出所有的手机设备里面 1rem = ?
# 750px:100vw =  100px: ?   ==> 当前设备的rem =  100vw/7.5 
# 网易适配 假设了1rem = 100px (好计算)


# 淘宝适配
# lib-flexible 
# cnpm i  lib-flexible  -S
# 1. 淘宝适配把屏幕分成100等分 1rem = 10等分  1rem=10vw  (宽度的1/10)
# 2. 淘宝适配会根据屏幕的 dpr (设备像素密度比) 缩放viewport  (注释meta )


1. 把项目里面所有的 localhost 替换成 公网IP
2. router 设置成 mode:hash 
3. 设置 打包相对路径  publicPath:"",    base:"./"
4. 测试文案 全部去掉  
5. 打包   npm run build
6. 本地测试
7. 线上测试
8. 正式发布  



# SVN 
# SVN checkout 检出仓库 
# URL of repository 远程仓库地址   
# checkout directory  本地文件地址 

# GIT 
# SVN 
# 仓库链接
# 用户名 
# 密码 

# 右键 SVN checkout 
# 右键 svn update 拉取远程代码    pull 
# 右键 svn commit 提交代码到远程仓库   push  


# GIT  
# 仓库链接
# 用户名 
# 密码 

# git clone  
# 配置 SSH 秘钥  

# 多人协作  
# git pull 
# git add ./
# git commit -m "ds"
# git push origin master

# git branch 
# git branch zkl
# git merge 

# GIT  冲突 
# GIT  常用指令  


# 面试题

# 原生

# Vue 

# React

# 小程序

# 简历 (务必一整天时间去处理的)

# 毕业人人18K 早点拿offer 





# 原生 
1. JS有哪些基本类型 哪些引用数据类型 
2. JS 里面 null 和 undefined 的区别 
3. 如何判断 基本类型  引用数据 数组  原型  (方法)  typeOf instanceOf isArray  __proto__
4. JS 宿主对象  本地对象 内置对象(Math ) 分别有哪些?   
5. JS 线程和进程的区别 如何启动多个线程  如何关闭线程  
6. TCP 和 UDP 的区别 
7. 解释一下什么是TCP的三次握手 (TCP四次握手)
# 8. JS 原型和原型链的区别 
# 9. 什么是 强缓存 弱缓存 协商缓存   (性能优化)
10. 图片懒加载实现原理  (代码)
11. 浏览器从输入 url 到页面显示  这一系列发生的哪些事情 
12. Ajax 请求数据的流程  (代码)
# 13. Http 状态码有哪些 (10条)
14. 跨域怎么生成的 如何解决跨域
# 15. 谈一下 Promise的理解  (有几种状态     race和all的区别 )
16. async + await 的实现原理 和 promise 的区别 
17. 什么是闭包 闭包的意义  
18. ES6 有哪些新特性 (10条)
19. 函数的防抖和节流的概念 (代码封装实现)
20. 什么是 深浅拷贝  深浅监听 深浅复制 
21. 实现深拷贝的方法 有哪些 (5种)
# 22. 箭头函数和普通函数的区别  和 this 的几种指向 
# 23. 什么是作用域链 什么是函数作用域 
# 24. 前端性能如何实现 (10条)
25. 事件里面 有 DOM0 DOM1 DOM2 三者区别 再说事件委托 
26. for 和 forEach map 循环数组的区别
27. ES6  map 和 set 数据类型 的区别 
28. 数组去重的3种方式  (代码)
29. 数组排序的3种方式  (代码)
30. 什么是变量提升
31. 0.1 + 0.2 = ? 为什么  怎么解决
32. 什么是 长链接 短连接 长轮询 短轮询
33. 什么是事件循环 (事件轮询)
34. 如何实现继承 (3种)
35. apply call bind 三者的区别
36. 什么是面向对象编程  什么是面向过程
37. 什么是函数柯里化 和 函数反柯里化
38. 开发常用的设计模式有哪些 (5种)



# Vue 

1. Vue 用哪些常用的指令  有什么作用
# 2. v-if 和 v-show 的区别 
3. Vue 如何自定义指令和自定义过滤器
4. Vue.use() 作用和原理是什么 
5. Vue 实例里面有哪些实例方法 和 实例属性 (6条)
# 6. watch 和 computed 的区别
7. Vue 里面的虚拟DOM 和 diff算法 
# 8. Vue 组件生命周期 (所有的 10条)
9. 父子组件生命周期的执行顺序 
10. v-model 的原理 
11. vue 里面  render 函数的理解  
12. vue 父子组件通信 有哪些
13. vue 组件里面有几种模式的插槽 
14. Vue 组件动态切换的方式有哪些
15. 如何在组件使用 v-model   <mybox v-model="show" >
16. Vue 路由基本的配置流程  
# 17. Vue 路由传参的方式有哪些 如何获取参数 
# 18. Vue 有哪些路由守卫  说2个具体使用场景
19. 路由里面的如何配置动态路由的添加 (addRoutes)
20. Vue 里面如何强制刷新一个 组件或者一个路由页面
# 21. Vue v-for 为什么必须绑定key 而且key!=index 
22. Vue.nextTick 的作用 
# 23. 谈一下你对 mvc  和 mvvm 的理解 
24. Vue组件的data为什么必须是一个函数 必须有返回值 
# 25. vuex的原理
26. vuex 和 pinia 的区别  
27. vuex 常用的4个辅助函数 是什么  以及  vuex模块化开发如何实现
28. 谈一下你对 Vue.extend 的理解 
29. Vue 你对 混入 mixins 的理解 
30. Vue  keep-alive 的具体作用 
# 31. Vue 和  React 的区别 
32. Vue 如何监听路由切换 
33. Vue 项目里面配置代理的 和如何实现适配  devServer ==> proxy 
34. Vue.set 和 Vue.delete 的作用   this.$set  this.$delete 
# 35. Vue 项目的 打包上线的流程 
# 36. Vue 项目遇到的 bug难点细节 (微信群) (3条)
# 37. Vue3 和  Vue2 的区别 

# 38.  谈一下你对 Vue3 的 setup函数的理解
# 39.  toRef 和 toRefs 的区别 
# 40.  reactive的具体作用
# 41.  Vue3.0 常用的组件生命周期 有哪些 (4条)

