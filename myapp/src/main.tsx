import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'

// 1.0 全局的CSS 
import "./styles/index.scss"

// 2. 淘宝适配 
import "lib-flexible" 

// 3. mobx 
import {Provider as MboxProvider} from 'mobx-react'
import { mStore } from './mobx/mStore'

// 4.redux 
import {Provider as ReduxProvider} from "react-redux"
import { store } from './redux/store';


// 严格模式去掉 
// StrictMode  严格模式  useEffect 执行2次 
ReactDOM.createRoot(document.getElementById('root')!).render(
  

 
    <ReduxProvider store={store}>
         <MboxProvider {...mStore}>
          <App />
        </MboxProvider>
    </ReduxProvider>



    
 
)
