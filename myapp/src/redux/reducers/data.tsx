
import { fromJS } from 'immutable';



const initialState = fromJS({
    count:1,
    userInfo:null,
    goodList:[],
    goodType:[],
    myLikes:[],  
    myCollects:[],
    myCart:[],
    cityList:[]
})

export const data = (state:any=initialState,action:any  )=>{
    const {type,payload} = action;
    console.log(action) 


    switch(type){

        case "getCityListAsync":
            return state.set('cityList',payload)
            break;

        case "getGoodTypeAsync":
            return state.set('goodType',payload)
            break;

        case "getGoodListAsync":
            return state.set('goodList',payload)
            break;

        case "getuserInfoAsync":
            return state.set('userInfo',payload)
            break;

        case "getMyLikes":
            return state.set('myLikes',payload)
            break;

        case "getMyCollects":
            return state.set('myCollects',payload)
            break;

        case "getMyCart":
            return  state.set('myCart',payload)
            break;
            
            
        default:
            console.log(state)
            return state;
            break;
    }
}
