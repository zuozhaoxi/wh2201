
import { Http } from '../../api/index';

export const getuserInfoAsync = async (payload?:any,cb?:any)=>{
    let res:any  = await Http.getuserinfo(payload)
    cb(res.result)
    return {
        type:"getuserInfoAsync",
        payload:res.result
    }
    
}



export const getGoodListAsync = async (payload?:any)=>{
    let res:any  = await Http.getgoodlist(payload)
    return {
        type:"getGoodListAsync",
        payload:res.result
    }
}


export const getGoodTypeAsync = async (payload?:any)=>{
    let res:any  = await Http.getgoodtype(payload)
    return {
        type:"getGoodTypeAsync",
        payload:res.result
    }
}


export const getMyLikes = async (payload?:any)=>{
    let res:any  = await Http.getlikes(payload)
    return {
        type:"getMyLikes",
        payload:res.result
    }
}

export const getMyCollects = async (payload?:any)=>{
    let res:any  = await Http.getcollect(payload)
    return {
        type:"getMyCollects",
        payload:res.result
    }
}


export const getMyCart = async (payload?:any)=>{
    let res:any  = await Http.getcart(payload)
    return {
        type:"getMyCart",
        payload:res.result
    }
}


export const getCityListAsync = async (payload?:any,headers?:any)=>{
    let res:any  = await Http.getMaiZuoData(payload,headers)
    return {
        type:"getCityListAsync",
        payload:res.data.cities 
    }
}



// useContext 
// useReducers 

