import { action, observable,makeObservable } from "mobx";


class Info{

    constructor(){
        makeObservable(this)
    }

    @observable userInfo:any  = null;
    @observable count:number = 1000;

    @action changeUserInfo =  (payload:any) =>{
        this.userInfo = payload; 
    }

    @action changeCount = (payload:number)=>{
        console.log('123123')
        this.count += payload;
        console.log(this.count)
    }
}

export default new Info()