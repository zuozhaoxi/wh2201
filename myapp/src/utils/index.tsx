//判断当前日期为当年第几周
export const  getYearWeek = function () {
    var time = new Date();
    var a = time.getFullYear()
    var b:any = time.getMonth() + 1;
    var c = time.getDate()
    
    //date1是当前日期
    //date2是当年第一天
    //d是当前日期是今年第多少天
    //用d + 当前年的第一天的周差距的和在除以7就是本年第几周
    var date1 = new Date(a, parseInt(b) - 1, c), date2 = new Date(a, 0, 1),
        d = Math.round((date1.valueOf() - date2.valueOf()) / 86400000);   // 24 * 3600  * 7 * 1000 
    return Math.ceil((d + ((date2.getDay() + 1) - 1)) / 7);
};
