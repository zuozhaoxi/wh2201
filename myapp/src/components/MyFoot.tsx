

import React, { FC } from 'react'
import "./foot.scss"
import { NavLink, useLocation, useNavigate } from 'react-router-dom';
import {Tag,TabBar} from 'antd-mobile'
import {connect} from 'react-redux';
import {
    AppOutline,
    MessageOutline,
    MessageFill,
    UnorderedListOutline,
    UserOutline,
    TeamOutline,
    AppstoreOutline,
    TruckOutline,
    HistogramOutline
  } from 'antd-mobile-icons'
export const ftlist = [
    {path:'/main/home',icon:"icon-home2",text:"首页",node:<HistogramOutline />},
    {path:'/main/classify',icon:"icon-gengduo",text:"分类",node:<AppstoreOutline />},
    {path:'/main/cart',icon:"icon-shangcheng",text:"购物车",hot:true,node:<TruckOutline />,  badge: '18',},
    {path:'/main/mine',icon:"icon-wode",text:"我的",node:<UserOutline />},
]

export const MyFoot:FC<any> = () => {
    const location = useLocation()
    return (
        <footer >
            {
                ftlist.map((l,i)=>(
                    <div key={i}>
                        <NavLink to={l.path}>
                            <i className={'iconfont ' + l.icon} ></i>
                            <span>{l.text}</span>
                            {
                                l.hot &&  <Tag color={location.pathname==l.path?'#ff5f16':'#999'} className='hot' > {8} </Tag>
                            }
                        </NavLink>
                    </div>
                ))
            }
        </footer>
    )
}



const AntFootOld:FC<any> = ({
    myCart
})=>{
    const myftlist = [
        {path:'/main/home',icon:"icon-home2",text:"首页",node:<HistogramOutline />},
        {path:'/main/classify',icon:"icon-gengduo",text:"分类",node:<AppstoreOutline />},
        {path:'/main/cart',icon:"icon-shangcheng",text:"购物车",hot:true,node:<TruckOutline />,  badge: myCart.length,},
        {path:'/main/mine',icon:"icon-wode",text:"我的",node:<UserOutline />},
    ]
    const navigate = useNavigate()
    const {pathname} = useLocation()
    const changeRoute = (path:any)=>{
        // console.log(path)
        navigate(path)
    }
    return (
        <div className="footer" style={{background:"#fff"}}>
            <TabBar safeArea onChange={changeRoute} activeKey={pathname}>
                {
                    myftlist.map((l:any,i:any)=>(
                        <TabBar.Item
                            key={l.path}
                            title={l.text}
                            icon={l.node}
                            style={{color:pathname==l.path?"#ff5f16":"#999"}}
                            badge={l.badge}
                        />
                    ))
                }
            </TabBar>
        </div>
    )
}

export const AntFoot = connect(
    (state:any)=>{
        return {
            myCart:state.getIn(['data','myCart'])
        }
    }
)(AntFootOld)