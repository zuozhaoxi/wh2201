



import React, { Component, FC } from 'react';
// 1.下载引入
import Swiper from "swiper"   
import PropTypes from 'prop-types';
import { useEffect } from 'react';



export const MySwipe:FC<any> = ({id,children,options})=>{
    useEffect(()=>{
        // 3. 实例化swiper
        let mySwiper = new Swiper("#"+id,{
            ...options, 
            // 默认的参数 
            observer:true, // 动态的刷新轮播 解决异步的BUG
        })
    },[])
    return (
        <div className='swiper-container' id={id}  >
            <div className='swiper-wrapper'>
                {children}
            </div>  
        </div>
    )
}

MySwipe.defaultProps = {
    id:"banner",
    options:{}
}

MySwipe.propTypes = {
    id:PropTypes.string,
    options:PropTypes.object 
}

// 静态属性 
export const MySwipeItem:FC<any> =({children})=>{
    return (
        <div  className='swiper-slide'>
            {children}
        </div>
    )
}

