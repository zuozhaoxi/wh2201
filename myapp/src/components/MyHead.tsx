


import React, {FC} from 'react'
import { NavBar, Space, Toast, Popover } from 'antd-mobile'
import { SearchOutline, MoreOutline, CloseOutline , LeftOutline } from 'antd-mobile-icons'
import { useNavigate, useLocation } from 'react-router-dom';
import {
    AntOutline,
    HandPayCircleOutline,
    ScanningOutline,
    TransportQRcodeOutline,
    UserOutline,
    TeamOutline
  } from 'antd-mobile-icons'

const MyHead:FC<{
    title?:any,
    back?:boolean,
    children?:any,
}> = ({
    title,
    back,
    children
}) => {
    const navigate = useNavigate()
    const {pathname} = useLocation()
    const actions: any[] = [
        { key: 'scan', icon: <ScanningOutline />, text: '扫一扫',disabled:true  },
        { key: 'payment', icon: <HandPayCircleOutline />, text: '付钱/收钱' , disabled:true},
        { key: '/main/home', icon: <TransportQRcodeOutline />, text: '首页',disabled:pathname=='/main/home' },
        { key: '/city', icon: <TeamOutline />, text: '城市定位',disabled:pathname=='/city'},
        { key: '/main/mine', icon: <UserOutline />, text: '个人中心',disabled:pathname=='/main/mine'},
        { key: '/login', icon: <AntOutline />, text: '登录',disabled:pathname=='/login' },
      ]
    
    // console.log(pathname)
    const actionClick = (node:any)=>{
        // console.log(node)
        navigate(node.key)
    }
    const HeadRight =()=>{
        return (
            <div  style={{ fontSize: 24 }}>
                <Space    style={{ '--gap': '32px' }}>
                    {
                       pathname !='/search' && <SearchOutline fontSize={60}    onClick={()=>navigate('/search')}   />
                    }
                    <Popover.Menu
                        placement='bottom-start'
                        trigger='click'
                        mode='light'
                        actions={actions}
                        onAction={actionClick}
                    >
                        <MoreOutline fontSize={60}  />
                    </Popover.Menu>
                </Space>
          </div>
        )
    }
    
    const HeadLeft =()=>{
        return (
            back?<div onClick={()=>navigate(-1)}>
            <LeftOutline /> 返回 
        </div>:null 
        )
    }
  return (
        <NavBar 
            style={{
            '--border-bottom': '1px #ccc solid',
          }}    
          right={HeadRight()} 
          left={HeadLeft()} 
          back={null}
        >
            {title }
        </NavBar>
  )
}
MyHead.defaultProps={
    title:'标题',
    back:true,
}

export default MyHead