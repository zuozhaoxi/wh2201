import {get,axios,post} from './request'

export const Http = {
    testApi:(data?:any)=>get("/api/test",data),

    getBanner:(data?:any)=>get("http://47.104.209.44:3333/banner",data),

    register:(data?:any)=>post("/api/register",data),

    login:(data?:any)=>post("/api/login",data),

    sentcaptcha:(data?:any)=>post("/api/sentcaptcha",data),

    verifycaptcha:(data?:any)=>post("/api/verifycaptcha",data),

    getuserinfo:(data?:any)=>post("/api/getuserinfo",data),

    uploadfiles:(data?:any)=>post("/api/uploadfiles",data),

    changeuserinfo:(data?:any)=>post("/api/changeuserinfo",data),

    getgoodlist:(data?:any)=>post("/api/getgoodlist",data),

    getgoodlistlimit:(data?:any)=>post("/api/getgoodlistlimit",data),

    getgoodtype:(data?:any)=>post("/api/getgoodtype",data),

    getgoodone:(data?:any)=>post("/api/getgoodone",data),

    // 点赞

    getlikes:(data?:any)=>post("/api/getlikes",data),

    addlikes:(data?:any)=>post("/api/addlikes",data),

    dellikes:(data?:any)=>post("/api/dellikes",data),


    // 收藏
    getcollect:(data?:any)=>post("/api/getcollect",data),

    addcollect:(data?:any)=>post("/api/addcollect",data),

    delcollect:(data?:any)=>post("/api/delcollect",data),

    // 购物车
    getcart:(data?:any)=>post("/api/getcart",data),

    addcart:(data?:any)=>post("/api/addcart",data),

    setcart:(data?:any)=>post("/api/setcart",data),

    // 卖座接口
    getMaiZuo:(data?:any,headers?:any)=>get("https://m.maizuo.com/gateway",data,headers),

    getMaiZuoData:(data?:any,headers?:any)=>get("/api/getMaiZuo",data,headers),

}