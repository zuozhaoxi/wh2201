import React, { FC, useEffect, useState } from 'react'
import MyHead from '../../components/MyHead'
import { inject, observer } from 'mobx-react';
import { Http } from '../../api';
import { Button, Empty, Space, Swiper, Toast, PullToRefresh, List , InfiniteScroll  } from 'antd-mobile'
import GList from '../Search/GList';
import { QuestionCircleOutline } from 'antd-mobile-icons'
import { PullStatus } from 'antd-mobile/es/components/pull-to-refresh'
import { sleep } from 'antd-mobile/es/utils/sleep'
const Home: FC<any> = ({
  info
}) => {
  const { count, changeCount } = info
  const [banner, setBanner] = useState<any>([])
  let  [list, setList] = useState<any>([])
  const [total, setTotal] = useState<any>([])
  const [page, setPage] = useState<any>(1)
  const [pageSize, setPageSize] = useState<any>(10)
  const [hasMore,setHasMore] = useState<any>(true)


  const statusRecord: Record<PullStatus, string> = {
    pulling: '用力拉',
    canRelease: '松开吧',
    refreshing: '玩命加载中...',
    complete: '刷新成功',
  }

  const getNewBanner = async () => {
    let res: any = await Http.getBanner()
    setBanner(res.banners)
  }
  const getGoodList = async () => {
    let res: any = await Http.getgoodlistlimit({
      page,
      pageSize,
    })
    setList(res.result)
    setTotal(res.total)
    setPage(page+1)   // 下一页数据 
  }
  useEffect(() => {
    getNewBanner()
    getGoodList()

  }, [])

  // 下拉刷新
  const onRefresh = async () => {
    
    let res: any = await Http.getgoodlistlimit({
      page:3,
      pageSize,
    })
    await sleep(1000)
    setList([...res.result])
    setTotal(res.total)
    setPage(3);
  }
  // 加载更多 
  const loadMore = async ()=>{
    console.log('loadmore')
    // setPage(page+1)   // 异步 
    console.log(page)
    if(page> Math.ceil(total/pageSize)){
      Toast.show({
        content:"没有更多数据了"
      })
      setHasMore(false)
    }else{
      let res: any = await Http.getgoodlistlimit({
        page,
        pageSize,
      })
      setHasMore(true)
      list =  list.concat(res.result) 
      setList([...list])  
      setTotal(res.total)
      setPage(page+1)   // 下一页数据 
      Toast.show({
        content:"加载成功"
      })
    }
  }
  return (
    <div className="subpage">
      <MyHead title="首页" back={false} />
      <Swiper autoplay loop>
        {
          banner.map((item: any, index: number) => {
            return (
              <Swiper.Item key={index}>
                <img src={item.imageUrl} style={{ width: '100%', height: 500 }} alt="" />
              </Swiper.Item>
            )
          })
        }
      </Swiper>
      {
        list.length > 0 ?
          <div>
            <PullToRefresh
              renderText={status => {
                return <div>{statusRecord[status]}</div>
              }} 
              onRefresh={onRefresh}

            >
              <GList list={list} />
              <InfiniteScroll loadMore={loadMore} hasMore={hasMore} />
            </PullToRefresh>

          </div>
          : <Empty
            style={{ padding: '64px 0' }}
            image={
              <QuestionCircleOutline
                style={{
                  color: 'var(--adm-color-light)',
                  fontSize: 48,
                }}
              />
            }
            description='没有商品数据' />
      }
    </div>
  )
}

export default inject('info')(observer(Home))