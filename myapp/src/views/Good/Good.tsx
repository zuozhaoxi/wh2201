

import React, { FC, useEffect, useState } from 'react'
import { useNavigate, useParams, useSearchParams } from 'react-router-dom'
import { Http } from '../../api'
import MyHead from '../../components/MyHead'
import {List,Stepper,CapsuleTabs,Modal ,Toast} from "antd-mobile"
import "./index.scss"
import { connect } from 'react-redux';



const Good:FC<{
    userInfo:any 
}> =  ({
    userInfo
}) => {
    const  params = useParams()
    const  [searchParams] = useSearchParams()
    const [data,setData] = useState<any>(null)
    let  [count,setCount] = useState<number>(1)
    let  [likeNum,setLikeNum] = useState<number>(0)
    let  [hasLike,sethasLike] = useState<boolean>(false)

    let  [collectNum,setCollectNum] = useState<number>(0)
    let  [hasCollect,setHasCollect] = useState<boolean>(false)

    const router = useNavigate()
    const getGoodData = async ()=>{
        let res:any  = await Http.getgoodone({
            _id:params.goodId,
        })
        setData(res.result)
    }
    const checkIsLogin = (next?:any)=>{
        if(userInfo){
            next()
        }else{
            Modal.confirm({
                content:"请你先登录哦!",
                title:"提示",
                onConfirm:()=>{
                    router("/login")
                }
            })
        }
    }

    // 加入购物车 
    const addToCart = ()=>{
        checkIsLogin(async ()=>{
            // 没有登录需要去登录
            // 你是否购过买 
            let res:any = await Http.getcart({
                goodId:params.goodId,
                phone:userInfo.phone 
            })
            if(res.result.length>0){
                // 购买过
                // count 
                let value:any  = await Http.setcart({
                    _id:res.result[0]._id,
                    count:res.result[0].count + count 
                })
                if(value.code==200){
                    Toast.show({
                        content:"再次购买成功"
                    })
                }
            }else{
                let value:any  = await Http.addcart({
                    goodId:params.goodId,
                    phone:userInfo.phone,
                    good:data,
                    count:count 
                })
                if(value.code==200){
                    Toast.show({
                        content:"新增购物车成功"
                    })
                }
            }
        })
    }
    const todoLikeAction = ()=>{
        checkIsLogin(async ()=>{
            // 开始点赞逻辑 
            // hasLike true 已经点赞
            // hasLike false 没有点赞 
            if(hasLike){
                // 取消
                let res:any  = await Http.dellikes({
                    goodId:params.goodId,
                    phone:userInfo.phone,
                })
                if(res.code==200){
                    Toast.show({
                        content:"取消点赞了"
                    })
                    sethasLike(false)
                    setLikeNum(likeNum-1);
                }
            }else{
                // 直接点赞 
                let res:any  = await Http.addlikes({
                    goodId:params.goodId,
                    phone:userInfo.phone,
                    good:data 
                })
                if(res.code==200){
                    Toast.show({
                        content:"点赞成功"
                    })
                    sethasLike(true)
                    setLikeNum(likeNum+1);
                }
            }
        })
    }

    const todoCollectAction = ()=>{
        checkIsLogin(async()=>{
            if(hasCollect){
                // 已经收藏
                // 取消
                let res:any  = await Http.delcollect({
                    goodId:params.goodId,
                    phone:userInfo.phone,
                })
                if(res.code==200){
                    Toast.show({
                        content:"取消收藏了"
                    })
                    setHasCollect(false)
                    setCollectNum(collectNum-1);
                }
            }else{
                // 直接收藏
                let res:any  = await Http.addcollect({
                    goodId:params.goodId,
                    phone:userInfo.phone,
                    good:data 
                })
                if(res.code==200){
                    Toast.show({
                        content:"收藏成功"
                    })
                    setHasCollect(true)
                    setCollectNum(collectNum+1);
                }
            }
        })
    }

    // 这个商品点赞次数
    const getLikeCounts = async ()=>{
        let  res:any  = await Http.getlikes({
            goodId:params.goodId,
        })
        setLikeNum(res.result.length)
    }

    // 是否已经点赞
    const getHasLike = async ()=>{
        let  res:any  = await Http.getlikes({
            goodId:params.goodId,
            phone:userInfo.phone 
        })
        sethasLike(!!res.result.length)
    }   

    // 这个商品被收藏次数
    const getCollectCounts = async ()=>{
        let  res:any  = await Http.getcollect({
            goodId:params.goodId,
        })
        setCollectNum(res.result.length)
    }

    // 本人是否已经收藏
    const getHasCollect = async ()=>{
        let  res:any  = await Http.getcollect({
            goodId:params.goodId,
            phone:userInfo.phone 
        })
        setHasCollect(!!res.result.length)
    }   

    useEffect(()=>{
        getGoodData()
        getLikeCounts()
        getCollectCounts()

        // 已经登录
        if(userInfo){
            getHasLike()
            getHasCollect()
        }
    },[])
    return (
        <div className='good'>
            <MyHead title={searchParams.get('name')} />
            {
                data && (
                    <div>
                       <img style={{width:'100%',height:'6rem'}} src={data.img}  alt="" /> 

                       <div className="gbox" style={{padding:40}}>
                            <List mode="card">
                                <List.Item  extra={data.price}   >
                                    商品单价
                                </List.Item>
                                <List.Item  extra={data.discount}  >
                                    商品折扣
                                </List.Item>
                                <List.Item  extra={data.type.text}  >
                                    商品分类
                                </List.Item>
                                <List.Item  extra={ <Stepper
                                    min={1}
                                    defaultValue={count}
                                    onChange={(value:any) => {
                                        setCount(value)
                                    }}
                                    />}   >
                                    购买数量
                                </List.Item>
                                <List.Item  extra={ Math.round(data.price * data.discount / 10 * count ).toFixed(2)  }   >
                                    商品小计
                                </List.Item>
                            </List>
                       </div>
                       
                       <div className="gfoot">
                            <div className={hasLike?"gbtn color1":"gbtn color2"}  onClick={todoLikeAction}> {hasLike?'已点赞':'点赞'}({likeNum})</div>
                            <div className="gbtn color1">评论</div>
                            <div className={hasCollect?"gbtn color1":"gbtn color2"}  onClick={todoCollectAction}> {hasCollect?'已收藏':'收藏'}({collectNum})</div>
                            <div className="gbtn color1" onClick={()=>router('/payorder?amount='+ Math.round(data.price * data.discount / 10 * count ).toFixed(2))}>购买</div>
                            <div className="gbtn color1" onClick={addToCart}>购物车</div>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default connect(
    (state:any)=>{
        return {
            userInfo:state.getIn(['data','userInfo'])
        }
    }
)(Good)