import Good from "./Good";
import PayOrder from "./PayOrder";


export default [
    {
        path:"/good/:goodId",
        element:<Good/>
    },
    {
        path:"/payorder",
        element:<PayOrder/>
    }
]