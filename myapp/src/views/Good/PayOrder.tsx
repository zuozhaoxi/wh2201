import React,{FC} from 'react'
import { useSearchParams } from 'react-router-dom'
import MyHead from '../../components/MyHead'

const PayOrder:FC<any> = () => {
    const [searchParams] =  useSearchParams()
    return (
        <div>
            <MyHead title={'商品结算 - ' +searchParams.get('amount') } />
        </div>
    )
}

export default PayOrder