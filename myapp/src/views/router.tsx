

import {HashRouter as Hash, BrowserRouter as History , useRoutes,Navigate} from "react-router-dom"
import {lazy,Suspense} from "react"
import Loading from "../components/Loading"
import ErrorRoute from "./Errors/route"
import MainRoute from "./Mains/route"
import LoginRoute from "./Logins/route"
import GoodRoute from "./Good/route"
import MineRoute from "./Mine/route"

import Search from "./Search/Search"
import City from "./City/City"


// 路由懒加载  (闪屏效果)
const Demo = lazy(()=>import("./Demo/Demo"))
const Guide = lazy(()=>import("./Guide/Guide"))


export default function MainRouter(){
    return (
        <Hash>
            <Suspense fallback={ <Loading/>  } >
                <NewRouter></NewRouter>
            </Suspense>
        </Hash>
    )
}

function NewRouter(){
    const routes:any = [
        {
            path:"/",
            element:<Navigate replace to="/guide" />
        },
        {
            path:"/guide",
            element:<Guide/>
        },
        {
            path:"/demo",
            element:<Demo/>
        },
        {
            path:"/search",
            element:<Search/>
        },
        {
            path:"/city",
            element:<City/>
        },
        ...MainRoute,
        ...ErrorRoute,
        ...LoginRoute,
        ...GoodRoute,
        ...MineRoute,
        {
            path:"*",
            element:<Navigate replace to="/404" />
        }
    ]
    return useRoutes(routes)
}