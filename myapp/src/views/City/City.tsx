

import React, { FC, useEffect } from 'react'
import MyHead from '../../components/MyHead'
import { Button, SearchBar, Space, Toast , Empty   } from 'antd-mobile'
import { connect } from 'react-redux'
import { getCityListAsync } from '../../redux/actions'

const City:FC<any> = ({
    cityList,
    dispatch
}) => {
    useEffect(()=>{
        dispatch(getCityListAsync({},{
            'X-Host': 'mall.film-ticket.city.list'
        }))
    },[])

    const onCancel = ()=>{

    }
    const onSearch = ()=>{

    }
    return (
        <div className='city'>
            <MyHead title="选择城市" />
            <div className="sbox" style={{padding:30}}>
                <SearchBar placeholder='请输入搜索城市关键字' showCancelButton  
                onCancel={onCancel}
                onSearch={onSearch} />
            </div>
        </div>
    )
}

export default  connect(
    (state:any)=>{
        return {
            cityList:state.getIn(['data','cityList'])
        }
    }
)(City) 