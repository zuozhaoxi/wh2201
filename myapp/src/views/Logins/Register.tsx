import React, { FC, useRef } from 'react'
import MyHead from '../../components/MyHead'
import { Form, Button, Input, Toast } from 'antd-mobile';
import { Link } from 'react-router-dom';
import { reg } from './../../utils/validate';
import { Http } from '../../api';
import { useNavigate } from 'react-router-dom';
import qs from 'qs';

const Register: FC<any> = () => {
  const [form] = Form.useForm()
  const formRef: any = useRef()
  const router = useNavigate()
  const checkDbPass = ()=>{
    form.validateFields(['dbpass'])  // 触发确认的校验
    console.log('12123231')
  }
  const onFinish = async (value:any)=>{
    console.log(value)
    console.log(form.getFieldsValue())
    let res:any   = await Http.register(value)
    if(res.code==200){
      value.from='rigister'
      router('/login?'+qs.stringify(value))
    }
  }
  const onFinishFailed = ()=>{
    Toast.show({
      content: '请输入有效的注册信息',
    })
  }

  return (
    <div style={{ background: "#ccc", height: "100%" }}>
      <MyHead title="注册账户" />
      <div className="rbox" style={{ margin: "20px 0" }}>
        <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        ref={formRef} form={form} layout='horizontal' mode='card'
          footer={
            <div style={{ width: '100%', display: 'flex' }}>
              <Button style={{ flex: 1, margin: '0 10px' }} type='submit' color='success' size='large'>
                注册
              </Button>
              <Button
                style={{ flex: 1, margin: '0 10px' }}
                color='danger'
                onClick={() => {
                  console.log(form)
                  console.log(formRef)
                  form.resetFields()
                  // formRef.current.resetFields()
                }}
                size='large'
              >
                重置
              </Button>

            </div>
          }
        >
          <Form.Item label='用户名' name="username" 
              rules={[
                { required: true },
              ]}
          >
            <Input clearable placeholder='请输入用户名' />
          </Form.Item>
          <Form.Item label='手机号' name="phone"
              rules={[
                { required: true },
                { pattern:reg.phone,message:"请输入有效的手机号"}
              ]}
          >
            <Input clearable placeholder='请输入手机号' />
          </Form.Item>
          <Form.Item label='密码' name="password"
              rules={[
                { required: true },
                { pattern:reg.pwd,message:"请输入6-16位的字母和数字组合的密码"},
                
              ]}
          >
            <Input clearable onBlur={checkDbPass} type='password' placeholder='请输入密码' />
          </Form.Item>
          <Form.Item label='确认密码' name="dbpass"
              rules={[
                { required: true },
                { pattern:reg.pwd,message:"请输入6-16位的字母和数字组合的密码"},
                {
                  validator:(rule, value,callback )=>{
                      console.log(value)
                      console.log(form.getFieldValue('password'))
                      if(reg.pwd.test(value) && reg.pwd.test(form.getFieldValue('password')) ){
                        if(value==form.getFieldValue('password')){
                          return Promise.resolve();
                        }else{
                          return Promise.reject(new Error('两次密码不一样'));
                        }
                      }else{
                        return Promise.resolve();
                      }
                  }
                }
              ]}
          >
            <Input clearable type='password' placeholder='请输入确认密码' />
          </Form.Item>

          <Form.Item >
            <div className="lines" style={{ display: 'flex', alignItems: "center", justifyContent: 'space-between' }}>
              <div className="item">
                <Link to="/login" style={{ color: 'pink' }}>去登录?</Link>
              </div>
              <div className="item">
                <Link to="/findpass" style={{ color: '#f50' }}>找回密码?</Link>
              </div>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default Register


// rules={[
//   // { required: true, message: '请输入密码' },
//   // {
//   //   pattern:reg.pwd,message:'请输入6-16位的数字和字母组合密码'
//   // },
//   ({ getFieldValue }) => ({
//     validator(_, value) {
//       // if(value==""){
//       //   return Promise.reject(new Error('请输入密码'));
//       // }
//       if(value){
//         if(reg.pwd.test(value)){
//           if (!value || getFieldValue('password') === value) {
//             return Promise.resolve();
//           }else{
//             return Promise.reject(new Error('两次密码不一样'));
//           }
//         }else{
//           return Promise.reject(new Error('请输入6-16位的数字和字母组合密码'));
//         }
//       }else{
//         return Promise.reject(new Error('请输入密码'));
//       }
      
      
      
//     },
//   }),
// ]}