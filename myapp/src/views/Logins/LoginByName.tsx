import React, { FC, useEffect } from 'react'
import { Form, Input , Button,Toast} from 'antd-mobile'
import { useRef } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useSearchParams } from 'react-router-dom';
import { reg } from '../../utils/validate';
import { Http } from '../../api';
import { connect } from 'react-redux';
import { getuserInfoAsync } from '../../redux/actions';
const LoginByName:FC<any> = ({
  dispatch 
}) => {

  const formRef:any = useRef()
  const [form] = Form.useForm()
  const [searchParams] =  useSearchParams()
  const router = useNavigate()
  useEffect(()=>{
    console.log(searchParams.get('phone'))
    const username = searchParams.get('username')
    if(username){
      form.setFieldsValue({'username':username})  // 设置表单的某个值
    }else{
      form.setFieldsValue({'username':localStorage.getItem('username')})
    }
  },[])

  const onFinishFailed = ()=>{
    Toast.show({
      content: '请输入有效的登录信息',
    })
  }

  const onFinish = async (value:any)=>{
    let res:any  = await Http.login(value)
    if(res.code==200){
      sessionStorage.setItem("token",res.token)
      localStorage.setItem("username",value.username)
      // dispatch(getuserInfoAsync())
      let data:any  = await Http.getuserinfo({})
      if(data.code==200){
        dispatch({
          type:"getuserInfoAsync",
          payload:data.result 
        })
        // 继续优化 
        if(searchParams.get('from')){
          // good 
          router('/main/mine')
        }else{
          router(-1)
          
        } 
      }
      
    }
  }
  return (
    <div>
      <Form 
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        ref={formRef}
        form={form}
        layout='horizontal' mode='card'
        footer={
          <div style={{width:'100%',display:'flex'}}>
             <Button style={{flex:1,margin:'0 10px'}}  type='submit' color='success' size='large'>
            提交
          </Button>
            <Button
            style={{flex:1,margin:'0 10px'}}
            color='danger'
            onClick={() => {
              formRef.current.resetFields()
            }}
            size='large'
          >
            重置
          </Button>
           
          </div>
        }
      >
        <Form.Item label='用户名' name="username" 
          rules={[
            { required: true },
          ]}
        >
          <Input clearable  placeholder='请输入用户名' />
        </Form.Item>
        <Form.Item label='密码' name="password" 
          rules={[
            { required: true },
            { pattern: reg.pwd,message:"请输入6-16位的字母和数字组合的密码"},
            
          ]}
        >
          <Input  clearable type='password' placeholder='请输入密码' />
        </Form.Item>

        <Form.Item >
          <div className="lines" style={{display:'flex',alignItems:"center",justifyContent:'space-between'}}>
            <div className="item">
              <Link to="/register" style={{color:'pink'}}>去注册?</Link>
            </div>
            <div className="item">
              <Link to="/findpass" style={{color:'#f50'}}>找回密码?</Link>
            </div>
          </div>
        </Form.Item>
      </Form>
    </div>
  )
}

export default connect()(LoginByName)