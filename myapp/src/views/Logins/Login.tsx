

import React, { FC, useState } from 'react'
import MyHead from '../../components/MyHead'
import { Tabs } from 'antd-mobile'
import LoginByName from './LoginByName'
import LoginByPhone from './LoginByPhone'
import "./login.scss"

const Login: FC<any> = () => {
  // http://47.104.209.44/mp/brkw.mp4
  // const [vSrc, setVSrc] = useState<string>('https://img-baofun.zhhainiao.com/pcwallpaper_ugc/preview/3760b2031ff41ca0bd80bc7a8a13f7bb_preview.mp4')
  const [vSrc, setVSrc] = useState<string>('http://47.104.209.44/mp/brkw.mp4')
  return (
    <div className='loginall'>
      <MyHead title="登录"  />
      <div className="login">
        <video
          autoPlay
          loop
          muted
          playsInline
          src={vSrc}
          className="myvideo"></video>
        <div className="nav">
          <Tabs defaultActiveKey={'username'}>
            <Tabs.Tab title='用户名登录' key='username'>
              <LoginByName />
            </Tabs.Tab>
            <Tabs.Tab title='手机号登录' key='phone'>
              <LoginByPhone />
            </Tabs.Tab>
          </Tabs>
        </div>
      </div>
    </div>
  )
}

export default Login