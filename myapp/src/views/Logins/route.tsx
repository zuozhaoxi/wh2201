
import FindPass from './FindPass';
import Login from './Login';
import Register from './Register';


export default [
    {
        path:"/login",
        element:<Login/>
    },
    {
        path:"/register",
        element:<Register/>
    },
    {
        path:"/findpass",
        element:<FindPass/>
    }
]