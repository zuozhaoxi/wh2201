


import React, { FC ,useEffect,useState } from 'react'
import {Button} from "antd-mobile"
import "./index.scss"
import img1 from "../../assets/images/img1.jpg"
import img2 from "../../assets/images/img2.jpg"
import img3 from "../../assets/images/img3.jpg"
import img4 from "../../assets/images/img4.jpg"
import { MySwipe, MySwipeItem } from '../../components/MySwiper'
import { Navigate, useNavigate } from 'react-router-dom';
import { Http } from './../../api/index';
import { getYearWeek } from '../../utils'

const options:any ={
  autoplay:true ,
  loop:true,
  speed:1000,
  effect : 'cube',
  cubeEffect: {
      slideShadows: true,
      shadow: true,
      shadowOffset: 100,
      shadowScale: 0.6
  },
}

const Guide:FC<any> = () => {
  const [list,setList] = useState<string[]>([
    img1,
    img2,
    img3,
    img4
  ])
  let [count,setCount] = useState<number>(5)
  const navigate = useNavigate()

  const gotoMain = ()=>{
    navigate('/main')
  }

  const testdata = async ()=>{
    let res= await Http.testApi();
    console.log(res)
  }

  const getBanner = async ()=>{
    let res:any = await Http.getBanner({type:2,limit:4})
    setList(res.banners.slice(0,4))
  }

  let timer:any  = null 
  const timeDown = ()=>{
    timer =  setInterval(()=>{
       if(count>0){
         setCount(--count)
       }else{
         clearInterval(timer)
         timer = null;
         navigate('/main')
       }
    },1000)
  }
  useEffect(()=>{


    // 一周值看一次
    let week:any  = getYearWeek()
    let localweek:any = localStorage.getItem("localweek")
    console.log(week,localweek)
    timeDown()
    if(localweek){
      if(week==localweek){
        console.log(123)
        // 你已经看过了 
        // gotoMain()
        navigate('/main')
      }else{
        localStorage.setItem("localweek",week)
        getBanner()
      }
    }else{
      localStorage.setItem("localweek",week)
      getBanner()
    }
    

    return ()=>{
      clearInterval(timer)
      timer=null;
    }
  },[])


  return (
    <div className='guide'>
        <div className="down" onClick={()=>navigate("/main")}> 
          广告 {count} S
        </div>
        <MySwipe options={options}>
          {
            list.map((l:any,i)=>(
              <MySwipeItem key={i}>
                <img src={l.pic} alt="" style={{width:"100vw",height:"100vh"}} />
                {
                  list.length-1 == i && <Button color="danger"  onClick={gotoMain} className="gbtn"> 开启App </Button>
                }
              </MySwipeItem>
            ))
          }
        </MySwipe>
    </div>
  )
}




// const Guide:FC<any> = () => {
//   return (
//     <div>
//         <div>
//           <Button color='primary' fill='solid'>
//               Solid
//             </Button>
//             <Button color='primary' fill='outline'>
//               Outline
//             </Button>
//             <Button color='primary' fill='none'>
//               None
//             </Button>
//             <Button color='primary'>Primary</Button>
//             <Button color='success'>Success</Button>
//             <Button color='danger'>Danger</Button>
//             <Button color='warning'>Warning</Button>
//         </div>

//         <div className="gbox">
//             rem适配(等比缩放)
//         </div>
//     </div>
//   )
// }

export default Guide