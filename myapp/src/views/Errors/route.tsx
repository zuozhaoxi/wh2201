
import { lazy } from 'react';
// import NotFound from './404';
// import BadService from './503';
const NotFound = lazy(()=>import('./404'))
const BadService = lazy(()=>import('./503'))

export default [
    {
        path:"/404",
        element:<NotFound />,
    },
    {
        path:"/503",
        element:<BadService />
    }
]