


import React, { FC, RefObject, useEffect, useRef, useState  } from 'react'
import "./index.scss"
import pic from "../../assets/images/404.png" 
import { useNavigate } from 'react-router-dom'

const NotFound:FC = () => {
  interface twos{
    
  }
  type spans = {
    top:string,
    left:string,
    animationDelay:string,
    transform:string,
    background:string
  }

  const bgstar:RefObject<any>  = useRef()
  let [count,setCount] = useState<number>(5)
  const [list,setList] = useState<Array<spans>>([])

  let timer:any  = null;
  const navigate = useNavigate()

  const countDown = ():void =>{
    timer = setInterval(()=>{
       if(count>0){
         console.log('count = ' +count )
         setCount(--count)
        }else{
         clearInterval(timer)
         timer=null;
         gotoGuide()
       }
    },1000)
  }

  const gotoGuide = ()=>{
    navigate("/guide")
  }
  

  useEffect(()=>{
    countDown()
    spanInitFunc()
    return ()=>{
      clearInterval(timer)
      timer=null;
    }
  },[])

  
  const spanInitFunc = ()=>{
    // const width:any = document.getElementById('bgstar').clientWidth;
    const width:any = bgstar.current.clientWidth
    const height:any = bgstar.current.clientHeight;
    console.log(width,height)
    for(var i=0;i<520;i++){
      var left = Math.random() * width;
      var top = Math.random() * height;
      var scaler = Math.random() * 1.5;
      var rate = Math.random() * 2.5 
      
      var r = Math.random() * 255;
      var g = Math.random() * 255;
      var b = Math.random() * 255;
      var a = Math.random()    // 透明度
      list.push({
        top:top+'px',
        left:left+'px',
        animationDelay:rate+'s',
        transform:`scale(${scaler})`,
        background:`rgba(${r},${g},${b},${a})`
      })  
    }
    setList([...list])
  }

  return (
    <div className='errorpage' id="bgstar" ref={bgstar} >
        <img src={pic} alt="" className="myimg" />
        <div className="down" onClick={gotoGuide} >剩余 {count} S</div>
        {
          list.map((l,i)=>{
            return (
              <span className="spandot" key={i} style={l}  ></span>
            )
          })
        }
    </div>
  )
}

export default NotFound