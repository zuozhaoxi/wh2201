


import { lazy } from 'react';
import Cart from '../Cart/Cart';
import Classify from '../Classify/Classify';
import Home from '../Home/Home';
import Mine from '../Mine/Mine';
import { Navigate } from 'react-router-dom';
const MainIndex = lazy(()=>import('./index'))


export default [
    {
        path:"/main",
        element:<MainIndex/>,
        children:[
            // 重定向 
            {
                path:"/main",
                element:<Navigate replace to="/main/home" />,
            },
            {
                path:"/main/home",
                element:<Home/>
            },
            {
                path:"/main/classify",
                element:<Classify/>
            },
            {
                path:"/main/cart",
                element:<Cart/>
            },
            {
                path:"/main/mine",
                element:<Mine/>
            }
        ]
    }
]