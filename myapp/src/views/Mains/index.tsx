


import React from 'react'
import { Outlet } from 'react-router-dom'
import { AntFoot, MyFoot } from '../../components/MyFoot'

const MainIndex = () => {
  return (
    <div className="main" style={{width:'100vw',height:"100vh"}}>
      <Outlet/>
      {/* <MyFoot/> */}
      <AntFoot/>
    </div>
  )
}

export default MainIndex