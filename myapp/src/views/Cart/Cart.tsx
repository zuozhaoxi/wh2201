


import React, { FC, useEffect, useState } from 'react'
import MyHead from '../../components/MyHead'
import { connect } from 'react-redux';
import {Modal,Stepper ,Toast, Checkbox, Button } from 'antd-mobile'
import { useNavigate } from 'react-router-dom';
import { getMyCart } from '../../redux/actions';
import {Link} from 'react-router-dom'
import { Http } from '../../api';
const Cart:FC<any> = ({
    userInfo,
    myCart,
    dispatch,
}) => {
    const router = useNavigate()
    const [quan,setQuan] = useState<boolean>(false)
    const [total,setTotal] = useState<number>(0)
    const [amount,setAmount] = useState<number>(0)

    useEffect(()=>{
        if(userInfo){
            // 请求购物车 
            dispatch(getMyCart({
                phone:userInfo.phone 
            }))
        }else{
            Modal.confirm({
                content:"请你先登录哦!",
                title:"提示",
                onConfirm:()=>{
                    router("/login")
                }
            })
        }
    },[])

    const changeItemCount=async (count:any,item:any,index:any)=>{
        let res:any  = await Http.setcart({
            _id:item._id,
            count:count 
        })
        if(res.code==200){
            Toast.show({
                content:"数量修改成功"
            })

            // dispatch(getMyCart({
            //     phone:userInfo.phone 
            // }))

            item.count = count;
            myCart.splice(index,1,item);
            dispatch({
                type:"getMyCart",
                payload:myCart
            })
        }
    }

    // 修改单个选中 
    const changeOneCheck = async (value:any,item:any,index:any)=>{
        let res:any  = await Http.setcart({
            _id:item._id,
            check:value
        })
        if(res.code==200){
            Toast.show({
                content:"修改成功"
            })
            item.check = value;
            myCart.splice(index,1,item);
            console.log(myCart)
            dispatch({
                type:"getMyCart",
                payload:[...myCart]
            })
        }
    }

    useEffect(()=>{
        console.log('123123')
        setQuan(myCart.every((item:any)=>item.check))
        setTotal(myCart.reduce((prev:any,item:any,index:any)=>(prev+ (item.check?item.count:0) ),0))
        setAmount(myCart.reduce((prev:any,item:any,index:any)=>(prev+ (item.check?item.count * item.good.price * item.good.discount / 10 :0) ),0))
    },[myCart])

    const changeQuan = async (value:boolean)=>{
        let res:any  = await Http.setcart({
            // 修改我的 
            phone:userInfo.phone,
            check:value
        })
        if(res.code==200){
            Toast.show({
                content:"修改成功"
            })
            var list = myCart.map((val:any)=>{
                val.check=value;
                return val;
            })
            dispatch({
                type:"getMyCart",
                payload:[...list]
            })
        }
    }

    const gopayorder = ()=>{
        router("/payorder?amount="+amount.toFixed(2))
    }
    return (
        <div style={{paddingBottom:150}}>
            <MyHead title="购物车" back={false} />
            <div>
                <div style={{padding:30}}>
                    {
                        myCart.map((item:any,index:number)=>{
                            return (
                                <div key={index}  style={{display:'flex',width:"100%",marginTop:30}}>
                                    <Checkbox checked={item.check} onChange={(value)=>changeOneCheck(value,item,index)} style={{marginRight:20}} ></Checkbox>
                                    <img src={item.good.img} alt="" style={{width:280,height:280,marginRight:30}} />
                                    <div className="right" style={{flex:1,padding:20}}>
                                        <h3 className="name"> {item.good.name}</h3>
                                        
                                        <p style={{display:'flex',justifyContent:"space-between"}}>
                                            <span>分类 : {item.good.type.text}</span>
                                            <span>折扣 : {item.good.discount}折</span>
                                        </p>
                                        <div>
                                            <Stepper
                                                    min={1}
                                                    defaultValue={item.count}
                                                    onChange={(value:any) => {
                                                        changeItemCount(value,item,index)
                                                    }}
                                                />
                                        </div>
                                        <p style={{display:'flex',justifyContent:"space-between"}}><span>单价: {item.good.price}</span>  <span>小计: { Math.round(item.good.price * item.good.discount * item.count/10).toFixed(2) }</span> </p>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>

                <div className="ftbom" style={{padding:"40px 20px",display:'flex',alignItems:'center',justifyContent:'space-between'}}>
                    <div className="all">
                        <Checkbox checked={quan}  onChange={changeQuan} style={{marginRight:20}} >全选反选</Checkbox>
                    </div>
                    <div className="total" style={{padding:'0 20px'}}>
                        总数量: <span style={{color:'red',fontSize:60}}> {total} </span>
                    </div>
                    <div>
                        <Button color="danger" onClick={gopayorder}>结算 ({amount.toFixed(2)}) 元</Button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default connect(
    (state:any)=>{
        return {    
            userInfo:state.getIn(['data','userInfo']),
            myCart:state.getIn(['data','myCart']),

        }
    }
)(Cart)