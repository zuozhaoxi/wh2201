import React, { FC, useEffect, useState } from 'react'
import { connect } from 'react-redux'
import MyHead from '../../components/MyHead'
import { getGoodListAsync, getGoodTypeAsync } from '../../redux/actions'
import { SideBar } from 'antd-mobile'
import List from './List'
import "./index.scss"

const Classify: FC<{
  goodList: any,
  dispatch: any
}> = ({
  goodList,
  dispatch
}) => {
    const [types, setTypes] = useState<any>([])
    let  [current,setCurrent] = useState<any>('0')
    const changeCurrent = (index:any)=>{
      // console.log(index) 
      setCurrent(index)
    }
    useEffect(() => {
      dispatch(getGoodListAsync())

    }, [])

    useEffect(() => {
      dispatch(getGoodTypeAsync())
    }, [])

    useEffect(() => {
      console.log(goodList)
      var obj: any = {}    // obj.衬衫 = 衬衫 
      var arr: any = [{
         title:"全部",
         key:'all',
         text:"全部",
         value:"all"
      }]
      goodList.forEach((item: any) => {
        if (!obj[item.type.text]) {
          obj[item.type.text] = item.type.text
          item.type.key = item.type.value;
          item.type.title = item.type.text;
          arr.push(item.type)
        }
      })
      console.log(arr)
      setTypes([...arr])
      // 前端数组去重 
    }, [goodList])


    return (
      <div className='classify subbox'  >
        <MyHead title="商品分类" back={false} />
        <div className="classbox" style={{display:"flex"}}>
          <div className="bleft" >
            <SideBar 
              activeKey={current}
              defaultActiveKey={'0'}
              style={{
                '--width':'200px'
              }}
              onChange={changeCurrent}
            >
              {types.map((item:any,index:any) => (
                <SideBar.Item  key={index} title={item.title} />
              ))}
            </SideBar>
          </div>
          <div className="bright" style={{flex:1}}>
             {
               current==0 ?   <List list={goodList} />
               : 
               <List list={goodList.filter((item:any)=>item.type.value==types[current].value)} />
             }
          </div>
        </div>
      </div>
    )
  }

export default connect(
  (state: any) => ({
    goodList: state.getIn(['data', 'goodList'])
  })
)(Classify)