

import React , {FC} from 'react'
import { Link } from 'react-router-dom';

const List:FC<{
    list:Array<any>
}> = ({
    list
}) => {
  return (
     <ul>
         {
             list.map((item:any,index:number)=>{
                 return (
                     <li key={index} style={{margin:"45px 0",border:"3px solid  #ddd",paddingBottom:'30px',borderRadius:"10px"}}>
                         <Link to={"/good/"+item._id+"?name="+item.name  } >
                            <img src={item.img} style={{width:'100%',height:'600px'}} />
                            <h3 className="name" style={{padding:'30px '}}> {item.name }</h3>
                            <div style={{display:'flex',justifyContent:'space-between',padding:'30px '}}>
                                <p className="one">{item.price}</p>
                                <p className="one">{item.type.text }</p>
                            </div>
                         </Link>
                     </li>
                 )
             })
         }
     </ul>
  )
}

export default List