

import React, { FC, useEffect, useState } from 'react'
import MyHead from '../../components/MyHead'
import Photo from "../../assets/images/photo.png"
import { inject, observer } from 'mobx-react'
import { connect } from 'react-redux'
import { getMyCart, getMyCollects, getMyLikes, getuserInfoAsync } from './../../redux/actions/index';
import "./index.scss"
import { useNavigate } from 'react-router-dom'
import { List, Button , Mask , ProgressCircle, Toast } from "antd-mobile"
import {
    UnorderedListOutline,
    PayCircleOutline,
    SetOutline,
    AntOutline,
    HandPayCircleOutline,
    ScanningOutline,
    TransportQRcodeOutline,
    AppOutline,
    MessageOutline,
    MessageFill,
    UserOutline,
    TeamOutline,
    AppstoreOutline,
    TruckOutline,
    HistogramOutline
} from 'antd-mobile-icons'
import MyAvatar from './MyAvatar'
const Mine: FC<{
    info?: any,
    userInfo: any,
    count: number,
    dispatch: any,
    myLikes:any,
    myCollects:any,
    myCart:any, 
}> = ({
    userInfo,
    count,
    dispatch,
    myLikes,
    myCollects,
    myCart,
    info,
}) => {
        
        const router = useNavigate()
        const [cache,setCache]= useState<number>(Math.ceil(Math.random() * 100 ))
        const [visible,setVisible] = useState<boolean>(false)
        let [percent,setpercent] = useState<number>(0)
        let timer:any = null 
        useEffect(() => {
            dispatch(getuserInfoAsync({},(userInfo:any)=>{
                dispatch(getMyLikes({
                    phone:userInfo.phone 
                }))

                dispatch(getMyCollects({
                    phone:userInfo.phone 
                }))

                dispatch(getMyCart({
                    phone:userInfo.phone 
                }))
            }))

            
        }, [])

        // 退出登录 
        const logoutAction = ()=>{
            dispatch({
                type:"getuserInfoAsync",
                payload:null 
            })
        }
        const showMask =  ()=>{
            setVisible(true)
            timer = setInterval(()=>{
                if(percent<100){
                    percent+=5;
                    setpercent(percent)
                }else{
                    clearInterval(timer)
                    timer = null;
                    setpercent(0)
                    setVisible(false)
                    setCache(0)
                    Toast.show({
                        content:"缓存清除成功"
                    })
                }
            },200)
        }
        return (
            <div className='mine'>
                <MyHead title="个人中心" back={false} />
                <div className="mine-top">
                    <div className="left">
                        <MyAvatar/>
                    </div>
                    <div className="right">
                        {
                            userInfo ? <div >
                                {userInfo.username} -  {userInfo.phone}
                            </div>
                                : <h2 onClick={() => router('/login')} style={{ fontSize: 48 }}>立即登录  {count} </h2>
                        }
                    </div>
                </div>
                <ul className="list">
                    <li className="liitem" onClick={()=>info.changeCount(100)}>
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAAAAXNSR0IArs4c6QAACqFJREFUaAXdms9vXUcVx78z98d7z7/y7NhJXVEaSqRSV7CgEmpRSaoKgYSASkiBFexZgcQSiTZqV/wHhQUCIVQwggWwQqUNLLqoIlEk3FYtUVKaOE4c5/nZ79f9yefc99ykaW2XxqnsjjV+986dO/P9zpw5c86Z67RLKku5YZVyl5ofxeMhFOe0LZgR2HeDGZLYeuf0jTqLC06n3l33I7lbpJdTS1uAuHlydO10K7kbYKl2g8gWCQhoyensvNNDVFhadlpY4IL05uXhu8eHt3v6/81Ra8fvGgJfWpIW5kudpfyhZcoWyFsEjdwNYhWoIRFrhGeL3/Y69SDlkNC00/mOVy3x8m0nP8abm5TPSq1O9a5mRp3v5c/aqLHmOIBWgTVRquiSp0oN4kLHxgvpOs8gt/jvUqd+x/0QjhuSecrxAHFCns5e99pYDnS/vNqNQFHTKw4CBS1IlcPcjZzG6bSX0MrYXlIZtdWVGnGpDrdjKURcUeW8WSjJc6WtQlO9XK+r0OR8roemCy0il6ceLCFUQmY0K0uQmJoK1ElDNSZDxa1QaRAprYcai72yXijHbwqxGp0lmVMc7z2hJJHikNmg6QgyZVIobGTq8hv1M0V5qqSZqbeRaTzK1G7nWoAcszUkpEWvpaVAC5B5M4TAy5/UxqtPq8y+yHwf2XvEt9Gi81fko5fkH3haRx8/r956quNZqiUjtZCHLBov3efVux5qbTxU/y/H1Hn1DESat9HtnXu1LI4oHzyh4pWT6rivyH/+vNYY89444n8fomNa7PyfA02EgXp5pP4bp/ctmZuHqWTAN5Z+ogbCb7gNPzyYIbRZjbXT9IHW+7EKE7MDkvL0YZZFXXk/19E4V28+Z4ZQze1WoNUyUtmtsRntrzWz09iWxayioF7hNvzwgBD7jKnmCM1WOtNdByuVYaPCbfjh4XWWTTOxmyxU6KKDxQa0aV6vcBt+eDAzWACH+k4Ja8gXrKkDlryPK9yGHx7MipkzOWYNGq8oDyAhMGdkw+9i5zXHjPSKoUnjmLGDlhLIGG4jBI+DR2CXAR+KmBmAiS+VFqXiuUUl174hFfVd3h0+rt09UPNLawom+hjrGGHK2NUyLKuca6xgM39dWblk7OXc8h8vRtTyCqhhGEIVnYZa/5jT4GKD+92T8wPFR59XgK2X5KXYdMyQDbWMWT43smbrPlP9Wz9TsvY3XfvrD5W17t+15eaJawrHuzRnpuQAkANAZgAfEoMC5SXlRor1anY+pIxIwRr29svWHoxnmn401+XffmrXPqOZ85r58i/lJ8/JB6nq9JUOOXjN3MtYYpZHkCkcGUMvmGxp9omfKz78xq6NOzcAqJHpAbUDkY68aw+zb8v5dZ6tU6dd/dq9p3yrjtW39+z9srQZ3jnFR97W3Dd/D5mWHPUNr+E2/PBAODqlsqDQRj1HAFIA0Kjv87up6cf+KBfs3EnrzKzyjjlYECv7/PbQlj2kraucbIB1U7Z7K7fnVT3q23vZZqnW3+/akY0LU82cfAElsMn79FHhTCrchh8eoY4dLbW6nCtPMo0Bfj1NFMU91lNXrrmq2r3/VP/cF7btqH9pUv3nPrvt87180Pj063KTa8pLsLmuwhCcOE8TUYKvlGkiNlvuv4Vms1y1XqbVXioulA16iASio0017n59LzHdVlv1+fOI52aFy/AZTsNruA0/PCCEX/7Sa+incUSunsrniTKmMq9EoavaXW/dFoi9fLl25BJKwNYp4gw+w2l4Dbfhh4fXi/T4yCO5xnFxoxKFkLFm0oHCoo9ssh5y1sV+Say1IhviMnyG0/AabsMPD6/HUJqaK5SuklMWlqluFIMLUb/BQINLdyKu8+FGqL98SAKT4TJ8htPwGm7DDw+v07T94hm2w+lSUxDLPRGWKEcD5chqrv7F3feiDwfv/3+rf/EY4jbEZfgMp+E13IYfHjsbo9nKlAbLj+/Yc21+Q4dOXGDv2sCeQh1v7UuIgdgfyqJgC2D0bH+1PZVBdGZysakLd8X8ZZU1JZtNtc8c1+Ays7BN6l94WPn1Pyk4vE0FMz+e5NljJzE+ruO5XsUYMUM1DXBta7r6wg8k/I2dUvPERYUTtrESbrKQEyPnyLw4vIYUCxHQZH6dkRw9t3p2be+Fk301T17YqSsGo6a157+HdiNo4HB3wGl4Dbfhh4cpBUaLgmiWHAUog1C9y4e18txPlW9sv/9s9Uxgz4a9yg6TpwKNKWLXzrNRO9usb8pb9/acelV9e2/Uxla72/1m65/T6h9+rGzlsPpFXOE13IYfHsTlngr10nqko5/BUcoauvbrJ5X3vsvIWWx09xTPtzV94j+IHOYNVoD37A1mMZgZMxI5b+sSziZpxbtEjiilq1PWYPBm1DrzgJKVDxY+c75Ln4s6+p1nGI5NFWyyK68lEHrW3O5Ib3fqGkBi9Vdv7c5iH9WY//6DSjsbqrHRfmK8z5Dd47VKTGvQCDXbOHgxBYBXuA0/PEKdX0H14I9ncahu9w4Equ/wbAZBrG4eKxsLtekJY4WEUEM22Ml+gF46eDNUohgMt+GHh9faBVQ1RyUp2s3b3nDAkudwwXAbfnh4zRP1sQCD5T6FBy0VFo+vJqPigFIg2QGWCzjr8aynA5ZyJsJwG354DAndzMGFWweCN5fuz2sftm4F5nWVooZtfGa2ZHZS9sqtlfbtfTj5mmLbvMFt+OFBTIGoT5kMzzE9Dw898ixqHJ99nycXdDX16G+qAInhriYkQf5STpbX68TjMBwLfIv4nkua+eqPFE+/TGh1Y9/R8nFHtbl/aeZrzyg6sozZROTHcIMfHpg+v6jrXEJcjOPsojelRjRFrHgOm/EwxuMslWfRgE2s20lsNepwZuYsrIe1a/5AFWd7H9o7fO3xTu13vlJ5p2R4MQxL8tQscSJRBHmroEhRbjATLYLzq/S/ij1ATNBdVS8lLNZosx91UdOc+adXkMG5TGEXTzDrEY3Et8EcyjJUIUalc4wCURZWG6TqCjjmLwsIWQSU/F7VghFqEdJdUkBE9dZEdxAxCx5CAR40x/ilIypLDMGVBEiKNtdrRHyIy+UblFn5QCmn4flV23f4gGGqmauBWb8+xoMOkUzk0wM4RB2ar1KY/14QX8MyNmcst8PmisZwhgzEh0lEnt+TrGjoknCV07K5GDiNAkNpkahgEwLrYGK2cCg9MQ83PtAs+HtNA0bUx4K37XtyTdQx+QfmExFyZPRStEcUokWYcln4yOF/8N1CWVkUzAuz42wm3m+K3gP1gxUwfgCvaPGCebvWP9HRKqpKUFJdYtlgIdgY4a6UYV8huFcsFLdsM8R3M8fuy3X2LEcrtVRroBvjvCioo8LN0yQQYSMR4C06XOaMnTnAwiiMhRHij172LllzI0LWS4YjFRK/drj0OQH6gNBVjM9VhiyBQV89ymZ4fpmD4/u/bjN0iiHhs5LGcqYZNN4aReVsoQ4fMkxBpp8QOoJIFGHrMQAlAfZBhodIPYJjFam9YzNqyWSR05AB0hawjryJHWskyZgtviKpMyvtNNXELKLIhxczVyAEXp0rWH6IzMfr0xgTmac+Ph8v2RybghzONTO9NVsH9fOykdBWP0NitsBPjwgevA8A/wdFwLAhoyb/zQAAAABJRU5ErkJggg==" width="56px" height="56px" alt="" />
                        <div className="common-p">电影订单 {info.count}</div>
                    </li>
                    <li className="liitem">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAAAAXNSR0IArs4c6QAACahJREFUaAXtWs9vFMkVfq+qutsz9njAxmZxWIEiULR2LhE3TnPIJYckygEp5xxz3H8AO/fc8l9wyGojJZdI61xyiVAUKTiK4ODVsnawM8B4frS7u6pevtdjAwYZY4ZdnBVlV/f0TNV731evfnW9xyLEVKfVg/syr69vcKezxX96kJlP3Dx3u1smuTzL/aePjbPT7EzBfVPi3mA7KA/qjaVMeg0zqfiYSyvqPRMfhtI6Nxerh3syP78U/+W78pNrRVxfX5JOZ1mINpA1rdZ3ox/X1sZk7hDI0AYvLt4zDygzoVfYzdGO8+3M7e1speenOSlpkBWSZw1xWRbzLOUiS6dtnaOVqbfJh/VVlspU2apDdalO1a0YFItiUmyKUbEq5hc5sIjgizX8cI8X1le4Bcv0N0vbylIzs+dtnkWbx/Rj8qM1MnKThC+qgG8tsTyiyH8l17zdMOVXjcKEwawL/aKMratp6MNSu517cotWYKHbYnmVTQfoVqiDrvUP0x5lRhpkd4ZDNz017YoiXiHaX0eRHxHxDO7fcoJOpk+Yql9G7z4zjay/vT/guUZD2o8yEnlKP176Kcjs0hr9he0Xqx2+c2eZd1c2uDcqbZxyphrkbgFk+tUgEQ6/JVEy7z01xISPilj9cSFtyXBYUd5sStcZ+npuKH+7s0y/XtklpzAXFja4RZf4qiMeZTv28ZPM9d0gsRLTKHzzvVM5BCB001JM0dAk5Qy15wq55BelC+y0sKGlxKl1Fm5tUGNjm7v5lvF+aC7MWdMdJA5mTjBuFg/lnYH7IgeTCCXxwtww9HrB5D1vGiOK/Q6JcjF0i6h1d4t3mpv11Jy5xPSepI4S7zABJGeAxFEIignYFKNi1eVEsSsH5WKQiW4QXcWfrjPlfrQ+weQbnBNn6y55VOL7fVJMik0xKlbFrNiVg3JxuoguLj7h7VksmH6abSxYTDRkvCWOjvz7JfCKdhtcIgbYnMEMBqwZb7setzee8PrOBh9YYIVSC5MZ4oJzFGFTemM5gNAZs5HsRyfW2NSV2K8oHUzqtmBdeDQ56tR3om0ii+2MlYwr9saYyqC8CZizT0j/JjG/x+Sxd0K51/8ceRY94hco9IPXFTSGjQBbZRJjxbHlkgOlRMuohVxvfb580OVkN+dBv2STezZFwGzhOMBSrxOuvzG7z4RlyEIWe463yuO6kAFZJ+lTTIqtxgisilmxKwet6zq4PEAeYUPTLtr0dNjnhklhSE0RuiJ+PT6JRMvorLUdg26jTp8EfUErqqyTajNsAk74F85jyefabZJsQNf7RNdQ+RULNJtEMCK6HdpirOe1Opjiz5mkLVGtaYDr9FnrqgyV9Vpl+FExKTbFqFhfTscOefRKKrArPKnJYZnrGGafwpovyz7VM15j3igFYMpgh+KY0i9Y6Ogm2lfxJC7HiPzmv34V23Pszyz0uDsC85LTabXjcfy/ebCn09CkAV4wiwFzMx0PvxcsdDpRZ7X0d47Qsy73ti2OgXaf2PwB9TFxTpRaJFHf1K5PImViC2Fu/zzGicmQylBZk5DRuhMTmhTAu64/MSEcsvwMe5HWpMBUhsqaVM7EY2i8sMZPJwXyrupPbKF3BeRdyflA6F215Dcl5ztnoYknhQ8L6zF97cPCekzDTDyGzuzCOjffFC5EStE95omv9s/a5/0urCOamWnJdNYU6o/f4V6w0KNnIPUDTlnf8KX4SLVv5eFVbM+xv0DoKJYSj/bgMOfoL+/3STEptuPSK4RGI6JUrCTIcO6dOSspJsWmGBXry8mt45sc+XuwWo97NGVTibESPfjShIPWM5VEAlBF/EeZEie9Xo9mxdJ9HIU8BNJ6Yb1ybV7y7UJmilIKFMKpn7Cv0AYm4uynCzPNnwVWaNsumhlLFkhlcC4C6wywVlkqVy7Nyw5AGlrHtXZ+EQW40kMsJIma9IqmIP77WSCjGBSLYlJsik6xKuYan3JYf/bGeo/KMF3HBWShET08kcZR4Cnjcej6O5w2TnYQX2uc7KIYaiyKCdgU4xhrJoqd6F6twGjwQr58Hr7Kdh3kEOI+7hhFAY6HYPHRbEWxv8L7/hew7uPJYJ2+tupU3YpBsSgmxaYYx1iHNXbloFzcHej4/l2izflNmjt3Icb8v0EGqNAsPZfkXRbLKrivydvfiKMsBpPZBH4j2FAMWZwA67k0XEkHB/WvzJsnkDg4QbY4sEeXEsiMkBnIRB8qWEM9cN5iaNN+4nxJhfGcAleRhnTGhKnGXNzMN+FWTenODZ0UwKh/a0kWNyjmCIHx7Sq2z0ffHTh4yaoqeoauaIILUOeiNRKC5yRhsT6StThnr2KEd+wA+GkJHSwMHnQSAxYYJFYoVJ4DdFVRTBldVaLtCmApwbuCL9u3z5e+l8Mjh5CZxcbVuHMDQwxc0CSrZn2dTKtziec3t+1oesc9/o8kpjl268OFl43IZ1iZUoab31nr0CnxgHkdhHCcD/8gweky2Vk4fKASIEdnMfgxQon7FLvKo/kEDkX4Q8omucKTV+9VGUcz1dxHXDWHi7579RIiSrbR5QhdB2l3tw4Coh0EBp0rssBpj1vJDGk8AElCGforDgF95biSUKnvGU4abypsJuD3hwctwrd5aCKVePrk0cC6C6goCrxTIUFvgC4MG/iwfYRqU3mbVIFjCWzVXlr4XtEOXwJzAygPObg16L59SyOalulu78+xjeceYmkeDvf4cjpbk9K4gKnUYyhikoAXULz2CmfQww2mGrYVLH16Dkdq1FZO4NwsUqyD9YIRxbmQQSun3u+jmzEXXsk8LPfC4ux0aA8x1/WKeINgEHBQLm8UvPS0TF0DbnRfRtuagfe1KOHjRGKH9beqR00GYSU6xhGUb/iQplYO/R0uwQqDFtN1JsnS2B/E6FIT8sqEc2npTwpeAhhGeJn6+FcQ1bSMlwdEN42GsT3Mwv2BC09HTd8wWVkNRiVCvfYxEAu8ZexjgshZfJ5VYZQlGGbI0aZvlbWuylBZKlNlqw7VpTpVt2JQLIpJsSnGGiswK3bloFzquXLckKsHrfv/HQBYk3i+BR2TOoydow7CZhAYtPPgn6bh+py58/wx2D96NOSlJUwmJufH3dZBQ4ybZdLr3HxfFmJDtraILl6clq8gsPBPJPctWbz2Q1hlW3SL8zxGTjWu1kMYu4mDxRBfHZJao1W+rWWQ6sAmRGrVxDSWBgtXY2OJNR5A3eiHfo+HD/vPSV2tq775ZfN50cuXWzWw+/hKN8y6x8yXt4Sw8PdvLI2JYEa+VU9iCFtEudsvkFFJ/wMKasPHqJjHuAAAAABJRU5ErkJggg==" width="56px" height="56px" alt="" />
                        <div className="common-p">商品订单</div>
                    </li>
                </ul>
                <div className="mine-box">
                    <List >
                        {
                            userInfo ?
                                <div>
                                    <List.Item extra={myLikes.length} clickable prefix={<UnorderedListOutline />} onClick={() => { router('/mylikes')}}>
                                        我的点赞
                                    </List.Item>
                                    <List.Item extra={myCollects.length} clickable prefix={<PayCircleOutline />} onClick={() => { router('/mycollects') }}>
                                        我的收藏
                                    </List.Item>
                                    <List.Item extra={5} clickable prefix={<UserOutline />} onClick={() => { }}>
                                        我的评论
                                    </List.Item>
                                    <List.Item extra={5} clickable prefix={<HandPayCircleOutline />} onClick={() => { }}>
                                        我的优惠券
                                    </List.Item>
                                    <List.Item extra={599} clickable prefix={<ScanningOutline />} onClick={() => { }}>
                                        我的余额
                                    </List.Item>
                                    <List.Item clickable prefix={<TransportQRcodeOutline />} onClick={() => { }}>
                                        我的信息
                                    </List.Item>
                                    <List.Item extra={myCart.length } clickable prefix={<TruckOutline />} onClick={() => { router("/main/cart") }}>
                                        我的购物车
                                    </List.Item>
                                    <List.Item clickable prefix={<TeamOutline />} onClick={() => { }}>
                                        修改密码
                                    </List.Item>
                                </div>
                                :
                                <div>
                                    <List.Item extra={5} clickable prefix={<AntOutline />} onClick={() => { }}>
                                        我的足迹
                                    </List.Item>
                                    <List.Item onClick={showMask} extra={cache+'M'} clickable prefix={<TransportQRcodeOutline />}>
                                        清除缓存
                                    </List.Item>
                                    <List.Item clickable prefix={<TruckOutline />} onClick={() => { }}>
                                        设置
                                    </List.Item>
                                    <List.Item clickable prefix={<MessageOutline />} onClick={() => { }}>
                                        帮助和客服
                                    </List.Item>
                                    <List.Item clickable prefix={<TeamOutline />} onClick={() => { }}>
                                        公告
                                    </List.Item>
                                    <List.Item clickable prefix={<AppOutline />} onClick={() => { }}>
                                        联系我们
                                    </List.Item>
                                    <List.Item clickable prefix={<SetOutline />} onClick={() => { }}>
                                        公司简介
                                    </List.Item>
                                </div>
                        }
                    </List>
                </div>

                {
                    userInfo && <div className="mbtn">
                        <Button onClick={logoutAction} color="default" block size="large" >退出登录</Button>
                    </div>
                }

                <Mask visible={visible} style={{height:'100%'}}>
                    <div className="center" style={{width:'100%',height:'100%',display:'flex',justifyContent:'center',alignItems:'center'}}>
                        <ProgressCircle
                            percent={percent}
                            style={{
                                '--fill-color': '#FF3141',
                                '--track-width': '7px',
                                '--size': '240px' 
                            }}
                            >
                            <span style={{color:"#fff" }}> {percent} % </span>
                        </ProgressCircle>
                    </div>
                </Mask>
            </div>
        )
    }

// @inject('info')
// @observer
// export default inject('info')(observer(Mine))

export default inject('info')(
    connect(
        // 输入逻辑
        (state: any) => {
            console.log(state)
            return {
                userInfo: state.getIn(['data', 'userInfo']),
                count: state.getIn(['data', 'count']),
                myLikes:state.getIn(['data','myLikes']),
                myCollects:state.getIn(['data','myCollects']),
                myCart:state.getIn(['data','myCart']),
    
            }
        }
        // 输出逻辑 
    )(observer(Mine))) 


    // inject()(connect()())