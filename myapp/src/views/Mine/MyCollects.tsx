


import React, { FC,useEffect } from 'react'
import MyHead from './../../components/MyHead';
import { connect } from 'react-redux';
import { getMyCollects } from '../../redux/actions';
import {Link} from 'react-router-dom'

const MyCollects:FC<any> = ({
    myCollects,
    dispatch,
    userInfo
}) => {
    useEffect(()=>{
        dispatch(getMyCollects({
            phone:userInfo.phone 
        }))
    },[])
  return (
    <div>
        <MyHead title="我的收藏"/>
        <div>
            <div style={{padding:30}}>
                {
                    myCollects.map((item:any,index:number)=>{
                        return (
                            <Link key={index} to={"/good/"+item.goodId} style={{display:'flex',width:"100%",marginTop:30}}>
                                <img src={item.good.img} alt="" style={{width:280,height:280,marginRight:30}} />
                                <div className="right" style={{flex:1}}>
                                    <h3 className="name"> {item.good.name}</h3>
                                    <p>单价: {item.good.price}</p>
                                    <p>分类 : {item.good.type.text}</p>
                                </div>
                            </Link>
                        )
                    })
                }
            </div>
        </div>
    </div>
  )
}

export default connect(
    (state:any)=>{
        return {
            myCollects:state.getIn(['data','myCollects']),
            userInfo:state.getIn(['data','userInfo']),

        }
    }
)(MyCollects)