import MyCollects from "./MyCollects";
import MyLikes from "./MyLikes";



export default [
    {
        path:"/mylikes",
        element:<MyLikes/>
    },
    {
        path:"/mycollects",
        element:<MyCollects/>
    }
]