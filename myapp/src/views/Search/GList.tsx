

import React, { FC } from 'react'
import { Link } from 'react-router-dom';

const GList:FC<any> = ({
    list 
}) => {
  return (
    <div className="glist" style={{padding:30}}>
        {
            list.map((item:any,index:any)=>{
            return (
                <Link key={index}  to={"/good/"+item._id+"?name="+item.name  }  className="gitem"  style={{display:"block",marginTop:40,borderBottom:'3px solid #ccc',borderRadius:0}}>
                    <img src={item.img} style={{width:'100%',height:500,}} alt="" />
                    <div className="ginfo" style={{padding:'40px 20px '}}>
                        <h2 className="name"> {item.name }</h2>
                        <h2>单价: {item.price }</h2>
                        <h2>分类: {item.type.text  }</h2>
                    </div>
                </Link>
            )
            })
        }
    </div>
  )
}

export default GList