

import React, { FC,useState } from 'react'
import MyHead from '../../components/MyHead'
import { connect } from 'react-redux';
import { Button, SearchBar, Space, Toast , Empty   } from 'antd-mobile'
import { useEffect } from 'react';
import { getGoodListAsync } from '../../redux/actions';
import GList from './GList';
import { QuestionCircleOutline } from 'antd-mobile-icons'

const Search:FC<any> = ({
  goodList,
  dispatch 
}) => {
  useEffect(()=>{
    dispatch(getGoodListAsync())
  },[])
  let  [list,setList] = useState<any>([])
  let  [flag,setFlag] = useState<any>(false)

  const onCancel = ()=>{
    setList([...list])
    setFlag(false)
  }
  const onSearch = (value:any)=>{
    setFlag(true)
    list = goodList.filter((item:any)=>(item.name.indexOf(value)!=-1||item.type.text.indexOf(value)!=-1))
    console.log(list)
    setList([...list])
  } 
    return (
      <div>
          <MyHead title="搜索"/>
          <div className="sbox" style={{padding:30}}>
            <SearchBar placeholder='请输入搜索关键字' showCancelButton  
              onCancel={onCancel}
              onSearch={onSearch} />
          </div>
          {
            flag && <div>
                {
              list.length>0 ? <GList list={list} />
              : <Empty 
              style={{ padding: '64px 0' }}
              image={
                <QuestionCircleOutline
                  style={{
                    color: 'var(--adm-color-light)',
                    fontSize: 48,
                  }}
                />
              }
              description='没有商品数据' />
            }
            </div>
          }
      </div>
    )
}

export default connect((state:any)=>{
  return {
    goodList:state.getIn(['data','goodList'])
  }
})(Search)