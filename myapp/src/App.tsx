// import { useState } from 'react'
// import logo from './logo.svg'
// import './App.css'
import MainRouter from './views/router';

function App() {
  return (
    <div className="App">
        <MainRouter/>
    </div>
  )
}

export default App
