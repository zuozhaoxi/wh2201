import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import postcssPx2Rem from 'postcss-pxtorem'
import babel from "@rollup/plugin-babel"
// https://vitejs.dev/config/
export default defineConfig({
  base:"./",
  plugins: [

    react({
      babel: {
        parserOpts: {
          plugins: ["decorators-legacy"],
        },
      },
    }),
    // babel({
    //   extensions:['.tsx'],
    //   plugins: [
    //     ["@babel/plugin-proposal-decorators", { "legacy": true }],
    //     ["@babel/plugin-proposal-class-properties", { "loose" : true }]
    //   ]
    // }) 
  ],
  transpileDependencies: true,
  lintOnSave: false,   // 去除ESLINT 警告  
  pwa: {
    iconPaths: {
      favicon32: 'favicon.ico',
      favicon16: 'favicon.ico',
      appleTouchIcon: 'favicon.ico',
      maskIcon: 'favicon.ico',
      msTileImage: 'favicon.ico'
    }
  },

  server: {
    host: "localhost",
    port: 7700,
    open: true,
    // proxy:{}  // 代理
  },

  // px=>rem 
  css: {
    postcss: {
      plugins: [
        postcssPx2Rem({
          rootValue: 37.5,  // antd-mobile 设计稿宽度是 375px 
          propList: ["*"]
        })
      ]
    }
  }
})
