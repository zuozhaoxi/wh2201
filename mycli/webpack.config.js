
// webpack 基本配置属性
// 入口
// 出口
// devtool  source-map 
// 模块打包
// JS/JSX/TSX/TS
// PNG/JPG
// CSS/LESS/SCSS
// JSON/HTML
// plugin 插件

const path = require('path')  // Node 内置 
const htmlWebpackPlugin = require('html-webpack-plugin')  // 处理HTML 
const openBrowserPlugin = require('webpack-open-browser-plugin')  // 打开浏览器
const miniCssExtractPlugin = require("mini-css-extract-plugin")   // 样式抽离打包 
const Webpack = require('webpack')

module.exports = {
    mode:"development",  //  development 开发环境   production 生产环境  
    entry:["./src/main.js" ],   // 入口文件 
    output:{
        filename:"js/[name].[fullhash:8].js",   // fullhash:8  长度为8 随机字符串  避免文件名冲突
        path:path.resolve(__dirname,"dist"),  // 打包文件就是DIST 
        publicPath:""  // 打包的相对路径 
    },
    // 调试工具  方便在线调试
    devtool:"source-map", 
    
    // 别名
    resolve:{
        alias:{
            "@":path.resolve(__dirname,"src"),
            "~":path.resolve(__dirname,"src/utils"),
        }
    },

    // 模块定义 babel loader 
    module:{
        rules:[
            {
                test:/\.(js|jsx|tsx)$/,
                exclude:/node_modules/,
                use:["babel-loader"]
            },
            {
                test:/\.(jpg|png|gif|svg|ttf|woff|woff2|eot)$/,
                use:[
                    {
                        loader:"url-loader",
                        options:{
                            limit:8192,
                            name:"imgs/[name].[hash:8].[ext]"  // 404.png => 404.qwer1234.png 
                        }
                    }
                ]
            },
            {
                test:/\.css$/,
                use:[miniCssExtractPlugin.loader,'css-loader'],
            },
            {
                test:/\.(scss|sass)$/,
                use:[miniCssExtractPlugin.loader,'css-loader','sass-loader'],
            },
            {
                test:/\.less$/,
                use:[miniCssExtractPlugin.loader,'css-loader','less-loader'],
            }
        ]
    },

    devServer:{
        liveReload:true,
        hot:true,
        // open:true,
        host:"0.0.0.0",   // 主机  0.0.0.0 泛指任何主机  localhost 127.0.0.1  192.168.53.212 
        port:8000,      //端口
        compress:true, 
        // proxy:
        // }
    },


    // webpack 插件 
    plugins:[   

        // 抽离Css
        new miniCssExtractPlugin({
            filename: "css/[name].[fullhash:8].css",   // 文件名 
            chunkFilename: "css/[id].[fullhash:8].css",
        }),

        // 处理打包html 
        new htmlWebpackPlugin({
            // filename 文件名
            template:'./public/index.html', // 打包路径 
            inject:true,  // 自动引入js和css 
            minify:true,  // 压缩html 
        }),

        new openBrowserPlugin({
            url:"http://localhost:8000"   // 访问地址 
        }),

        // 全局自动引入
        new Webpack.ProvidePlugin({
            React:'react',
            Component:['react',"Component"]
        })
    ]
}