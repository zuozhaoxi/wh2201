

// React 请求数据 

import React, { Component } from 'react';
import Axios from '../utils/ajax';
class MyData extends Component {

    state={
        list:[]
    }
    componentDidMount(){
        Axios.post("http://localhost:3800/goodslist",{
            params:{
                id:12,
                limit:30
            },
            data:{
                skip:5
            }
        })
        .then(res=>{
            console.log(res)
            this.setState({
                list:res.data.result 
            })
        })
        .catch(err=>{
            console.log(err)
        })
    }

    render() {
        return (
            <div>
                <h2>React 实现简单的数据请求 </h2>
                <ul style={{width:"100%",margin:"20px 0 ",padding:20 }}>
                    {
                        this.state.list.map((item,index)=>{
                            return (
                                <li key={index} style={{margin:"20px 0 "}}>
                                    <img src={item.img} style={{width:"100%",height:240}} alt="" />
                                    <h3>{item.name}</h3>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        );
    }
}

export default MyData;
