

// 封装轮播图 组件  Swiper
// 插槽 和 props 


import React, { Component } from 'react';
import MySwipe from './MySwipe';
import Axios from './../utils/ajax';
const {MySwipeItem } = MySwipe
class MyBanner extends Component {
    state = {
        imgs:[
            require("@/assets/images/img1.jpg"),
            require("@/assets/images/img2.jpg"),
            require("@/assets/images/img3.jpg"),
            require("@/assets/images/img4.jpg"),
        ],
        options:{
            autoplay:true ,
            loop:true,
            speed:2000,
            effect : 'cube',
            cubeEffect: {
                slideShadows: true,
                shadow: true,
                shadowOffset: 100,
                shadowScale: 0.6
            },
        },
        list:[]
    }

    componentDidMount(){
        Axios.get("http://localhost:3800/goodslist",{
            params:{
                limit:5,
            }
        }).then(res=>{
            this.setState({
                list:res.data.result 
            })
        })
    }

    render() {
        const {imgs,options,list } = this.state;
        console.log(imgs)
        return (
            <div>
                <MySwipe options={options} id="onebanner">
                    {
                        imgs.map((item,index)=>(
                            <MySwipe.MySwipeItem key={index}>
                                 <img src={item.default } style={{width:"100%",height:300}} alt="" />
                            </MySwipe.MySwipeItem>
                        ))
                    }
                </MySwipe>

                <MySwipe options={options} id="newbanner">
                    {
                        list.map((item,index)=>(
                            <MySwipeItem key={index}>
                                <img src={item.img} style={{width:"100%",height:300}} alt="" />
                            </MySwipeItem>
                        ))
                    }
                </MySwipe>
            </div>
        );
    }
}

export default MyBanner;
