
// 隔空传递 
// context 隔空传递数据
// 数据嵌套太深  props   
// props 一级一级往里面传递数据    比较麻烦  
// context 隔空传递   this.context 


// redux    
// mobx        运用 context API  
// https://segmentfault.com/a/1190000004636213  

import React, { Component } from 'react';
import PropTypes from 'prop-types';

class MyContext extends Component {
    state = {
        msg:"WH2201-YYDS-DayDayup"
    }

    // 设置子组件的 context 
    getChildContext(){
        return {
            word:"WH2201 - 早点就业 - 早点上班",
            count:1314520
        }
    }

    render() {
        return (
            <div>
                <h2>
                    Context - 传递数据 - 隔空传递 
                </h2>
                <h2>最外层的父组件 </h2>
                <CptA msg={this.state.msg} ></CptA>
            </div>
        );
    }
}

// 定义子组件context的类型正则匹配 
MyContext.childContextTypes = {
    count:PropTypes.number.isRequired,
    word:PropTypes.string 
}

class CptA extends Component{
    render(){
        return (
            <div>
                <hr/>
                <h2> CptA - CptA - 1 </h2>
                <CptB msg={this.props.msg} />
            </div>
        )
    }
}

class CptB extends Component{
    render(){
        return (
            <div>
                <hr/>
                <h2> CptB - CptB - 2 </h2>
                <CptC msg={this.props.msg} />
            </div>
        )
    }
}

class CptC extends Component{
    render(){
        return (
            <div>
                <hr/>
                <h2> CptC - CptC - 3 </h2>
                <h2>word --- {this.context.word } </h2>
                <CptD msg={this.props.msg} />
            </div>
        )
    }
}

CptC.contextTypes = {
    word:PropTypes.string 
}

class CptD extends Component{
    render(){
        console.log(this)
        return (
            <div>
                <hr/>
                <h2> CptD - CptD - 4 </h2>
                <h2>msg---{this.props.msg} </h2>
                <h2>count === {this.context.count} </h2>
            </div>
        )
    }
}

// 获取父元素的context
CptD.contextTypes = {
    count:PropTypes.number 
}
export default MyContext;
