
/*
    组件之间的通信 
    
    1. 父子 组件 
    <A>
        <B></B>
    </A>
    2. 兄弟组件 
    <A></A>
    <B></B>

    props 传递数据
    state 修改数据 

    父组件如何修改子组件

    父组件把组件的state 当着子组件的props 传递给子组件
    父组件修改 state 会二次render 子组件接收到变化的 props  从而实现子组件修改  

    ref   this.refs  对象获取
    1. ref 作用于DOM 元素  指向这个真实的DOM元素    ref="text"  this.refs.text
    2. ref 作用于组件  指向这个组件对象             ref={e=>this.text=e }  this.text 
    ref={ele=>this.oh=ele}

    架构
    flux  redux mobx  event 


    受控的Input 组件
    受控的组件  input 绑定动态的 value  value 受state 状态控制的  只有state改变 input才能改变
    非受控的组件  value 没有绑定状态 

    
*/


import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MyBtn from './MyBtn';

class MyProp2 extends Component {
    
    state = {
        count:1000,
        msg:"2201 - daydayup",
        show:true 
    }
    
    changeCount=()=>{
        this.setState({
            count:++this.state.count 
        })
    }
    changeMsg=(e)=>{
        this.refs.one.style.color = "red" // ref 
        this.setState({
            msg:e.target.value 
        })
    }
    change=()=>{
        console.log(this.inp)
        this.setState({
            msg:this.inp.value 
        })
    }
    changeShow=()=>{
        this.setState({
            show:!this.state.show 
        })
    }
    changeChildByRef=()=>{
        // this.child => 子组件的 this 
        this.child.setState({
            flag:!this.child.state.flag 
        })
        this.child.changeNumber(520)
    }
    render() {
        console.log(this)
        const {
            count,msg ,show
        } = this.state
        return (
            <div>
                <h2 ref="one" >React 组件通信  -  (父改子) </h2>
                <h2>count -- {count }</h2>
                <h2>msg =-- {msg} </h2>
                <MyBtn text="changeCount" onClick={this.changeCount} ></MyBtn>
                <p>
                    <input type="text" value={msg} onChange={this.changeMsg}  />
                </p>
                <p>
                    <input type="text" placeholder='修改msg' onChange={this.change}  ref={el=>this.inp=el}     />
                </p>
                <MyBtn text="点击切换Show" onClick={this.changeShow} ></MyBtn>
                <MyBtn text="通过 ref 去实现父改子 " onClick={this.changeChildByRef}  ></MyBtn>

                <hr/>
                <MyChild
                    show={show}
                    count={count}
                    msg = {msg}
                    ref={el=>this.child=el} 
                />
            </div>
        );
    }
}

class MyChild extends Component{
    state = {
        flag:true,
        number:1314,
    }
    changeNumber(num){
        this.setState({
            number:this.state.number+num
        })
    }
    render(){
        const {
            count,msg,show 
        } = this.props
        const {flag,number} = this.state
        return (
            flag?<div>
            <h2> MyChild - MyChild  - 1  </h2>
            <h2>number--- {number}</h2>
            <h2>props (属性) </h2>
            <h2>count - {count}</h2>
            <h2>msg - {msg} </h2>
            <div style={{width:180,height:140,background:'orange',borderRadius:15,display:show?'flex':'none',justifyContent:'center',alignItems:'center'}}>
                我是被父组件给点击切换的 
            </div>
        </div>:null 
        )
    }
}
MyChild.propTypes = {
    count:PropTypes.number.isRequired
}


export default MyProp2;

