



import React, { Component } from 'react';
// 1.下载引入
import Swiper from "swiper"   
import PropTypes from 'prop-types';

class MySwipe extends Component {
    render() {
        const {id,children} = this.props  
        // 2.满足HTML静态结构
        return (
            <div className='swiper-container' id={id}  >
                <div className='swiper-wrapper'>
                    {children}
                    {/* 这里不确定  */}
                    {/* <div className="swiper-slide"></div> */}
                </div>  
            </div>
        )
    }

    componentDidMount(){
        let {id,options} = this.props;
        // 3. 实例化swiper
        let mySwiper = new Swiper("#"+id,{
            ...options, 
            // 默认的参数 
            observer:true, // 动态的刷新轮播 解决异步的BUG
        })
    }
}

MySwipe.defaultProps = {
    id:"banner",
    options:{}
}

MySwipe.propTypes = {
    id:PropTypes.string,
    options:PropTypes.object 
}

// 静态属性 
MySwipe.MySwipeItem=({children})=>{
    return (
        <div  className='swiper-slide'>
            {children}
        </div>
    )
}
export default MySwipe;
