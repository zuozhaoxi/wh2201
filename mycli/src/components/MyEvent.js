

// React 绑定事件  

// 基于原生Js 编写事件    DOM0级事件和DOM2级事件的区别  
// addEventListener("click",fn,true/false);  false/true 冒泡/捕获   默认false 冒泡   DOM2 
// onclick  onchange  onfocus  DOM0
// <p onclick="get()"></p>

// jquery 
// $("div").on("click")    $("div").off("click")
// $('div').bind('click')  绑定   $('div').unbind('click')

// 事件委托  把事件绑定到父元素上   子元素触发事件   通过事件冒泡  让父元素代替子元素执行冒泡的事件   
// $("div").on("click",childnode,fn);
// $("div").delegate(childnode,'click',fn)


// vue
// v-on:click = "get()"
// @input = "input($event)"  $event 事件对象 


// react 绑定事件  驼峰命名

// onClick
// onChange
// onInput
// onMouseMove
// onMouseDown
// onMouseUp
// onTouchStart
// onTouchMove
// onTouchEnd
// onKeyPress
// onKeyDown
// onKeyUp

// 绑定事件 
// 1. 全局变量 / 外部函数
// 2. 组件内部自定义函数
// 3. 原型链挂载 


import React, { Component } from 'react';
import { JS } from '../utils/common';

const actions = {
    clickOne(){
        console.log("clickOne - clickOne - 1 ")
    },
    clickTwo(msg){
        console.log("msg===="+msg)
    },
    change(e){
        console.log(e)
        console.log(e.target.value)
    },
    parentclick(){
        console.log('I am Parent')
    },
    stopEvent(e){
        var e = e || window.event;
        if(e.stopPropagation){
            e.stopPropagation()
        }else{
            e.cancelBubble = true;
        }
        console.log('我会阻止事件冒泡')
    },
    NoStopEvent(){
        console.log("我是正常的事件,我不会阻止事件冒泡")
    }
}

// super 超类 父类 
// super super()  继承父类的 构造器内容  
// super super.obj  指向父类的原型对象  prototype 

class MyEvent extends Component {

    constructor(){
        super()   // 继承父类的 构造器内容 
        this.checkThisOne =  this.checkThisOne.bind(this)  // 强制改变 this 指向 
    }

    handleChange(e){
        console.log(e.target.value )
    }

    handleMove(e){  
        console.log(e.clientX)
    }

    handleTouch(e){
        console.log("handleTouch")
        console.log(e.touches[0].clientX)
    }
    handleKeyAction(e){
        console.log(e)
        console.log(e.keyCode || e.charCode)
    }

    // undefined 
    // class 构造函数  里面的自定义函数的 this 指向  undefined
    checkThisOne(){
        console.log(this)
    }

    checkThisTwo(){
        console.log(this)
    }

    checkTHisThree=()=>{   // 配环境 stage-0 
        console.log(this)
    }


    render() {
        console.log(this)
        return (
            <div>
                <h2>MyEvent - MyEvent - React事件绑定 </h2>
                <h2 onClick={actions.clickOne} >react 如何实现事件触发  </h2>
                <h2 onClick={ ()=>actions.clickTwo('WH2201 - DayDayup')  } >wh2201 趁早背面试题 </h2>
                <p> 
                    <input type="text" onInput={actions.change} />
                </p>
                <div onClick={actions.parentclick}>
                    <h2 >I am Parent </h2>
                    <h2 onClick={actions.stopEvent}>stop -  一定会阻止事件冒泡 </h2>
                    <h2 onClick={actions.NoStopEvent}>No stop - 一定不会阻止事件冒泡  </h2>
                </div>

                <div>
                    <h2 onClick={()=>JS.todoNext('ARe YOu OK?')}> WH2201 务必保证每天的基本的代码量  </h2>
                </div>

                <p>
                    <input type="text" onInput={this.handleChange} />
                </p>
                <div onMouseMove={this.handleMove } onTouchStart={this.handleTouch} style={{width:300,height:200,background:'orange'}}>
                    鼠标mousemove
                </div>
                <p>
                    <input type="text" onKeyDown={this.handleKeyAction } placeholder='keydown' />
                    <input type="text" onKeyUp={this.handleKeyAction } placeholder='keyup' />
                    <input type="text" onKeyPress={this.handleKeyAction } placeholder='keypress' />
                </p>
                <p>
                    <button onClick={this.checkThisOne}>检测 this - 1 </button>
                    <button onClick={()=>this.checkThisTwo()  }>检测 this - 2 </button>
                    <button onClick={ this.checkTHisThree }>检测 this - 3 </button>



                </p>
            </div>
        );
    }
}

// 箭头函数的 this 
// 改变 this 指向  (apply call bind )
// 箭头函数 保留this指向
// 1. 箭头函数本身没有this 它会借用函数外部代码块的this  (不能new  不能被继承  不能被实例化)
// 2. 箭头函数this 永远指向箭头函数的初始定义环境  不会指向箭头函数的当前所在环境   
export default MyEvent;
