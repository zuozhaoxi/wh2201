

import React, { Component } from 'react';

class MyBtn extends Component {
    render() {
        const {
            style,
            className,
            onClick,
            text,
            disabled
        } = this.props
        return (
            <button disabled={disabled} onClick={onClick} style={style} className={'btn '+className}>
                {text}
            </button>
        );
    }
}

MyBtn.defaultProps = {
    text:"按钮"
}
export default MyBtn;
