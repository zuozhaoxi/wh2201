import MyCpt from "./MyCpt.js";

import MyFoot from "./MyFoot.jsx";
import MyHead from "./MyHead.js";

import Mycss from "./Mycss.js";
import MyEvent from "./MyEvent.js";
import MyProp1 from "./MyProp1.js";
import MyState1 from "./MyState1.js";
import MyProp2 from "./MyProp2.js";
import MyState2 from "./MyState2.js";
import MyLife from './MyLife';
import MyContext from "./MyContext.js";
import MyData from "./MyData.js";
import MyBanner from "./MyBanner.js";
import MyTodoList1 from "./MyTodoList1.js";
import MyTodoList2 from "./MyTodoList2.js";
import MyTodoList3 from "./MyTodoList3.js";


export default class MIndex extends Component{
    render(){
        return (
            <div style={{padding:15}}>
                <MyHead/>
                <h2> 
                    React 基础语法 
                </h2>
                <hr/>
                {/* <MyCpt/> */}
                {/* <Mycss/> */}
                {/* <MyEvent/> */}
                {/* <MyProp1/> */}
                {/* <MyState1/> */}
                {/* <MyProp2/> */}
                {/* <MyState2/> */}

                {/* <MyLife></MyLife> */}
                {/* <MyContext/> */}
                {/* <MyData/> */}
                {/* <MyBanner/> */}
                {/* <MyTodoList1/> */}
                {/* <MyTodoList2/> */}
                <MyTodoList3/>
                <hr/>


                <MyFoot/>
            </div>
        )
    }
}