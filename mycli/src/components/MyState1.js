/*
    state 状态 react 组件数据交互的载体  状态用来修改的 
    1. state 不能在组件外部定义  不能在组件外部 修改  只能在组件内部定义声明 只能在内部修改 
    2. state 用来被修改的  this.state 获取 state, this.setState() 来修改 state 
    3. state 在组件内部的 getInitialState(已废弃) 来初始化定义 state ,必须返回一个对象 
    4. state 修改 setState 这个方法会修改 state 会重新执行 组件内部的 render方法 , 会触发页面的
    二次渲染  虚拟DOM 会根据react 的 diff  算法  得到新的虚拟DOM 最后的批量的更新    
*/ 



import React, { Component } from 'react';
import MyBtn from './MyBtn';
// super 超类 父类 
// super super()  继承父类的 构造器内容  
// super super.obj  指向父类的原型对象  prototype 
var timer = null;
class MyState1 extends Component {

    constructor(props){
        super(props)
        this.state = {
            count:2000,
            msg:"wh2201-daydayup",
            word:"are you oK?",
            num:1,
            flag:true   // true 暂停  false 计数  
        }
    }

    state = {
        name:"zuozuomu",
        count:3000,
        age:18,
    }

    countAdd=()=>{
        // 修改方法 setState  本身异步函数  添加一个函数 转为  同步 
        this.setState({
            count:++this.state.count 
            // 转变为同步 
        },()=>{
            console.log("count has been changed " + this.state.count  )
        })
    }

    changeMsg=(msg)=>{
        this.setState({
            msg 
        })
    }

    changeWord=e=>{
        console.log(e.target.value)
        this.setState({
            word:e.target.value 
        })
    }

    change=(e)=>{
        this.setState({
            word:e.target.value 
        })
    }

    startTimeDown=()=>{
        // 异步函数 
        this.setState({
            flag:!this.state.flag 
        },()=>{
            if(this.state.flag){
                clearInterval(timer)
                timer = null;
            }else{
                timer = setInterval(()=>{
                    this.setState({
                        num:++this.state.num 
                    })
                },1000)
            }
        })
    }

    render() {
        // 死循环
        // this.setState({
        //     count:++this.state.count 
        // })

        console.log(this)
        const {
            count,msg,word,num , flag 
        } = this.state 
        return (
            <div>
                <h2>React 数据状态 state </h2>
                <h2>count -- {count } </h2>
                <h2>msg -- {msg} </h2>
                <h2>word -- {word} </h2>
                <hr/>
                <MyBtn text="countAdd" onClick={this.countAdd}/>
                <MyBtn  style={{margin:'0 10px'}} text="changeMsg" onClick={ ()=>this.changeMsg('WH2201 听话写代码背面试题') }/>
                <p>
                    <input type="text" placeholder='changeword' value={word} onChange={this.changeWord} />
                </p>
                <p>
                    <input type="text"  placeholder='changeword' onChange={this.change }/>
                </p>
                <p>
                    <MyBtn style={{background:flag?'orange':'green'}} text={flag?"暂停计数":"开始计数" } onClick={this.startTimeDown}/>
                </p>
                <p style={{width:30,height:30,borderRadius:'50%',background:num%2==0?'#b5b5b5':'yellow'}}>

                </p>
                <h2>num =  {num }</h2>
            </div>
        );
    }
}

export default MyState1;

