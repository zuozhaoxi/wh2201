/*
1. react 数据传递载体  props 属性(父子数据传递 )   state 状态  组件之间数据交互  

2. props
a.  props 默认从组件外部（父组件）传入,props 也能在组件内部初始化定义
b.  组件内部 通过生命周期钩子函数    App.defaultProps = {}  定义props 的初始值 
c.  props 一般不允许被修改   props 只用来传递数据
d.  props 接收  对象 常量 函数  数组  任何数据
e.  props 在组件内部 通过 this.props 来获取  key-value 
*/ 

// PropTypes  校验Props 
// optionalArray: PropTypes.array,
// optionalBool: PropTypes.bool,
// optionalFunc: PropTypes.func,
// optionalNumber: PropTypes.number,
// optionalObject: PropTypes.object,
// optionalString: PropTypes.string,
// optionalSymbol: PropTypes.symbol,

import React, { Component } from 'react';
import PropTypes from "prop-types"
class MyProp1 extends Component {
    render() {
        const name = "zuozuomu"
        const list = ["Vue","原生JS","NodeJS面试题"]
        const person = {
            username:"liangzai",
            age:19,
            word:"Are you OK?"
        }
        return (
            <div>
                <h2>React - props 数据传递的载体  </h2>
                <MyChild
                    time="0525"
                    id="2201"
                    msg="天道酬勤"
                    name={name}
                    list={list}
                    person={person}
                    {...person}
                />
            </div>
        );
    }
}

class MyChild  extends Component{
    render(){
        console.log(this)
        const {
            time,id,msg , name ,list ,person 
        }  = this.props
        return (
            <div>
                <h2> MyChild - MyChild - MyChild </h2>
                <h2>props=== {time} -- {id} -- {msg} </h2>
                <h2>name -- {name} </h2>
                <h2>list -- {list} </h2>
                <h2>person --- {person.username} - {person.word }</h2>
            </div>
        )
    }
}

// 初始化定义的props 
MyChild.defaultProps={
    id:"1314",
    msg:"做事写代码背面试题"
}

MyChild.propTypes = {
    id:PropTypes.string.isRequired, 
    msg:PropTypes.string.isRequired,
    list:PropTypes.array.isRequired,
    person:PropTypes.object.isRequired, 
}
export default MyProp1;
