

// React 实现简单的留言板  
// 主组件 留言板组件  MyTodoList2
// 添加留言组件  PostMsgCom
// 显示留言组件  ShowMsgCom 

import React, { Component } from 'react';
import MyBtn from './MyBtn';

class MyTodoList2 extends Component {
    constructor(){
        super()
        this.state = {
            comments:[
                {
                    title:'Vue',
                    content:"Vue is so easy"
                },
                {
                    title:'Node',
                    content:"Node is so 无聊"
                },
                {
                    title:'React',
                    content:"React is so 火热"
                },
                {
                    title:'原生',
                    content:"原生 is 如此的寂寞"
                },
                {
                    title:'Css',
                    content:"Css is so 烦躁"
                }
            ]
        }
    }

    addComment=(comment)=>{
        this.state.comments.push(comment)
        this.setState({
            comments:this.state.comments 
        })
    }

    delComment=(item,index)=>{
        this.state.comments.splice(index,1)
        this.setState({
            comments:this.state.comments
        })
    }

    setComment=(item,index)=>{
        this.state.comments.splice(index,1,item)
        this.setState({
            comments:this.state.comments 
        })
    }
    render() {
        const {comments} = this.state
        return (
            <div>
                <h2>
                    React 实现简易的留言板  - 2 - MyTodoList2
                </h2>
                <ShowMsgCom comments={comments}  delComment={this.delComment} setComment={this.setComment}/>

                <PostMsgCom addComment={this.addComment}/>
            </div>
        );
    }
}

class ShowMsgCom extends Component{
    delComment=(item,index)=>{
        this['item'+index].className = 'zoomOut'
        setTimeout(()=>{
            this.props.delComment(item,index)
            // this.state.comments.splice(index,1)
            // this.setState({
            //     comments:this.state.comments
            // })
            
            this['item'+index] && (this['item'+index].className = '')
        },1200)
    }

    setComment=(item,index)=>{
        let content = window.prompt(item.title,item.content)
        if(content && content !=item.content){
            item.content = content; 

            this.props.setComment(item,index)
            // this.state.comments.splice(index,1,item)
            // this.setState({
            //     comments:this.state.comments 
            // })
            this['item'+index].className="tada"
            setTimeout(()=>{
                this['item'+index] && (this['item'+index].className = '')
            },1200)
        }
    }

    render(){
        const {comments} = this.props;
        return (
        <div style={{margin:"10px 0",width:'100%',height:'auto',padding:"15px",borderRadius:15,border:'1px solid #0f0'}}>
            <h2>展示留言 - 1 </h2>
            <ul>
                {
                    comments.map((item,index)=>{
                        return (
                            <li ref={el=>this['item'+index]=el}       key={index} className="zoomIn" style={{  margin:"10px 0",background:"pink"}}>
                                <p>序号: {index+1}</p>
                                <p>标题: {item.title}</p>
                                <p>内容: {item.content}</p>
                                <p>操作: 
                                    <MyBtn text="删除"  onClick={()=>this.delComment(item,index) }/>
                                    <MyBtn text="修改"  onClick={()=>this.setComment(item,index)} style={{background:"red"}}/>
                                </p>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
        )
    }
}

class PostMsgCom extends Component{
    addCommentMy=()=>{
        var title = this.title.value
        var content = this.content.value 

        this.props.addComment({title,content})

        this.title.value = ""
        this.content.value  = ""
    }
    render(){
        return (
            <div style={{width:'100%',height:'auto',padding:"15px",borderRadius:15,border:'1px solid deeppink'}}>
                <h2>添加留言 </h2>
                <div>
                    <p>
                        <input type="text" placeholder='请输入标题' ref={el=>this.title=el} />
                    </p>
                    <p>
                        <input type="text" placeholder='请输入内容' ref={el=>this.content=el} />
                    </p>
                    <p>
                        <MyBtn onClick={this.addCommentMy} text="添加留言" style={{background:'#0f0'}}/>
                    </p>
                </div>
            </div>
        )
    }
}
export default MyTodoList2;
