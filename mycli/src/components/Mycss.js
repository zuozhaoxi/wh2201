// react 书写 样式 

// 1. 基于class  但是 react 必须 className 代替class
// 2. 基于内联样式 style  style = {obj}   遵循 驼峰命名 
// style = {backgroundColor:"red",fontSize:20}
// lineHeight:'20px'    必须带单位 
// 3. 全局变量  外部样式   原型链挂载 


import React, { Component } from 'react';
import { Style } from '../utils/common';
import "./index.scss"

const golbal = {
    flex:{
        display:'flex',
        justifyContent:'center',
        alignItems:"center"
    },
    box:{
        border:'1px solid #ddd',
        color:"green",
        fontSize:80
    }
}

class Mycss extends Component {
    render() {
        return (
            <div className ="mbox">
                <h2 className='title'> React 组件内部书写 CSS 样式 </h2>
                <h2 style={ {color:'red',backgroundColor:'#0f0',fontSize:40} } > 基于class  但是 react 必须 className 代替class </h2>
                <h2 style={{...golbal.flex,...golbal.box}} > 基于内联样式 style  style = obj    遵循 驼峰命名  </h2>
                <h2 style={golbal.box} > 基于内联样式 style  style = obj    遵循 驼峰命名  </h2>
                <h2 style={Style.all}>
                    WH2201 - daydayup - 用心背面试题啊 
                </h2>
            </div>
        );
    }
}

export default Mycss;
