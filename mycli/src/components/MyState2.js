
/*
    组件之间的通信 
    
    1. 父子 组件 
    <A>
        <B></B>
    </A>
    2. 兄弟组件 
    <A></A>
    <B></B>

    props 传递数据
    state 修改数据 

    子组件如何修改父组件
    1. 反向 props  父组件把修改自身变量state的方法 通过 props 传递给子组件 子组件触发 

    
    兄弟组件通信
    1. 中间人模式  子组件A 去修改父组件  父组件再去修改 组件B 
    a. 子改父   反向props  + props
    b. 子改父   反向props  + ref 


    2. 事件机制  bus 事件总线  event (emit on once)
    emit 发送事件
    on 监听事件
    once 监听一次
    remove 移除事件


    架构
    flux  redux mobx  event 


    受控的Input 组件
    受控的组件  input 绑定动态的 value  value 受state 状态控制的  只有state改变 input才能改变
    非受控的组件  value 没有绑定状态 


*/ 


import React, { Component } from 'react';
import MyBtn from './MyBtn';
import EventEmitter  from "events" // 无需安装 Node自带 

const bus = new EventEmitter()  // emit on remove once 
class MyState2 extends Component {
    state = {
        count:2000,
        msg:"Wh2201 - daydayup - 天天进步一点点 ",
        show:true 
    }
    changeCount=(count)=>{
        this.setState({
            count:this.state.count + count 
        })
    }
    changeMsg=(msg)=>{
        this.setState({msg:msg})
    }
    changeShow=show=>{
        this.setState({show})
    }
    changeChildTwo=()=>{
        this.child2.setState({
            message:'wh2201 - 早起写代码，晚睡背面试题...'
        })
    }
    render() {
        const {
            count , msg , show 
        } = this.state 
        return (
            <div>
                <h2>React 组件通信  (子改父)</h2>
                <h3>count - {count} </h3>
                <h2>msg -- {msg}</h2>
                <div style={{width:220,height:140,background:'orange',borderRadius:15,display:show?'flex':'none',justifyContent:'center',alignItems:'center'}}>
                    我是被子组件给点击切换的 
                </div>
                <hr/>
                <MyChild
                    changeCount={this.changeCount}
                    changeMsg={this.changeMsg}
                    show={show}
                    changeShow={this.changeShow}
                    changeChildTwo={this.changeChildTwo}
                />
                <hr/>
                <MyChildTwo
                    count={count}
                    msg={msg}
                    show={show}
                    ref={el=>this.child2=el}
                />
            </div>
        );
    }
}

class MyChild extends Component{
    changeByBus=()=>{
        bus.emit("changeByBus","通过事件总线修改了Message")
    }
    changeTwoNum=()=>{
        bus.emit("changeTwoNum",200)
    }
    render(){
        const {changeCount,changeMsg , changeShow ,show, changeChildTwo } = this.props 
        return (
            <div>   
                <h2>
                    MyChild - MyChild  - 1 子组件 
                </h2>
                <MyBtn text="修改父亲的count" onClick={()=>changeCount(200)}></MyBtn>
                <p>
                    <input type="text" placeholder='修改父亲的msg' ref={el=>this.inp=el}  onChange={()=>changeMsg(this.inp.value)} />
                </p>
                <MyBtn text="修改父亲的 show "  onClick={()=>changeShow(!show)} />
                <MyBtn text="修改兄弟组件2 " onClick={changeChildTwo} />
                <MyBtn text="事件总线去修改兄弟组件2  " onClick={this.changeByBus} />
                <MyBtn text="事件总线去修改兄弟组件2 == Num " onClick={this.changeTwoNum} />
            </div>
        )
    }
}

class MyChildTwo extends Component{
    state = {
        message:"2201 - 天道酬勤 ",
        num:8000
    }
    // mounted 
    componentDidMount(){
        bus.on("changeByBus",msg=>{
            this.setState({
                message:msg 
            })
        })

        bus.on("changeTwoNum",num=>{
            this.setState({
                num:this.state.num+num 
            })
        })
    }
    render(){
        const {count,msg , show } = this.props;
        const {message,num } = this.state; 
        return (
            <div>   
                <h2>
                    MyChildTwo - MyChildTwo  - 2 子组件 
                </h2>
                <h2>message == {message} </h2>
                <h1>num== {num }</h1>
                <h2> 
                    count -- {count}
                </h2>
                <h2>msg -- {msg} </h2>
                <h2>{show ? '晚上下雨':'晚上不会下雨的哦'}</h2>
                <hr/>
                <MyList id="list">
                    {arr}
                </MyList>
            </div>
        )
    }
} 

const arr = ["未来可期","天道酬勤","勤勤恳恳","肯为人先"]

// React 插槽 Slot 
// props.children 就是获取插槽内容  
class MyList extends  Component{
    render(){
        console.log(this)
        return (
            <div>
                <h3>
                    Mylist - mylist - mylist 
                </h3>
                {this.props.children.map((item,index)=>(
                    <h2 key={index}> {item} </h2>
                ))}
                <hr/>
                <MyArr msg="daydayup"> 
                    <div>
                        <h2>Are YOU oK</h2>
                        <h2>WH2201 daydayup </h2>
                    </div>
                </MyArr>
            </div>
        )
    }
}

class MyArr extends Component{
    render(){
        console.log(this)
        return (
            <div>
                <h2> MyArr - MyArr -MyArr </h2>
                <div>
                    {this.props.children}
                </div>
                <hr/>
                <Myfunc id="520">
                    <h2>Wh2201 每天早背面试题</h2>
                    <h2>WH2201 每天熬夜到晚上2点 </h2>
                </Myfunc>
            </div>
        )
    }
}


const Myfunc = ({id,children })=>{
    // console.log(props)
    return (
        <div>
            <h2>我是纯函数组件 </h2>
            <h2>Myfunc - Myfunc - 1 </h2>
            <h2>id == {id}</h2>
            <div>
                { children }
            </div>
        </div>
    )
}


export default MyState2;
