


import React, { Component } from 'react';

import {connect} from 'react-redux' 

@connect(
    state=>({
        // data:state.data, 
        // count:state.data.count,
        // ...state.data, 
        data:state.get('data'),
        count:state.getIn(['data','count']),
        city:state.getIn(['data','city']),
        msg:state.getIn(['data','msg']),
        list:state.getIn(['data','list']),
        test:state.getIn(['data','test']),
        flag:state.getIn(['data','flag'])
    }
))
class MyCounter extends Component {
    render() {
        console.log(this)
        const {count,msg,list,test,city ,data ,flag  } = this.props
        return (
            flag ? <div style={{border:"1px solid deeppink",borderRadius:15,minHeight:200,marginTop:20,padding:20}}>
                <h2> React-redux 实现简单的计数器  </h2>
                <h2>这是一个简单的UI 组件 </h2>
                <h2>count --- {count}</h2>
                <h2>city == {city} </h2>
                <h2>msg  --- {msg}</h2>
                <h2> test --- {test} </h2>
                <ul>
                    {
                        data.get('list').map((l,i)=>{
                            return (
                                <li key={i}>
                                    {l.name}--{l.price}
                                </li>
                            )
                        })
                    }
                </ul>
            </div> : <h1>WH2201- 学好React </h1>
        );
    }
}

// mapStateToProps()   输入逻辑  传递state   父改子    mapStateToProps会订阅 Store
const funA = (state)=>{
    console.log(state)
    return {
        data:state.data, 
        count:state.data.count,
        ...state.data, 
    }
}
// mapDispatchToProps() 输出逻辑  反向的props   子改父 
const funB = (dispatch )=>{
    return {

    }
}
// export default connect(funA,funB)(MyCounter)   // 容器组件 

export default MyCounter
