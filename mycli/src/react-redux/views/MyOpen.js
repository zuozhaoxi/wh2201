


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeCity, changeCount, changeMsg, countAdd, getTestAsync, getGoodsAsync, delFlag  } from '../actions';
import MyBtn from './../../components/MyBtn';

// @observer 
// @inject 
// @connect 类组件的装饰器  
// 只能写在类组件前面 用于给类组件混入函数逻辑
@connect(
    (state)=>({   // store.getState 
        // msg:state.data.msg 
        msg:state.getIn(['data','msg'])
    }),
    (dispatch)=>{  //store.dispatch 
        return {
            countAdd:()=>dispatch(countAdd),
            changeCount:n=>dispatch(changeCount(n)),
            changeCity:city=>dispatch(changeCity(city)),
            changeMsg:msg=>dispatch(changeMsg(msg)),
            getTest:()=>dispatch(getTestAsync()),
            getGoods:()=>dispatch(getGoodsAsync()),
            delFlag:()=>dispatch(delFlag())
        }
    }
)
class MyOpen extends Component {
    render() {
        console.log(this)
        const {
            countAdd,
            changeCount,
            changeCity,
            msg,
            changeMsg,
            getTest,
            getGoods,
            delFlag
        } = this.props
        return (
            <div style={{border:"1px solid deeppink",borderRadius:15,minHeight:200,marginTop:20,padding:20}}>
                <h2>负责修改数据的地方 </h2>
                <MyBtn text="countAdd" onClick={countAdd}  ></MyBtn>
                <MyBtn text="changeCount-60" onClick={()=>changeCount(60)  }  ></MyBtn>
                <MyBtn text="云南大理" onClick={()=>changeCity('云南大理')  }  ></MyBtn>
                <p>
                    <input type="text" value={msg}  ref={el=>this.text=el} onChange={()=>changeMsg(this.text.value )}   />
                </p>
                <MyBtn text="获取test " onClick={getTest }  ></MyBtn>
                <MyBtn text="获取Goods " onClick={getGoods }  ></MyBtn>
                <MyBtn text="删除Flag " onClick={delFlag }  ></MyBtn>
            </div>
        );
    }
}

export default MyOpen
// export default connect(
//     (state)=>({   // store.getState 
//         msg:state.data.msg 
//     }),
//     (dispatch)=>{  //store.dispatch 
//         return {
//             countAdd:()=>dispatch(countAdd),
//             changeCount:n=>dispatch(changeCount(n)),
//             changeCity:city=>dispatch(changeCity(city)),
//             changeMsg:msg=>dispatch(changeMsg(msg)),
//             getTest:()=>dispatch(getTestAsync()),
//             getGoods:()=>dispatch(getGoodsAsync())
//         }
//     }
// )(MyOpen)

