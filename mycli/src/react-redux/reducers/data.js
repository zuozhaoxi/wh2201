
import {fromJS,toJS} from 'immutable'

const defaultState = fromJS({   // immutable 
    count:2000,
    list:[],
    city:"海南三亚",
    test:"ARe you oK",
    msg:"WH2201 - daydayup ",
    flag:true 
})

export const data = (state=defaultState,action)=>{
    const {type,payload} = action;
    console.log(action)

    
    switch(type){
        case "countAdd":
            // state.count++
            // return {...state}
            // return state.set('count',state.get('count')+1)
            // return state.set("count",state.toJS().count + 1)
            return state.update('count',x=>x+1)
            break;
        case "changeCount":
            // state.count+=payload;
            // return {...state}

            return state.update('count',x=>x+payload)
            break;

        case "changeCity":
            // return {...state,city:payload}
            // return Object.assign(state,{city:payload})
            return state.set('city',payload) 
            break;

        case "changeMsg":
            return state.set('msg',payload) 
            break;

        case "getTestAsync":
            return state.set('test',payload) 
            break;
       
        case "getGoodsAsync":
            return state.set('list',payload) 
            break;

        case "delFlagAction":
            console.log("delFlagAction")
            return state.delete("flag")
            break;
            

        default:
            return state;
            break;
    }
}




// export const data = (state=defaultState,action)=>{
//     const {type,payload} = action;
//     console.log(action)

    
//     switch(type){
//         case "countAdd":
//             state.count++
//             return {...state}
//             break;
//         case "changeCount":
//             state.count+=payload;
//             return {...state}
//             break;

//         case "changeCity":
//             console.log(payload)
//             return {...state,city:payload}
//             // return Object.assign(state,{city:payload})
//             break;

//         case "changeMsg":
//             return {...state,msg:payload}
//             break;

//         case "getTestAsync":
//             return {...state,test:payload}
//             break;
       
//         case "getGoodsAsync":
//             return {...state,list:payload}
//             break;
            
//         default:
//             return state;
//             break;
//     }
// }
