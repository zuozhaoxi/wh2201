

import React, { Component } from 'react';
import MyCounter from './views/MyCounter';
import MyOpen from './views/MyOpen';

import {Provider} from "react-redux"
import store from './store'

class ReactRedux extends Component {
    render() {
        return (
            // 让所有的子组件通过 context 获取 store state 
            <Provider store={store} >
                <div style={{padding:15}}>
                    <h2>react-redux 入门学习 </h2>
                    <MyCounter/>
                    <MyOpen/>
                </div>
            </Provider>
        );
    }
}

export default ReactRedux;
