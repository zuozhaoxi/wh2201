





# React-Redux的用法  ( 对 redux 原理没有任何改造 )
还是遵循 redux 的原理  先修改 action  再去修改 redux 最后发送  dispatch action 
只是对 组件 进行了改造 讲它一拆而二 (单组件 => (UI组件 + 容器组件 ))

UI 为子组件
容器 为父组件 

父子组件通信  
父改子
子改父



# 复用其他组件得到的新组件 就是高阶组件  HOC 
# https://www.jianshu.com/p/4143a6296994    组件接收其他组件当做参数  复用生成新的组件   此类组件就是高阶组件
# 高阶组件 vs Mixins   

# 高阶函数    connect 
# 高阶组件    HOC  Higher-order component  (Vue mixins)
1. 代码复用，代码模块化
2. 渲染劫持, 操作state
3. Props 增删改


# 升级改造
一，UI组件
React-Redux将所有组件组装成两大类：UI组件（呈现组件）和容器组件（容器组件）。

UI组件有以下几个特征。
只负责UI的呈现，不带有任何业务逻辑
没有状态（即不使用this.state这个变量）
所有数据都由参数（this.props）提供
不使用任何Redux的API


二，容器组件    高阶组件   组件接收其他组件当做参数  复用生成新的组件   此类组件就是高阶组件
负责管理数据和业务逻辑，不负责UI的呈现
带有内部状态
使用Redux的API


UI组件负责UI的呈现，容器组件负责管理数据和逻辑。

外面是一个容器组件，里面包了一个UI组件   容器父组件  UI子组件 

所有的UI组件都由用户提供，容器组件则是由React-Redux自动生成


三，connect()  高阶函数  withRouter    函数接收一个组件当做参数 此类函数就是高阶函数  复用组件

connect方法接受两个参数：mapStateToProps和mapDispatchToProps
mapStateToProps  负责输入逻辑   state映射到UI组件的参数（props）   父改子   state => props
mapDispatchToProps  负责输出逻辑 把事件通过 props 发送出去         子改父   反向props 


四、mapStateToProps()  传递state   父改子 
mapStateToProps是一个函数。它的作用就是像它的名字那样，建立一个从（外部的）state对象到（UI 组件的）props对象的映射关系


mapStateToProps会订阅 Store，每当state更新的时候，就会自动执行，重新计算 UI 组件的参数，从而触发 UI 组件的重新渲染  
store.subscirbe 没有用了 


五、mapDispatchToProps()  反向的props   子改父

mapDispatchToProps是connect函数的第二个参数，用来建立 UI 组件的参数到store.dispatch方法的映射  
父组件 修改 state的 函数 通过 props 映射给子组件 子组件触发  修改 state


六 Provider   让所有的组件拿到 store 
React-Redux 提供Provider组件，可以让容器组件拿到state     
通过 context 把 state 传递给 容器组件  隔空传递 




# 类库    set([...data])
# immutable   https://blog.csdn.net/sinat_17775997/article/details/73603797
想像操作不可变对象一样操作 可变对象 (对象和数组 )

# immutable 没有改redux 原理 它只是对数据进行改造升级 (对象数组 浅拷贝)  (完美的操作对象和数组)

mutable  可变对象  Array Object 浅拷贝  修改可变对象 响应式系统可能检测到数据更改 从而触发视图更新 

immutable  不可变对象   Number/String 深拷贝  修改不可变对象  响应式系统一定检测到数据更改 从而触发视图更新 

immutable.js  ==>  解决 修改对象或者数组 视图不刷新 

把对象数组这些可变对象 更新为 不可变对象    return {...state}

Persistent data structure （持久化数据结构）
一个数据，在被修改时，仍然能够保持修改前的状态

structural sharing （结构共享）
immutable使用先进的tries(字典树)技术实现结构共享来解决性能问题，当我们对一个Immutable对象进行操作的时候，ImmutableJS会只clone该节点以及它的祖先节点，其他保持不变，这样可以共享相同的部分，大大提高性能。
共享大部分的节点数据  结构共享

support lazy operation （惰性操作）
延迟更新操作 到时候一次性更新所有的更改

immutable.js  提供了十余种不可变的类型（List，Map，Set，Seq，Collection，Range等）

List ==> Array  
Map  ==> Object 
Set ES6  


# API 文档 
//Map()  原生object转Map对象 (只会转换第一层，注意和fromJS区别)
immutable.Map({name:'danny', age:18,a:{b:1}})

//List()  原生array转List对象 (只会转换第一层，注意和fromJS区别)
immutable.List([1,2,3,4,5])

//fromJS()   原生js转immutable对象  (深度转换，会将内部嵌套的对象和数组全部转成immutable)
immutable.fromJS([1,2,3,4,5])    //将原生array  --> List
immutable.fromJS({name:'danny', age:18})   //将原生object  --> Map

//toJS()  immutable对象转原生js  (深度转换，会将内部嵌套的Map和List全部转换成原生js)
immutableData.toJS();

//查看List或者map大小  
immutableData.size()  或者 immutableData.count()

// is()   判断两个immutable对象是否相等
immutable.is(imA, imB);

//merge()  对象合并
var imA = immutable.fromJS({a:1,b:2});
var imB = immutable.fromJS({c:3});
var imC = imA.merge(imB);
console.log(imC.toJS())  //{a:1,b:2,c:3}

//增删改查（所有操作都会返回新的值，不会修改原来值）
var immutableData = immutable.fromJS({
    a:1,
    b:2，
    c:{
        d:3
    }
})
var data1 = immutableData.get('a') //  data1 = 1  
var data2 = immutableData.getIn(['c', 'd']) // data2 = 3   getIn用于深层结构访问
var data3 = immutableData.set('a' , 2);   // data3中的 a = 2
var data4 = immutableData.setIn(['c', 'd'], 4);   //data4中的 d = 4
var data5 = immutableData.update('a',function(x){return x+4})   //data5中的 a = 5
var data6 = immutableData.updateIn(['c', 'd'],function(x){return x+4})   //data6中的 d = 7
var data7 = immutableData.delete('a')   //data7中的 a 不存在
var data8 = immutableData.deleteIn(['c', 'd'])   //data8中的 d 不存在


immutable.js的优缺点
优点
降低mutable(引用数据类型)带来的复杂度
节省内存
拥抱函数式编程

缺点 
容易与原生对象混淆： obj.username  =>  obj.get('username')
由于api与原生不同，混用的话容易出错

cnpm i immutable redux-immutable -S
