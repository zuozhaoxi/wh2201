
import Axios from './../../utils/ajax';


export const countAdd = {
    type:"countAdd"
}

export const changeCount = payload=>{
    return {
        type:"changeCount",
        payload
    }
}

export function changeCity(payload){
    return {
        type:"changeCity",
        payload 
    }
}

export function changeMsg(payload){
    return {
        type:"changeMsg",
        payload 
    }
}

export async function getTestAsync(){
    let res = await Axios.get("http://localhost:3800/api/test",{})
    return {
        type:"getTestAsync",
        payload:res.data.msg 
    }
}

export async function getGoodsAsync(){
    let res = await Axios.get("http://localhost:3800/goodslist",{params:{limit:10}})
    return {
        type:"getGoodsAsync",
        payload:res.data.result  
    }
}

export function  delFlag(){
    return {
        type:"delFlagAction"
    }
}