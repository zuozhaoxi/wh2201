import { fromJS ,  toJS} from "immutable";
import { action, observable } from "mobx";


class MyInfo{
    @observable userInfo = fromJS({
        username:"zuozuomu",
        age:28,
        word:"WH2201-daydayup"
    })

    @action changeUserInfo = (payload)=>{
        this.userInfo = payload?fromJS({
            ...this.userInfo.toJS(),
            ...payload 
        }): fromJS({})
    }
}

export default new MyInfo()