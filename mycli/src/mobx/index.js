

import React, { Component } from 'react';
import MyCounter from './views/MyCounter';
import {observer } from "mobx-react"


import { Provider } from 'mobx-react';
import { store } from './store';

class MobxIndex extends Component {
    render() {
        return (
            <Provider {...store}>
                <div style={{width:'100%',padding:15}}>
                    <h2>Mobx 入门学习 </h2>
                    <hr/>
                    <MyCounter/>
                </div>
            </Provider>
            
        );
    }
}

export default MobxIndex;
