
import { observable ,action ,autorun, computed  } from 'mobx'
import {fromJS} from 'immutable'
import Axios from './../utils/ajax';

class MyData{
    // state 可观察状态 
    @observable count = 2000;
    @observable city = "美丽的武汉"
    @observable msg = "wh2201 - daydayup"
    @observable data = fromJS({
        test:"test-test-test",
        flag:true,
        list:[],
    })


    @action countAdd = ()=>{
        this.count++
        console.log(this.count)
    }

    @action countDesc = ()=>{
        this.count--;
    }

    @action changeCount = payload=>{
        this.count+=payload
    }

    @action changeCity = payload=>{
        this.city = payload; 
    }

    @action changeMsg = payload =>{
        this.msg = payload 
    }

    @action changeTestAsync = async ()=>{
        let res = await Axios.get("http://localhost:3800/api/test",{})
        // console.log(res)
        this.data = this.data.set("test",res.data.msg);
    }

    @action getGoodsAsync = async (payload)=>{
        let res = await Axios.get("http://localhost:3800/goodslist",{params:payload})
        this.data = this.data.set("list",res.data.result)
    }

    @action changeFlag=()=>{
        this.data = this.data.update('flag',x=>!x);
    }

    @computed get newCount(){
        return this.count * 10 + 888;
    }

    // newCount 的值自己主动改变 
    set newCount(v){
        console.log(v)
        this.count = v;
    }

    @action changeNewCount=(payload)=>{
        this.newCount = payload; 
    }


}

export default new MyData()