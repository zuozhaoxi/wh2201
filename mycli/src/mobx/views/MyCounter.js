


import React, { Component } from 'react';

import MyBtn from './../../components/MyBtn';
import {inject, observer} from "mobx-react"
// console.log(mydata)

// 引入太磨叽 
// import myinfo from '../info'
// import mydata from '../data'



// 订阅组件内部state 
@inject('mydata','myinfo')  
@observer    
class MyCounter extends Component {
    render() {
        console.log(this.props)
        const {
            count,
            city,
            msg, 
            data,
            newCount,
            countAdd,
            countDesc,
            changeCount,
            changeCity,
            changeMsg,
            changeTestAsync,
            getGoodsAsync,
            changeFlag,
            changeNewCount
        }
        = this.props.mydata
        //  = mydata
        const {
            userInfo,
            changeUserInfo
        } = this.props.myinfo 
        // = myinfo;
        return (
            <div>
               {
                   data.get('flag') &&  <div>
                   <h2>Mobx 实现简单的计数器 </h2>
                   <h2>count --- {count}</h2>
                   <h2>newCount --- {newCount} </h2>
                   <h2>city -- {city} </h2>
                   <h2>msg --- {msg} </h2>
                   <h2>test -- {data.get('test')} </h2>
                   <h2>用户名 ---- {userInfo.get("username")}</h2>
                   <h2>age ---- {userInfo.get("age")}</h2>
                   <h2>word ---- {userInfo.get("word")}</h2>
                   <h2>phone ---- {userInfo.get("phone")}</h2>



               </div> 
               }
                <hr/>

                <MyBtn text="countAdd" onClick={countAdd} ></MyBtn>
                <MyBtn text="countAdd" onClick={countDesc} ></MyBtn>
                <MyBtn  text="changeCount" onClick={()=>changeCount(200)} />
                <MyBtn  text="想去北京天安门" onClick={()=>changeCity('北京天安门')} />

                <p>
                    <input type="text" onChange={()=>changeMsg(this.text.value)}   ref={el=>this.text=el} value={msg}/>
                </p>

                <MyBtn  text="异步修改 test " onClick={changeTestAsync} />
                <MyBtn  text="异步获取 goodlist " onClick={()=>getGoodsAsync({limit:7})} />
                <MyBtn  text="删除flag  " onClick={changeFlag} />
                <MyBtn  text="changeNewCount  " onClick={()=>changeNewCount(1000)} />
                <MyBtn  text="修改用户age   " onClick={()=>changeUserInfo({age:16})} />
                <MyBtn  text="添加手机号  " onClick={()=>changeUserInfo({phone:15012348899})} />
                <MyBtn  text="清除  " onClick={()=>changeUserInfo(null)} />
                <div>
                    {
                        data.get("list").map((l,i)=>{
                            return (
                                <p key={i}>
                                    {l.name}
                                </p>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}

export default MyCounter;
