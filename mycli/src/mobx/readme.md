

# bug  不能更新的bug   安装重启服务器   再降版本 
#  https://blog.csdn.net/canduecho/article/details/79390207
#  https://cn.mobx.js.org/
#  https://www.jianshu.com/p/505d9d9fe36a


# 安装
cnpm i mobx mobx-react -S
cnpm i mobx@5 mobx-react@6 -S
    
# 单向数据流  从 mobx 流向 视图 view 

# 它由几个部分组成：Actions、State(全局状态) 、Computed Values、Reactions(响应式 )  

# 订阅组件内部的 state 每当state 改变会从新计算求值  重新刷新页面   @observer 

# mobx 原理

通过事件驱动（UI 事件、网络请求…）触发 Actions
在 Actions 中修改了 State (全局状态) 中的值  
然后根据新的 State 中的数据计算出所需要的计算属性（computed values）值 
最后更新修改到 UI视图层 


# 可观察状态（State） 
observable   

@observable 接受任何类型的 js 值（原始类型、引用、纯对象、类实例、数组和、maps），observable 的属性值在其变化的时候 mobx 会自动追踪并作出响应


#计算属性值（Computed Values） 
# get   取值  
# set   监听值的修改  


#动作（Action） 修改数据的地方 
只能是 箭头函数  保留this 指向   专门修改 可观察状态（State）  =>  observable
修改 状态之后 mobx 自动跟踪数据 并且修改数据 修改页面



#
@observer 观察者  
observer 会订阅组件的 可观察状态 state (observable)
如果组件内的 可观察状态 state 通过 action 被修改 了 
observer 观察者自动去刷新当前视图  启动数据响应式系统  ( store.subscribe )


#mobx原理   (单向数据+ 集中式管理组件的状态 )
# 定义 class 定义一些可观察状态    把这些可观察状态呈现视图中  
# 定义 action  视图通过点击事件或者ajax请求 来执行action  
# action 里面去修改 可观察状态  
# 组件会添加 观察者 observer , 它们可以订阅 组件内部的 可观察状态 , 当状态改变时,主动去刷新视图  



# observable     定义数据
# action         修改数据
# observer       观察数据 
