
// 路由核心 单页面应用  匹配成功对应的Path 加载对应的组件到对应的视图内 

// 1. 路由基本配置流程
// 2. 路由嵌套
// 3. 路由传参   query params 

// V6 倾向于 纯函数组件
// HashRouter   Hash 
// BrowserRouter  History 
// Routes 匹配优先加载成功的路由    承担了router-view 
// Route  接受组件的视图  负责匹配组件 
// element 接受路由对应的组件  
// Navigate 路由重定向 
// Outlet  router-view  加载路由 
// Link 导航 
// NavLink  导航  有高亮的class 样式 
// useLocation 获取 location 对象 
// useSearchParams  获取url 里面的 search 路由查询参数 
// useParams 获取 params 参数  路由参数 
// useNavigate 路由跳转  router.push  
// useRoutes 路由配置 组件化 改造成 编程函数化 


import React, { Component } from 'react';
import {HashRouter as Hash,  BrowserRouter as History  , Routes , Route , useNavigate ,Navigate, Outlet, Link, NavLink , useLocation, useSearchParams, useParams ,useRoutes} from "react-router-dom"

import MyBtn from "../components/MyBtn"

class RouterIndex extends Component {
    render() {
        return (
            <div>
                <h2>v6 路由的学习</h2>
                <MyRouter/>
            </div>
        );
    }
}

class MyRouter extends Component{
    render(){
        return (
            <Hash>
                <MainRouter></MainRouter>
            </Hash>
        )
    }
}

class MainRouter extends Component{
    render(){
        return (
            <NewRouter/>
            // <Routes>
            //     <Route path="/"  element={<MyApp/>  } >
            //         <Route  path="/" replace element={<Navigate to='/home' />} />
            //         <Route  path="/home"  element={<Home/> }/>
            //         <Route  path="/mine"  element={<Mine/> }/>
            //         <Route  path="/news"  element={<News/> }/>
            //         <Route  path="/find/:address/:who"  element={<Find/> }/>
            //         <Route  path="/user" element={<User/>}> 
            //             <Route path="/user/" replace element={<Navigate to="/user/list"/>}/>
            //             <Route  path="/user/list" element={<UserList/> }/>
            //             <Route  path="/user/add" element={<UserAdd/> }/>
            //             <Route  path="/user/data" element={<UserData/> }/>
            //         </Route>

            //         <Route path="/goods/:type/:limit" element={ <Goods />}/>

            //         <Route path="/advise/:type" element={ <NewAdvise />}/>

            //     </Route>
            //     <Route path="/login" element={<Login/> } />
            //     <Route path="/register" element={ <Register/> }  />
            //     <Route path="/findpass"  element={ <FindPass/> }/>
                
            //     <Route path="/503"  element={ <BadService /> }/>
            //     <Route path="/404"  element={ <NotFound /> }/>
                
            //     <Route path="*" replace  element={<Navigate to="/404" />}/>
            // </Routes>
        )
    }
}

function NewRouter(){
    const routes = [
        {
            path:"/",
            element:<MyApp/>,
            children:[
                {
                    path:"/",
                    element:<Navigate replace to="/home" />
                },
                {
                    path:"/home",
                    element:<Home/>,
                },
                {
                    path:"/mine",
                    element:<Mine/>,
                },
                {
                    path:"/news",
                    element:<News/>,
                },
                {
                    path:"/advise/:type",
                    element:<NewAdvise/>,
                },
                {
                    path:"/goods/:type/:limit",
                    element:<Goods/>,
                },
                {
                    path:"/find/:address/:who",
                    element:<Find />,
                },
                {
                    path:"/user",
                    element:<User />,
                    children:[
                        {
                            path:"/user/",
                            element:<Navigate replace to="/user/list" />
                        },
                        {
                            path:"/user/list",
                            element:<UserList />,
                        },
                        {
                            path:"/user/add",
                            element:<UserAdd />,
                        },
                        {
                            path:"/user/data",
                            element:<UserData  />,
                        },
                    ]
                }
            ]
        },
        {
            path:"/login",
            element:<Login/>
        },
        {
            path:"/register",
            element:<Register/>
        },
        {
            path:"/findpass",
            element:<FindPass/>
        },
        {
            path:"/404",
            element:<NotFound/>
        },
        {
            path:"/503",
            element:<BadService/>
        },
        {
            path:"*",
            element:<Navigate replace to="/404" />
        }
    ]
    return useRoutes(routes)
}


class MyApp extends Component{
    render(){
        return (
            <div style={{border:"1px solid orange",borderRadius:15,minHeight:600,marginTop:20,padding:20}}>
                <h2>这是一个 主页面 Myapp - main </h2>
                <div style={{border:"1px solid deeppink",borderRadius:15,minHeight:400,marginTop:20,padding:20}}>
                    <Outlet/>
                </div>
                <div>
                    <NavLink to="/home" > 首页</NavLink>
                    <NavLink to="/news?time=0527&name=王心凌" > 新闻</NavLink>
                    <NavLink to="/mine?username=zkl&phone=13012341234" > 我的</NavLink>
                    <NavLink to="/find/东湖绿道/WH2201" > 发现 - 东湖绿道 </NavLink>
                    <NavLink to="/find/藏龙岛生态公园/家人" > 发现 - 藏龙岛生态公园 </NavLink>

                    <NavLink to="/goods/鞋子/20" > 商品列表 - 鞋子</NavLink>
                    <NavLink to="/goods/裤子/100" > 商品列表 - 裤子</NavLink>

                    <NavLink to="/advise/生活意见?name=zuozuomu" > 意见数据  </NavLink>
                    <NavLink to="/user" > 用户管理</NavLink>
                </div>
            </div>
        )
    }
}

function Home(){
    const navigate = useNavigate()  // 路由跳转
    const gotoAdvise = ()=>{
        // navigate("/advise/社区意见?name=拉拉老师")
        navigate("/advise/千锋意见?name=daZuo")
    }

    const goback = ()=>{
        navigate(-1)
    }
    return (
        <div>
            <h2> Home - Home - 首页 </h2>
            <MyBtn text="进入意见"  onClick={gotoAdvise }  ></MyBtn>
            <MyBtn text="返回上一级"  onClick={goback }  ></MyBtn>

        </div>
    )
}
const Mine = ()=>{
    const [searchParams] = useSearchParams()
    const location = useLocation()
    return (
        <div>
            <h2>Mine - Mine - MIne </h2>
            <div>
                <h2>username == {searchParams.get('username')}</h2>
                <h2>phone == {searchParams.get('phone')}</h2>
                <hr/>

                <h2>username == {new URLSearchParams(location.search).get('username')} </h2>
            </div>
        </div>
    )
}

const News = ()=>{
    const location = useLocation()  // 获取路由url参数信息 
    console.log(location)
    const [searchParams]  = useSearchParams()  // 获取search   query {}
    // console.log(searchParams)
    return (
        <div>
            <h2>News - News - News -新闻 </h2>
            <h2>time === {new URLSearchParams(location.search).get('time')}</h2> 
            <h2>name === { new URLSearchParams(location.search).get("name")  }</h2> 
            <hr/>

            <h2>time === {searchParams.get('time')}</h2> 
            <h2>name === {searchParams.get('name')}</h2> 

        </div>
    )
}
const Find = ()=>{
    const params = useParams()
    console.log(params)
    return (
        <div>
            <h2>Find - Find - Find - 发现美丽 </h2>
            <hr/>
            <h2>
                adderss == {params.address}
            </h2>
            <h2>
                who == {params.who}
            </h2>
        </div>
    )
}

const Goods = ()=>{
    const params = useParams()

    return (
        <div>
            <h2> Goods - Goods - 商品列表  </h2>
            <h2> type === {params.type}</h2>
            <h2> limit 数量 --- {params.limit}</h2>
        </div>
    )
}
const NewAdvise = ()=>{
    const params = useParams()
    const [searchParams] = useSearchParams()
    const navigate = useNavigate()
    return (
        <Advise navigate={navigate} params={params} searchParams={searchParams} />
    )
}
class Advise extends Component{
    render(){
        const {params,searchParams,navigate} = this.props 
        return (
            <div>
                <h2> Advise - Advise - 意见 - new  </h2>
                <h2>type ---- {params.type}</h2>
                <h2>name ---- {searchParams.get('name')}</h2>
                <hr/>
                <MyBtn text="进入用户管理" onClick={()=>navigate('/user')}  ></MyBtn>
            </div>
        )
    }
}
function User(){
    return (
        <div>
            <h2>User - user  -user</h2>
            <h2>用户管理</h2>
            <hr/>
            <div>
                <NavLink to="/user/data">用户分析</NavLink>
                <NavLink to="/user/add">用户新增</NavLink>
                <NavLink to="/user/list">用户列表</NavLink>
            </div>
            <div style={{border:"1px solid green",borderRadius:15,minHeight:300,marginTop:20,padding:20}}>
                <Outlet/>
            </div>
        </div>
    )
}
function UserList(){
    return (
        <div>
            <h2>UserList - 用户列表</h2>
        </div>
    )
}

function UserAdd(){
    return (
        <div>
            <h2>UserAdd - 用户添加</h2>
        </div>
    )
}

function UserData(){
    return (
        <div>
            <h2>UserData - 用户数据分析</h2>
        </div>
    )
}

class Login extends Component{
    render(){
        // const location = useLocation() 只能用于纯函数组件 
        return (
            <div>
                <h2>Login -  Login - Login</h2>
            </div>
        )
    }
}


const Register = ()=>{
    return (
        <div>
            <h2> Register - 注册  - 注册 </h2>
        </div>
    )
}

const FindPass = ()=>{
    return (
        <div>
            <h2>FindPass - 找回密码 </h2>
        </div>
    )
}

function NotFound(){
    return (
        <div>
            <h2> NotFound - NotFound - 404  </h2>
        </div>
    )
}

function BadService(){
    return (
        <div>
            <h2> BadService - BadService - 503   </h2>
        </div>
    )
}
export default RouterIndex;
