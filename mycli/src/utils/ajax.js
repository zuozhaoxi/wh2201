
import qs from "qs"

// 模拟封装Axios .then .catch 
// Promise 
const pro = new Promise((resolve,reject)=>{
    if(Math.random() > 0.5){
        resolve("端午节快乐")
    }else{
        reject('儿童节快乐')
    }
}) 

pro.then(res=>{console.log(res)}).catch(err=>console.log(err))


const Axios = {
    get(url,options){
        
        return new Promise((resolve,reject)=>{
            function handler(){
                // this=>  client ;
                // readyState 0 1 2 3 4 
                // 0 请求未初始化
                // 1 请求已经建立但是还没有发送
                // 2 请求已经发送 正在处理
                // 3 请求正在处理 准备响应数据
                // 4 请求已经完成  数据全部响应
                // status  100 200 300 400 500 
                if(this.readyState!=4){
                    return ;
                }
                if(this.status==200){
                    resolve({data:JSON.parse(this.responseText)})
                }else{  
                    reject(this.statusText)
                }
            }
            let {params,headers} = options;
            params =  qs.stringify(params)  // queryString qs 
            console.log(params)
            const client = new XMLHttpRequest()
            client.open("GET",url+"?"+params,true);
            // client.setRequestHeader(headers)
            client.send()
            client.onreadystatechange = handler;
        })
    },
    post(url,options){
        return new Promise((resolve,reject)=>{
            function handler(){
                // this=>  client ;
                // readyState 0 1 2 3 4 
                // 0 请求未初始化
                // 1 请求已经建立但是还没有发送
                // 2 请求已经发送 正在处理
                // 3 请求正在处理 准备响应数据
                // 4 请求已经完成  数据全部响应
                // status  100 200 300 400 500 
                if(this.readyState!=4){
                    return ;
                }
                if(this.status==200){
                    resolve({data:JSON.parse(this.responseText)})
                }else{  
                    reject(this.statusText)
                }
            }
            let {params,headers,data } = options;
            params =  qs.stringify(params)  // queryString qs 
            console.log(params)
            const client = new XMLHttpRequest()
            client.open("POST",url+"?"+params,true);
            client.setRequestHeader('content-type','application/x-www-form-urlencoded') // post 提交的body数据 
            client.send(qs.stringify(data))   
            client.onreadystatechange = handler;
        })
    }
}

export default Axios;