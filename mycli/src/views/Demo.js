


import React, { Component } from "react"

// Vue.component 
export default class Demo extends React.Component{
    render(){ 
        return (
            <div>
                <hr/>
                <h2> Demo - Demo - Demo  </h2>
                <h2>这是 React class 组件的写法</h2>
                <hr/>
                <Ones/>
            </div>
        )
    }
} 

class Ones extends React.Component{
    render(){
        return (
            <div>
                <h2> WH2201 - DayDayUp </h2>
                <MyNav></MyNav>
                <MyMenu></MyMenu>
            </div>
        )
    }
}

class MyNav extends Component{
    render(){
        return (
            <div>
                <h2> MyNav - MyNav -  导航 </h2>
            </div>
        )
    }
}

class MyMenu extends Component{
    render(){
        return (
            <div>
                <h2>MyMenu - MyMenu - 菜单  </h2>
            </div>
        )
    }
}

class Person{
    constructor(){

    }
}

class Student extends Person{
    constructor(){
        super()
    }
}