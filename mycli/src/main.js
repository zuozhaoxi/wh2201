

// import {word , msg } from "~/demo"
// import img2 from "@/assets/images/img2.jpg";

// console.log("这是 自定义脚手架的 主入口文件  - react ")

// var app = document.getElementById("app")

// app.innerHTML += `<h2>WH2201 - daydayup</h2>`
//                 + `<h2>WH2201 - 天道酬勤</h2>` 
//                 + `<h2>WH2201 - 人人拿一个满意的 offer </h2>` 
//                 + `<h2>${word } </h2>` 
//                 + `<h2>${msg } </h2>` 
//                 + `<img src='${img2}'  />` 


import "./styles/index.scss";
import MyRoot from './views/index';
import React from "react"
import ReactDOM, {createRoot} from 'react-dom/client'






// ReactDOM.render(根组件,根节点)
// ReactDOM.render(<MyRoot/> , document.getElementById("app") )  react17 

// const root =  createRoot(document.getElementById("app"))
// root.render(<MyRoot/> )


// redux 
// import ReduxIndex from "./redux";
// import store from './redux/store'
// const root =  createRoot(document.getElementById("app"))
// root.render(<ReduxIndex/> )

// store.subscribe(()=> root.render(<ReduxIndex/> ) )    // state 改变 这个函数就执行 
// Store 允许使用store.subscribe方法设置监听函数，一旦 State 发生变化，就自动执行这个函数


// react-redux
// import ReactRedux from "./react-redux";
// const root =  createRoot(document.getElementById("app"))
// root.render(<ReactRedux/> )


// mobx 
// import MobxIndex from './mobx/index';
// const root =  createRoot(document.getElementById("app"))
// root.render(<MobxIndex/> )

// Hooks
import HookIndex from "./Hooks";
const root =  createRoot(document.getElementById("app"))
root.render(<HookIndex/> )