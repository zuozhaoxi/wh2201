


import React, { Component } from 'react';
import MyBtn from './../../components/MyBtn';
import store  from '../store';
import { CHANGEACTION, changeCity, changeDataFlag, changeDataTest, changeMsg, changeWord, COUNTDESC, getGoodList, getTestAsync } from '../actions';

class MyDemo extends Component {
    render() {
        return (
            <div>
                <h2>我要修改数据了的  - MyDemo</h2>
                <p>
                    <MyBtn text="countAdd" onClick={()=>store.dispatch({type:'COUNTADD'})}  > </MyBtn>
                    <MyBtn text="countDesc" onClick={()=>store.dispatch(COUNTDESC) }  > </MyBtn>
                    <MyBtn text="changeCount 25" onClick={ ()=>store.dispatch(CHANGEACTION(25))  }  > </MyBtn>
                </p>
                <p>
                    <MyBtn text="想去 荆门京山 " onClick={()=>store.dispatch(changeCity('荆门京山'))}  > </MyBtn>
                </p>
                <p>
                    <input type="text" ref={el=>this.text=el} onChange={()=>store.dispatch(changeMsg(this.text.value) )}   />
                </p>
                <p>
                    <MyBtn text="小程序也很简单 - so easy" onClick={()=> store.dispatch(changeWord('小程序也很简单 - so easy')) }  > </MyBtn>
                </p>
                <p>
                    <MyBtn text="changeDataTest" onClick={()=>store.dispatch(changeDataTest("周末和晚上都要认真背诵面试题了..."))}  > </MyBtn>
                    <MyBtn text="修改flag" onClick={()=>store.dispatch(changeDataFlag())}  > </MyBtn>
                    <MyBtn text="获取异步的 test " onClick={()=>store.dispatch(getTestAsync())}  > </MyBtn>
                    <MyBtn text="获取异步的 glist  " onClick={()=>store.dispatch(getGoodList())}  > </MyBtn>
                </p>
            </div>
        );
    }
}

export default MyDemo;
