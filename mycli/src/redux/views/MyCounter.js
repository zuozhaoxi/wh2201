


import React, { Component } from 'react';
import store from '../store'

class MyCounter extends Component {
    render() {
        const {state} = this.props
        const {count,city, msg ,word ,  data :{mv,test,flag,glist  } } = store.getState()   // state 获取state   直接从store  每次都会获取新的state 
        return (
            flag ? <div  style={{border:"1px solid deeppink",borderRadius:15,minHeight:200,marginTop:20,padding:20}}>
                <h2>Redux 实现简单的计数器 </h2>
                <h2>count --- {state.count} -- {count }</h2>
                <h2>city --- {state.city }  -- {city }</h2>
                <h2>msg  -- {msg} </h2>
                <h2>word ---- {word} </h2>
                <h2>test --- {test} </h2>
                <div>
                    {
                        glist.map((item,index)=>(
                            <p key={index}> {item.name} </p>
                        ))
                    }
                </div>
            </div> : <div>组件被隐藏了</div>
        );
    }
}

export default MyCounter;
