

import Axios from './../../utils/ajax';
// 改变 State 的唯一办法，就是使用 Action。它会运送数据到 Store

// store.dispatch()是 View 发出 Action 的唯一方法。
export const countDescType = "zzzzzzxxxxxxxxxxxxxcccccccccccccccc"  // action 类型 常量 

export const COUNTDESC = {
    type:countDescType
}

// Action creators 创造器 

export function CHANGEACTION(payload){
    return {
        type:"CHANGEACTION",
        payload,
    }
}

export const  changeCity = (payload)=>{
    return {
        type:"changeCity",
        payload 
    }
}

export const changeMsg = payload=>({
    type:"changeMsg",
    payload 
})

export const changeWord = payload=>{
    return {
        type:"changeWord",
        payload 
    }
}

export function changeDataTest(payload){
    return {
        type:"changeDataTest",
        payload
    }
}


export function changeDataFlag(payload){
    return {
        type:"changeDataFlag",
        payload
    }
}


// Action  中间件改造了 store.dispatch 
export async function getTestAsync(){
    let res = await  Axios.get("http://localhost:3800/api/test",{})
    return {
        type:"changeDataTest",
        payload:res.data.msg 
    }
}

export const getGoodList = async ()=>{
    let res = await Axios.get("http://localhost:3800/goodslist",{
        params:{limit:10}
    })
    return {
        type:"getGoodList",
        payload:res.data.result 
    }
}