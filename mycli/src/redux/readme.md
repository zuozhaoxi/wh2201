

redux    (全局的状态管理架构)      (vuex)

# Redux  单向数据流 
# https://www.ruanyifeng.com/blog/2016/09/redux_tutorial_part_one_basic_usages.html

# 安装 
cnpm i redux react-redux -S

# redux 不推荐使用场景
用户的使用方式非常简单
用户之间没有协作
不需要与服务器大量交互，也没有使用 WebSocket
视图层（View）只从单一来源获取数据

# redux 推荐使用    
用户的使用方式复杂
不同身份的用户有不同的使用方式（比如普通用户和管理员）
多个用户之间可以协作
与服务器大量交互，或者使用了WebSocket
View要从多个来源获取数据


# 组件 角度考虑
某个组件的状态，需要共享
某个状态需要在任何地方都可以拿到
一个组件需要改变全局状态
一个组件需要改变另一个组件的状态

localStorage 


# 数据的集中管理  全局通信管理 

# Redux 的设计思想很简单   单向数据流 (flux 数据只能从 state 流向 view )
1. Web 应用是一个状态机，视图与状态是一一对应的。
2. 所有的状态，保存在一个对象里面。 state 

# store 
Store 就是保存数据的地方，你可以把它看成一个容器。整个应用只能有一个 Store。
Redux 提供 createStore 这个函数，用来生成 Store。


# State 状态 -- 数据源  ==> store  
Store对象包含所有数据。如果想得到某个时点的数据，就要对 Store 生成快照。这种时点的数据集合，就叫做 State
Redux 规定， 一个 State 对应一个 View。只要 State 相同，View 就相同。你知道 State，就知道 View 是什么样，反之亦然

# action  
Action 就是 View 发出的通知，表示 State 应该要发生变化
Action 是一个对象。其中的type属性是必须的
{
    type:"xxxx"
}

改变 State 的唯一办法，就是使用 Action。它会运送数据到 Store

# Action Creator 
封装 action  以函数实现 action  动态的action 

# store.dispatch()是 View 发出 Action 的唯一方法。


# Reducer 计算 state 的函数 
Store 收到 Action 以后，必须给出一个新的 State，这样 View 才会发生变化。这种 State 的计算过程就叫做 Reducer
Reducer 是一个函数，它接受 Action 和当前 State 作为参数，返回一个新的 State

负责计算组件的初始化的state  初始化state 存储在 reducers


# store.subscribe()  刷新视图
Store 允许使用store.subscribe方法设置监听函数，一旦 State 发生变化，就自动执行这个函数


# store.getState() 获取store 里面的 state   
 

# redux 原理 
component----(dispatch)---> actions  ----> store ---action&state  ==> reducers  --- change state ---> store --- 刷新 component

1. 组件点击事件或者其他方法 发送 action
2. store 接收到 action 会把 oldState  和  action 发送给 reducers
3. reduers 匹配对应的 action 就会修改 对应的 state 
4. state 改变 store 触发监听函数 去修改 component视图
5. 单向数据流管理组件的状态 


# 
store.getState()   获取状态
store.dispatch()   发送action
store.subscribe()  订阅监听 state  刷新页面 




# 如何添加异步 

中间件就是一个函数，对store.dispatch方法进行了改造，在发出 Action 和执行 Reducer 这两步之间，添加了其他功能

使用中间件 就是要去改造 store.dispatch 

没有改造之前 store.dispatch()  必须只能接收一个对象(action)

经过中间件改造以后  store.dispatch()  不仅可以接收对象 还可以接收函数 (axios /ajax )


# Reducer：纯函数，只承担计算 State 的功能，不合适承担其他功能，也承担不了，因为理论上，纯函数不能进行读写操作

# View：与 State 一一对应，可以看作 State 的视觉层，也不合适承担其他功能。 

# Action：存放数据的对象，即消息的载体，只能被别人操作，自己不能进行任何操作