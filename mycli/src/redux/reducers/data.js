


const istate = {
    flag:true,
    mv:[],
    test:"TEST-Test - test",
    glist:[]
}
export const data  = (state=istate,action)=>{
    const {type,payload} = action ;

    switch(type){
        case "changeDataTest":
        return Object.assign(state,{test:payload })
        break;

        case "getGoodList":
            return Object.assign(state,{glist:payload })
        break;

        case "changeDataFlag":
            return Object.assign(state,{flag:!state.flag})
            break;
            
        default:
            return state;
            break;
        }
}