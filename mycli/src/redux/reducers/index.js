import { countDescType } from "../actions";

import {combineReducers} from "redux"
import { count } from './count';
import { word } from './word';
import { msg } from './msg';
import { city } from './city';
import { data } from './data';


export const reducers = combineReducers({   // 合并reducers 
    count:count,
    word:word,
    msg,
    city,
    data
})
























// //设置初始化state 
// const initialState = {
//     count:1000,
//     msg:"WH2201-daydayup",
//     city:"黄石大冶",
//     word:"React io so easy",
//     data:{
//         flag:true,
//         mv:[],
//         test:"TEST-Test - test"
//     }
// }


// Reducer 是一个函数，它接受 Action 和当前 State 作为参数，返回一个新的 State
// export const reducers = (state = initialState ,action )=>{
//     const {type,payload} = action // Action 是一个对象。其中的type属性是必须的
//     console.log(action)

//     switch(type){
//         case "COUNTADD":
//             state.count++;
//             console.log(state) 
//             return state;
//             break;

//         case countDescType:
//             state.count--;
//             return state;
//             break;
        
//         case "CHANGEACTION":
//             state.count+=payload;
//             // return state   返回的是旧数据 
//             return {...state}   // 返回的是新数据  
//             break;

//         case "changeCity":
//             // state.city = payload;
//             // return {...state,city:payload} 
//             return Object.assign(state,{city:payload})
//             break; 

//         case "changeMsg":
//             return Object.assign(state,{msg:payload})
//             break;

//         case "changeWord":
//             return Object.assign(state,{word:payload})
//             break;

//         case "changeDataTest":
//             return Object.assign(state,{data:{ ...state.data, test:payload }});
//             break;

//         case "changeDataFlag":
//             return Object.assign(state,{data:Object.assign(state.data,{flag:!state.data.flag } )});
//             break;

            
//         default:
//             return state;
//             break;
//     }

// }