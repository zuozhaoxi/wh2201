


import React, { Component } from 'react';
import MyCounter from './views/MyCounter';
import MyDemo from './views/MyDemo';
import store from './store'

// const state = store.getState()   // 第一次的数据  最开始的第一次的 state 


class ReduxIndex extends Component {
    render() {
        const state = store.getState()   // 每次获取最新的state 
        return (
            <div style={{padding:15}}>
                <h2>学习 react架构 -- redux  </h2>
                <hr/>
                <MyCounter state={state} />
                <hr/>
                <MyDemo/>
            </div>
        );
    }
}

export default ReduxIndex;
