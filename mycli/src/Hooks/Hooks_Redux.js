

// Hooks + Redux 
//1.  在纯函数组件 + redux 混合在一起  connect()
//2.  在纯函数组件 + mobx    observer()


// context 隔空传递
// Provider 使用了context 传送store 
// Hooks 接合 redux (store dispatch action reducer)
// Hooks 接合 mobx  (action state  项目使用)
// 单向数据流  action - state - view  (action - reducer - dispatch  - store  )



import React, { useContext, useEffect, useReducer, useRef } from 'react'
import MyBtn from '../components/MyBtn'
import { initialState, reducers } from './store/reducers'
import GolbalContext from "./store/store"
import Axios from './../utils/ajax';

const Hooks_Redux = () => {
  return (
    <div>
        <h2>Hooks + redux 实现简单的计数器 </h2>
        <MainBox/>
        {/* <Swiper.Item></Swiper.Item> */}
    </div>
  )
}

function MainBox(){
    const [state,dispatch] = useReducer(reducers,initialState)   // 得到state 和 dispatch 
    return (
        // 通过 context 传递 state 和 dispatch 
        <GolbalContext.Provider value={{state,dispatch}}>
            <LayoutView></LayoutView>
        </GolbalContext.Provider>
    )
}

function LayoutView(){
    const {state} = useContext(GolbalContext)
    return (
        <div>
            <h2>LayoutView - LayoutView - 主视图 </h2>
            <hr/>
           {state.get('flag') ?  <ChildOne/> : null } 
            <hr/>
            <ChildTwo/>
        </div>
    )
}

function ChildOne(){
    const {state,dispatch} = useContext(GolbalContext)
    console.log(state)
    useEffect(()=>{
        return ()=>{
            console.log("组件one 被销毁了")
        }
    },[])
    return(
        <div>
            <h2>ChildOne - ChildOne - ChildOne</h2>
            <h2>count -- {state.get("count")}</h2>
            <h2>city --- {state.get('city')} </h2>
            <h2>msg -- {state.get('msg')} </h2>
        </div>
    )
}

function ChildTwo(){
    const {state,dispatch} = useContext(GolbalContext)
    const text = useRef()
    return(
        <div>
            <h2>ChildTwo - ChildTwo - ChildTwo</h2>
            <MyBtn text="countADD" onClick={()=>dispatch({type:"countADD"})}  />
            <MyBtn text="changeCount" onClick={()=>dispatch({type:"changeCount",payload:188})}   />
            <p>
                <input type="text" ref={text}  value={state.get('msg')} onChange={()=>dispatch({type:"changeMsg",payload:text.current.value})} />
            </p>

            <MyBtn text="changeCity" onClick={()=>dispatch({type:"changeCity",payload:'美丽的新疆...'})}   />
            <MyBtn text="修改flag " onClick={()=>dispatch({type:"changeFlag"})}   />
            <hr/>
            <MvList/>
        </div>
    )
}

function MvList(){
    const {state,dispatch} = useContext(GolbalContext)
    const getList= async ()=>{
        let res = await Axios.post("http://localhost:3800/goodslist",{})
        console.log(res)
        dispatch({
            type:"changeList",
            payload:res.data.result 
        })
    }
    useEffect(  ()=>{
        getList()
        // Axios.post("http://localhost:3800/goodslist",{})
        // .then(res=>{
        //     console.log(res)
        //     dispatch({
        //         type:"changeList",
        //         payload:res.data.result 
        //     })
        // })
    },[])
    return (
        <div>
            <h2> MvList - MvList - MvList </h2>
            <div>
                {
                    state.get('list').map((l,i)=>{
                        return (
                            <p key={i} > {l.name} </p>
                        )
                    })
                }
            </div>
        </div>
    )
}
export default Hooks_Redux