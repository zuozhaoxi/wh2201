
// useCallback  vs    useMemo 
// useMemo 
import React , {useState,useRef,useEffect,useCallback,useMemo,Fragment}  from 'react'

export default function Hooks_5() {
    const [price, setPrice] = useState(0)
    const [name, setName] = useState('apple')
    const nameList = ['apple', 'peer', 'banana', 'lemon'] 
    
    // 普通函数 
    function getProductName() {
        console.log('没有被缓存的  getProductName触发 ')
        return name
    }
    // 只对name响应
    useEffect(() => {
        console.log('name effect 触发')
        getProductName()

        // componentDidUpdate 
    }, [name])
    
    // 只对price响应
    useEffect(() => {
        console.log('price effect 触发')
    }, [price])
  
    // memo化的getProductName函数   
    // useMemo就能解决之前的问题，怎么在DOM改变的时候，控制某些函数不被触发  缓存某个函数  减少这个函数不必要执行 
    const memo_getProductName = useMemo(() => {
        console.log('name memo 触发')
        return () => name  // 返回一个函数
    }, [name])

    const callback_getProductName = useCallback(() => {
        console.log('name callback 触发')
        return name;  // 返回一个函数
    }, [name])


    return (
        <div>
            <h2>Hooks - useMemo </h2>
            <Fragment>
                <p>name---- {name}</p>
                <p>price ---  {price}</p>
                <p>普通的name：{getProductName()}</p>
                <p>memo化的name ：{memo_getProductName ()}</p>
                <p>callback化的name ：{callback_getProductName ()}</p>

                <button onClick={() => setPrice(price+1)}>价钱+1</button>
                <button onClick={() => setName(nameList[Math.random() * nameList.length << 0])}>修改名字</button>
                <button onClick={()=>console.log("2201")}>onClick</button>
            </Fragment>
        </div>
    )
}


// connect(state,dispatch)(HooksFive)

//  useCallback  VS   useMemo  
//  useMemo是在DOM更新前触发的，就像官方所说的，类比生命周期就是shouldComponentUpdate   询问是否更新 做性能优化 

//  使用useMemo可以帮助我们优化性能，让react没必要执行不必要的函数
//  由于复杂数据类型的地址可能发生改变，于是传递给子组件的props也会发生变化，这样还是会执行不必要的函数，所以就用到了useMemo这个api
//  useCallback是useMemo的语法糖
//  useMemo > useCallback 