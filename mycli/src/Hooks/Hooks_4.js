// useCallback 
// useCallback 真正目的是在于缓存实例函数方法  减少不必要的渲染 componentWillUpdate && shouldComponentUpdate


import React , {useState,useRef, useCallback} from 'react'
import { useEffect } from 'react';
import MyBtn from '../components/MyBtn';

const Hooks_4 = () => {
    const [count,changeCount] = useState(0)
    const [msg,changeMsg] = useState("wh2201-daydayup")
    const text = useRef()
    const [list,setList] = useState(['Vue',"React","Node","原生JS","小程序"])
    
    useEffect(()=>{
      console.log("组件初始化 - onMounted ")
    },[])

    // 缓存函数 
    const changeList = useCallback(()=>{
        console.log('list- list -list ')
        return list.join(" & ")
    },[list])
    // 不写 [] 任何改变都会进来 没有缓存 
    // []   任何改变都不能进来  有缓存
    // [count]  某一个数据改变才进入return   有缓存  
 
    const changeListTwo = ()=>{
      console.log("1111222")
      return list.join(" % ")
    }

    return (
        <div>
            <h2>useCallback</h2>
            <p>  useCallback 真正目的是在于缓存实例函数方法  减少不必要的渲染  shouldComponentUpdate </p>
            <p>
              count -- {count}
            </p>
            <p>
              msg --- {msg}
            </p>
            <hr/>
            <MyBtn text="changeCount" onClick={()=>changeCount(count+1)} />
            <p>
                <input value={msg} type="text" ref={text} onChange={()=>changeMsg(text.current.value)} />
            </p>
            <h2>
              {list.join( " ~ ")}
            </h2>
            <MyBtn text="修改List" onClick={()=>setList(['TypeScript',"React-Native","flutter","乾坤微前端","微服务"])} />
            <hr/>
            <MyChild count={count} msg={msg} list={list} changeList={changeList} changeListTwo={changeListTwo}  ></MyChild>
        </div>
    )
}


function MyChild({
  count,
  msg,
  list,
  changeList,
  changeListTwo
}){
  return (
    <div>
        <h2> MyChild - MyChild - MyChild </h2>
        <h2>count == {count }</h2>
        <h2>list == {list.join(" * ") }</h2>
        <h2>msg == {msg }</h2>
        <h2> changeList  === { changeList()  }</h2>
        <h2> changeListTwo === {changeListTwo()}</h2>
    </div>
  )
}
export default Hooks_4