



import React , {useRef, useState,forwardRef, useImperativeHandle} from 'react'
import MyBtn from './../components/MyBtn';

const Hooks_1 = () => {
    // const [name,方法] = useState('zkl')
    let [count,useCount] = useState(1000)
    const [msg,setMsg] = useState('wh2201-dayadayup')
    const [word,setWord] = useState('React is so easy !!!')
    const [flag,setFlag] = useState(true)

    const inp = useRef()  // 绑定 ref 
    const child = useRef()  
    const changeWord = ()=>{
        console.log(inp)
        console.log(child)
        setWord(inp.current.value) 
    }
    return (
        <div>
            {
                flag && <div>
                    <h2>useState  -  useRef </h2>
                    <h2>count --- {count}</h2>
                    <h2>msg --- {msg} </h2>
                    <h2>word -- {word} </h2>
                </div>
            }
            <hr/>
            {/* <MyBtn text="修改count"  onClick={()=>useCount(count+1)}/> */}
            <MyBtn text="修改count"  onClick={()=>useCount(++count)}/>
            <MyBtn text="修改Msg"  onClick={()=>setMsg('wh2201-天天认真背面试题了吗')}/>
            <p> 
                <input type="text"  value={word}  onChange={e=> setWord(e.target.value)  } />
                <input type="text" ref={inp} value={word}  onChange={ changeWord  } />
            </p>
            <MyBtn text="修改flag "  onClick={()=>setFlag(!flag)}/>
            <MyChild ref={child} ></MyChild>
        </div>
    )
}

const MyChild = forwardRef((props,ref)=>{
    // bug  
    useImperativeHandle(ref,()=>{

    })
    return (
        <div>
            <h2>MyChild - MyChild - 子组件</h2>
        </div>
    )
})
// const MyChild = forwardRef(NChild)

export default Hooks_1