

import {fromJS} from 'immutable'

export const initialState = fromJS({
    count:2000,
    city:"大武汉",
    msg:"wh2201-daydayup ",
    flag:true,
    list:[]
})

export const reducers  = (state,action) =>{
    const {type,payload} = action;
    switch(type){
        case "countADD":
        return state.update("count",x=>x+1)
        break;

        case "changeCount":
        return state.update("count",x=>x+payload)
        break;

        case "changeMsg":
        return state.set('msg',payload)
        break;

        case "changeCity":
        return state.set('city',payload)
        break;

        case "changeFlag":
        return state.update('flag',x=>!x)
        break;

        case "changeList":
        return state.set('list',payload)
        break;

        default:
        return state;
        break;
    }
}