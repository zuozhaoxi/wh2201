

// 实现简单的留言板 
// useEffect  componentDidMount 
import React, { useState ,useEffect} from 'react'
import Axios from './../utils/ajax';
import { useRef } from 'react';
import MyBtn from '../components/MyBtn';


const Hooks_2 = () => {
    const [list,setList ] = useState([]);
    const title = useRef()   // current 
    const content = useRef()
    const getListAsync =  async ()=>{
        let res = await Axios.post("http://localhost:3800/api/getcomment",{})
        console.log(res)
        setList(res.data.result)
    }
    useEffect(()=>{
        console.log("纯函数 组件初始化了 -   componentDidMount")
        getListAsync()
    },[])

    const addComment = async ()=>{
        let data = {
            title:title.current.value,
            content:content.current.value,
        }
        let  res = await Axios.post("http://localhost:3800/api/addcomment",{
            data:data 
        })
        if(res.data.code==200){
            getListAsync()
            title.current.value=""
            content.current.value=""
        }
    }

    const delItemOne = async (l,i)=>{
        let res = await Axios.post("http://localhost:3800/api/delcomment",{
            data:{
                _id:l._id 
            }
        })
        if(res.data.code==200){
            var arr  = list.filter(item=>item._id !=l._id)
            setList([...arr])
        }
    }

    const setItemOne = async (l,i)=>{
        let content = window.prompt(l.title,l.content)
        if(content && content!=l.content){
            l.content = content;
            let res = await Axios.post("http://localhost:3800/api/setcomment",{
                data:l,
            })
            if(res.data.code==200){
                list.splice(i,1,l)
                setList([...list])
            }
        }
    }

    return (
        <div>
            <h2>Hooks 实现简单的 留言板 </h2>
            <div>
                <h2>展示数据</h2>
                <ul>
                    {
                        list.map((l,i)=>{
                            return (
                                <li key={i}>
                                    <p>序号: {i+1}</p>
                                    <p>标题: {l.title}</p>
                                    <p>内容: {l.content}</p>
                                    <p>操作: 
                                        <button onClick={()=>delItemOne(l,i)}>删除</button>
                                        <button onClick={()=>setItemOne(l,i)}>修改</button>
                                    </p>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
            <div>
                <h2>添加留言</h2>
                <p>
                    <input type="text" ref={title}  placeholder='标题'/>
                </p>
                <p>
                    <input type="text" ref={content} placeholder="内容" />
                </p>
                <p>
                    <MyBtn text="添加" onClick={addComment} />
                </p>
            </div>
        </div>
    )
}

export default Hooks_2