// useEffect 
// useEffect方法是在每次渲染之后执行，
// 可以理解为class写法中的 componentDidMount / componentDidUpdate / componentWillUnmount

// useEffect  第一个参数 是回调函数
// 第二个参数 不写 任何数据改变都能进来 
// 第二个参数 [] 不传参数 任何状态的改变都不会执行    [text]  只有你修改text 才会执行


import React, { useRef, useState } from 'react'
import { useEffect } from 'react';
import MyBtn from '../components/MyBtn';

const Hooks_3 = () => {
    const [count,setCount] = useState(1000)
    const [word,setWord] = useState('Are you OK?')
    const [flag,setFlag ] = useState(true)
    const text = useRef()

    useEffect(()=>{
        console.log('componentDidMount - 组件初始化  ')
    },[])   // 写 [ ]  任何数据改变都不能进来

    useEffect(()=>{
        console.log('componentDidMount + componentDidUpdate')   
    })  // 不写 []  任何数据改变都可以进来

    useEffect(()=>{
        console.log('只有 word 改变时候才会进来 ')   
    },[word])  // 只有 word 改变才会进来  watch 监听数据改变 
    

    const changeCount = ()=>{
        setCount(count+1)
    }

    return (
        <div>
            <h2> useEffect </h2>
            <h3> componentDidMount / componentDidUpdate / componentWillUnmount </h3>
            <h2>count --- {count }</h2>
            <h3>word --- {word}</h3>
            <hr/>
            <MyBtn text="changeCount" onClick={changeCount} />
            <p>
                <input type="text" value={word} ref={text} onChange={()=>setWord(text.current.value)}  />
            </p>
            <hr/>
            <MyBtn text="切换flag " onClick={()=>setFlag(!flag)} />
            {flag && <ChildOne></ChildOne>}
        </div>
    )
}

function ChildOne(){
    useEffect(()=>{
        console.log("这是一个组件初始化内容 - ChildOne ")

        return ()=>{
            console.log("componentWillUnmount  - 组件销毁 了")
        }
    },[])

    return (
        <div>
            <h2> ChildOne - ChildOne - ChildOne </h2>
        </div>
    )
}


export default Hooks_3