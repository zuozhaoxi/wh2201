



import React from 'react'
import Hooks_1 from './hooks_1'
import Hooks_2 from './hooks_2'
import Hooks_3 from './hooks_3'
import Hooks_4 from './Hooks_4'
import Hooks_5 from './Hooks_5'
import Hooks_Redux from './Hooks_Redux'

const HookIndex = () => {
    return (
        <div style={{padding:15}}>
            <h2>纯函数组件开发入门 </h2>
            <h2>纯函数组件 没有 this.props 没有this.state this.setState </h2>
            <h2>react 官方提供了几个 钩子函数 (Hooks) ====  (useState useRef useEffect useCallback useReudcer useContext)</h2>
            <hr/>
            {/* <Hooks_1/> */}
            {/* <Hooks_2/> */}
            {/* <Hooks_3/> */}
            {/* <Hooks_4/> */}
            {/* <Hooks_5/> */}
            <Hooks_Redux/>
        </div>
    )
}

export default HookIndex