// pages/todolist/todolist.js
import {http} from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    form:{},
    title:"",
    content:"",
    comments:[] ,  // 留言板 数据 
  },
  setOne(e){
    var item = e.target.dataset.item
    var index = e.target.dataset.index
    
    wx.showModal({
      title: item.title,
      content:item.content,
      editable:true,
      success:async (res)=> {
        if (res.confirm) {
          console.log(res)
          if(res.content!=item.content){
            // 修改数据 
            let result = await http({
              url:"http://116.62.225.234:3800/api/setcomment",
              data:{
                _id:item._id,
                content:res.content 
              },
              method:"post",
            })
            if(result.data.code==200){
              item.content = res.content 
              this.data.comments.splice(index,1,{...item})
              this.setData({
                comments:this.data.comments
              })
            }
          }
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  async delOne(e){
    console.log(e)
    var item = e.target.dataset.item
    var index = e.target.dataset.index
    console.log(item)
    let res = await http({
      url:"http://116.62.225.234:3800/api/delcomment",
      data:{
        _id:item._id 
      },
      method:"post",
    })
    if(res.data.code==200){
      this.data.comments.splice(index,1)
      this.setData({
        comments:this.data.comments
      })
    }
  },
  addcomment(){
    wx.showLoading({
      title: '提交中',
    })
    console.log(this.data.title)
    console.log(this.data.content)
    wx.request({
      url: 'http://116.62.225.234:3800/api/addcomment',
      method:"POST",
      data:{
        title:this.data.title,
        content:this.data.content 
      },
      success:res=>{
        // 
        wx.hideLoading()
        wx.showToast({title:"提交成功"})
        if(res.data.code==200){
          this.getComments()
          this.setData({
            title:"",
            content:""
          })
        }

      }
    })
  },
  getComments(){
    wx.showLoading({title:"获取中"})
    wx.request({
      method:"GET",
      url: 'http://116.62.225.234:3800/api/getcomment', //仅为示例，并非真实的接口地址
      data: {
        x: '',
        y: ''
      },
      // 请求头 headers 
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:res=>{
        console.log(res.data)  // axios 
        wx.hideLoading()
        wx.showToast({title:"成功"})
        this.setData({
          comments:res.data.result 
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载  mounted 
   */
  onLoad(options) {
    this.getComments()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})