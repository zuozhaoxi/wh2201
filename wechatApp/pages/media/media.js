// pages/media/media.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pic:"",
    items:[
      "打电话",
      "定位",
      "振动设备",
      "支付",
    ],
    latitude:"",
    longitude:"",
    src:''
  },
  openmap(){
    // 打开微信的内置地图 
    wx.openLocation({
      latitude:this.data.latitude,
      longitude:this.data.longitude,
      scale: 18,
      address:"金融港B18栋3楼",
      name:"千锋教育"
    })
  },  
  takePhoto(){
    const ctx = wx.createCameraContext()
    ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        this.setData({
          src: res.tempImagePath
        })
      }
    })
  },
  openAction(){
    wx.showActionSheet({
      itemList: this.data.items,
      success:(res)=>{
        console.log(res.tapIndex)
        if(res.tapIndex==0){
          wx.makePhoneCall({
            phoneNumber: '18086417861' //仅为示例，并非真实的电话号码
          })
        }else if(res.tapIndex==1){
          wx.getLocation({
            type: 'wgs84',
            success:(res)=>{
              const latitude = res.latitude
              const longitude = res.longitude
              const speed = res.speed
              const accuracy = res.accuracy
              this.setData({
                latitude,
                longitude
              })
            }
           })
        }else if(res.tapIndex==2){
          wx.vibrateLong()
        }else{
          wx.showToast({
            title:"支付成功"
          })
        }
      },
      fail (res) {
        console.log(res.errMsg)
      }
    })
  },
  // 扫码 
  startScan(){
      // 允许从相机和相册扫码
      wx.scanCode({
        success (res) {
          console.log(res)
        }
      })
  },

  // 云存储 
  uploadFile(filePath){
    wx.cloud.uploadFile({
      cloudPath: new Date().getTime() + "_WH2201_"+"photo.png",
      filePath: filePath, // 文件路径
      success: res => {
        // get resource ID
        console.log(res.fileID)
        this.setData({
          pic:res.fileID 
        })
      },
      fail: err => {
        // handle error
      }
    })
  },

  // JSSDK  
  startPhoto(){
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success:(res)=>{
        // tempFilePath可以作为 img 标签的 src 属性显示图片
        const tempFilePaths = res.tempFilePaths
       
        this.uploadFile(tempFilePaths[0])
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})