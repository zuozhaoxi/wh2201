// pages/home/home.js
const app = getApp()  // 获取到小程序全局唯一的 App 实例
const db = wx.cloud.database()
// Page 注册小程序中的一个页面。接受一个 Object 类型参数，其指定页面的初始数据、生命周期回调、事件处理函数
Page({
  /**
   * 页面的初始数据
   */
  data: {
      num:2000,
      msg:"WH2201-daydayup",
      isrc:"http://47.104.209.44/base/imgs/1.jpg",
      flag:true, 
      list:["Vue","react","Node","Typescript","小程序"],
      userInfo:app.globalData.userInfo,
      goods:[],
      page:1,
      pageSize:10,
      total:0
  },
  gotogood(e){
    console.log(e)
    var item = e.target.dataset.item 
    wx.navigateTo({
      url:"../good/good?goodId="+item._id+"&name="+item.name 
    })
  },
  getUser(){
      wx.cloud.callFunction({
        name:'wh2201_getuser',
        data:{
          id:2201,
          name:"zkl"
        }
      }).then(res=>{
        console.log(res)
      })
  },

  getGoods(){
    wx.showLoading({title:""})
    wx.cloud.callFunction({
      name:"wh2201_getgoods",
      data:{
        page:this.data.page,
        pageSize:this.data.pageSize 
      }
    }).then(res=>{
      console.log(res)
      wx.hideLoading({})
      this.setData({
        goods:res.result.data,
        total:res.result.total 
      })
    })
  },

  // 获取商品数据 
  getGoodList(){
    wx.showLoading({title:""})
    db.collection("WH01_goods")
    .where({})
    .get()
    .then(res=>{
      console.log(res)
      wx.hideLoading({})
      this.setData({
        goods:res.data 
      })
    })
  },
  clickOne(){
    console.log("clickOne - clickOne - 1 - 阻止冒泡")
  },
  clickTwo(){
    console.log("clickTwo - clickTwo - 2 - 不会阻止冒泡")
  },
  parentclick(){
    console.log("parentclick - parentclick - 父元素事件")
  },
  changeFlag(){
    this.setData({
      flag:!this.data.flag 
    })
  },
  changeMsg(e){
    console.log(e)
    this.setData({
      msg:e.detail.value 
    })
  },

  addAction(){
    this.setData({
      num:++this.data.num 
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options) // 小程序路由参数  query 
    // this.getGoodList()
    this.getUser()
    this.getGoods()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    wx.showLoading({
      title:"正在刷新"
    })

    setTimeout(()=>{
      this.setData({
        page:1,  
      })
      
       wx.cloud.callFunction({
        name:"wh2201_getgoods",
        data:{
          page:this.data.page,
          pageSize:this.data.pageSize 
        }
      }).then(res=>{
        wx.hideLoading()
        wx.showToast({title:"刷新成功"})
        wx.stopPullDownRefresh()
        this.setData({
          goods:res.result.data,
          total:res.result.total 
        })
      })
    },1500)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    console.log("页面滑到底部了 - 加载更多 ")
    this.setData({
      page:++this.data.page 
    })
    if(this.data.page > Math.ceil(this.data.total / this.data.pageSize)){
        wx.showToast({
          title:"数据见底了",
          icon:'none'
        })
    }else{
      wx.showLoading({title:""})
      wx.cloud.callFunction({
        name:"wh2201_getgoods",
        data:{
          page:this.data.page,
          pageSize:this.data.pageSize 
        }
      }).then(res=>{
        console.log(res)
        wx.hideLoading({})
        var list = this.data.goods.concat(res.result.data)  // 拼接数据 
        this.setData({
          goods:list,
          total:res.result.total 
        })
      })
    }
  },

  onPageScroll(value){
    console.log("页面正在滚动")
    console.log(value)
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    // const promise = new Promise(resolve => {
    //     setTimeout(() => {
    //       resolve({
    //         title: '自定义转发标题'
    //       })
    //     }, 2000)
    //   })
      return {
        title: 'wh2201-yyds',
        path: '/pages/home/home?id=123',
        imageUrl:"http://47.104.209.44/base/imgs/1.jpg" 
      }
  },

  onTabItemTap(item) {
    console.log(item.index)
    console.log(item.pagePath)
    console.log(item.text)
  }
})