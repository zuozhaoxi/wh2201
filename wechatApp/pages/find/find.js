// pages/find/find.js
import { http } from './../../utils/request';
function getRandomColor() {
  const rgb = []
  for (let i = 0; i < 3; ++i) {
    let color = Math.floor(Math.random() * 256).toString(16)
    color = color.length === 1 ? '0' + color : color
    rgb.push(color)
  }
  return '#' + rgb.join('')
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    percent:100,
    flag:true,
    banners:[],
    poster:"http://47.104.209.44/base/imgs/1.jpg",
    name:"纤夫的爱",
    author:"江小帅",
    src:"http://47.104.209.44/mp/Sugar1.mp3",
    vsrc:"http://47.104.209.44/mp/brkw1.mp4",
    danmuList:
      [{
        text: '第 1s 出现的弹幕',
        color: '#ff0000',
        time: 1
      }, {
        text: '第 3s 出现的弹幕',
        color: '#ff00ff',
        time: 3
    }],
    word:"666",
    toggle:true 
  },

  // 正在播放
  videotimeupdate(){
     if(!wx.getStorageSync('isplay') && this.data.toggle ){
        console.log("videotimeupdate")
        this.showAlert()
        this.setData({
            toggle:false 
        })
     }
  },

  // 视频开始播放了
  videoplay(){
    if(!wx.getStorageSync('isplay')){
      // 不能播放了 
      this.showAlert()
    }
  },

  showAlert(){
    this.videoCtx.pause()
    wx.showModal({
      title: '流量提示',
      content: '你正在使用流量观看视频,请确认.',
      cancelText:"暂停观看",
      cancelColor:"#333",
      confirmText:"继续观看",
      confirmColor:"#f50",
      success :(res)=> {
        if (res.confirm) {
          console.log('用户点击确定')
          wx.showToast({
            title:"请你继续观看",
            icon:'none'
          })
          this.videoCtx.play()
          wx.setStorageSync("isplay",true)
        } else if (res.cancel) {
          console.log('用户点击取消')
          wx.showToast({
            title:"你取消了观看",
            icon:'none'
          })
        }
      }
    })
  },

  choosevideo(){
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 60,
      camera: ['front', 'back'],
      success:(res)=>{
        this.setData({
          vsrc: res.tempFilePath
        })
      }
    })
  },  

  sendDanmu(){
    this.videoCtx.sendDanmu({
      text:this.data.word,
      color: getRandomColor()
    })
  },
  audioPlay(){
    this.audioCtx.play()   // 播放
  },
  audioPause(){
    this.audioCtx.pause()  // 暂停 
  },

  async getBanners(){
    let res = await http({
      url:"http://47.104.209.44:3333/banner",
    })
    console.log(res)
    this.setData({
      banners:res.data.banners 
    })
  },

  activeend(){
    this.setData({
      flag:false 
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getBanners()
  },


  drawCanvas(){
    // 使用 wx.createContext 获取绘图上下文 context
    var context = wx.createCanvasContext('firstCanvas')

    context.setStrokeStyle("#00ff00")
    context.setLineWidth(5)
    context.rect(0, 0, 200, 200)
    context.stroke()
    context.setStrokeStyle("#ff0000")
    context.setLineWidth(2)
    context.moveTo(160, 100)
    context.arc(100, 100, 60, 0, 2 * Math.PI, true)
    context.moveTo(140, 100)
    context.arc(100, 100, 40, 0, Math.PI, false)
    context.moveTo(85, 80)
    context.arc(80, 80, 5, 0, 2 * Math.PI, true)
    context.moveTo(125, 80)
    context.arc(120, 80, 5, 0, 2 * Math.PI, true)
    context.stroke()
    context.draw()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    this.drawCanvas()
    
    // wx.createInnerAudioContext()  
    this.audioCtx = wx.createAudioContext('myAudio')
    this.videoCtx = wx.createVideoContext("myVideo")

    wx.getNetworkType({
      success (res) {
        const networkType = res.networkType
        console.log(networkType)
        wx.setStorageSync("isplay",networkType=='wifi')
      }
    })

    wx.onNetworkStatusChange(function (res) {
      console.log(res.isConnected)
      console.log(res.networkType)
      wx.setStorageSync("isplay",res.networkType=='wifi')
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})