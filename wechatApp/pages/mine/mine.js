// pages/mine/mine.js
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: { 
    userlist:[],
    username:"",
    pass:"",
    userInfo:null 
  },

  logoutAction(){
    this.setData({
      userInfo:null 
    })
    wx.removeStorageSync('userInfo');
      
  },


  // 授权登录 
  authLogin(){
    // 1. 获取用户信息 
    // wx.getUserProfile
    // 2. 获取微信的login 的code
    // 3. 再去请求后台的登录接口 code 和用户基本信息传给后台  
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
          console.log(res)
          const userInfo  = {
            avatarUrl:res.userInfo.avatarUrl,
            nickName:res.userInfo.nickName,
          }
          wx.setStorageSync("userInfo",userInfo)

          wx.login({
            success:res=>{
              console.log(res)  // code 后台接口需要这个 code 去获取微信登录认证 
              // 3. 后台接口 

              wx.showLoading({})
              this.setData({
                userInfo
              })
              wx.showToast({
                title:"授权成功"
              })
              db.collection("WH01_user")
              .where({
                nickName:userInfo.nickName,
              })
              .get()
              .then(res=>{
                console.log(res)
                wx.hideLoading()
                if(res.data.length==0){
                  db.collection("WH01_user")
                  .add({
                    data:userInfo 
                  })
                  .then(res=>{
                    
                  })
                }
              })
              
            }
          })
      },
      fail:err=>{
        console.log(err)
        wx.showToast({
          icon:"error",
          title:"你拒绝授权"
        })
      }
    })
  },

  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getUser()
    if(wx.getStorageSync("userInfo")){
      this.setData({
        userInfo:wx.getStorageSync("userInfo")
      })
    }
  },  
  // 修改
  updateOne(e){
    var index = e.target.dataset.index;
    var item = e.target.dataset.item;
    console.log(item,index)
    wx.showModal({
      title: '请修改用户名',
      content:item.username ,
      editable:true,
      success:async (res)=> {
        if (res.confirm) {
          console.log(res)
          if(res.content!=item.username){
            // 修改数据 
            wx.showLoading({title:"操作中"})
            db.collection("WH01_user")
            .where({
              _id:item._id,
            })
            .update({
              data:{
                username:res.content 
              }
            })
            .then(result=>{
              wx.hideLoading({})
              item.username = res.content 
              this.data.userlist.splice(index,1,{...item})
              this.setData({
                userlist:this.data.userlist
              })
            })
          }
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // 删除
  deleteOne(e){
    var index = e.target.dataset.index;
    var item = e.target.dataset.item;
    console.log(item,index)
    wx.showLoading({title:"操作中"})
    db.collection("WH01_user")
    .where({
      _id:item._id 
    })
    .remove()
    .then(res=>{
      wx.hideLoading()
      this.data.userlist.splice(index,1)
      this.setData({
        userlist:this.data.userlist
      })
    })
  },  

  // 增删改查 
  // 查询 
  getUser(){
     wx.showLoading({title:"请求中"})
     db.collection("WH01_user")
     .where({

     })
     .get()
     .then(res=>{
      console.log(res)
      wx.hideLoading()
      this.setData({
        userlist:res.data 
      })
     })
     .catch(err=>{

     })
  },
  // 添加 
  addUser(){
    wx.showLoading({title:"请求中"})
    db.collection("WH01_user")
    .add({
      data:{
        username:this.data.username,
        pass:this.data.pass,
      }
    })
    .then(res=>{
     console.log(res)
     wx.hideLoading()
     this.setData({
      username:"",
      pass:""
     })
     this.getUser()
    })
    .catch(err=>{

    })
 },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})