// app.js
//  小程序启动的主文件
// App()  注册小程序。接受一个 Object 参数，其指定小程序的生命周期回调等
App({
  onLaunch(){
    console.log("小程序启动的主文件")
     // 云开发初始化     
      if (!wx.cloud) {
        console.error('请使用 2.2.3 或以上的基础库以使用云能力')
      } else {
        wx.cloud.init({
          // env 参数说明：
          //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
          env: 'nz1903-ozmt0',  
          // traceUser: true,
        })
      }
  },
  onShow(){
    console.log('App - onShow')
  },
  onHide(){
    console.log('App - onHide')
  },
  globalData:{
    userInfo:{
      username:'zuozuomu',
      age:18 
    }
  }
})
