// components/demo/demo.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    num:Number,
    msg:String 
  },

  created(){
    console.log('created')
  },
  attached(){
    console.log('attached')
  },
  ready(){
    console.log('ready')
  },
  moved(){
    console.log('moved')
  },
  detached(){
    console.log('detached')
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
