// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const db = cloud.database()
  const result = await db.collection("wh2114_users")
  .where({

  })
  .get()

  return {
    event,
    ...result,
    msg:'获取用户成功'
  }
}