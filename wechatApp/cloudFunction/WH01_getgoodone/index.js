// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const {goodId} = event;
  const result = await cloud.database().collection("WH01_goods")
  .where({
    _id:goodId 
  })
  .get()

  return {
    event,
    ...result,
    msg:"查询成功"
  }
}