// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const db = cloud.database()
  const {page,pageSize} = event;
  const count  = await db.collection("WH01_goods")
  .where({})
  .count() 

  const result = await db.collection("WH01_goods")
  .where({})
  .skip((page-1) * pageSize)
  .limit(pageSize)
  .get() 


  return {
    event,
    ...count,
    ...result,
    msg:"查询成功"
  }
}