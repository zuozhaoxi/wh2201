
import {createStore } from 'vuex'
import { Http } from './../api/index';

const store = createStore({
    state:{
        count:0,
        userInfo:null,
        collapse:false,  // false 展开  true 收起 
        xuekeList:[],
        banjiList:[] // 班级列表 
    },
    mutations:{
        changeCount(state,payload){
            state.count+= payload 
        },
        getuserinfo(state,payload){
            state.userInfo = payload;
        },
        changecollapse(state,payload){
            state.collapse = payload;
        },
        getXuekeList(state,payload){
            state.xuekeList = payload;
        },
        getBanjiList(state,payload){
            state.banjiList = payload;
        }
    },
    actions:{
        async getUserInfoAsync({commit},payload){
            let res = await Http.getuserinfo()
            commit("getuserinfo",res.result)
        },
        async getBanjiListAction({commit},payload={}){
            let res = await Http.banjilist(payload)
            commit("getBanjiList",res.result) 
        }
    },
    getters:{

    },
    modules:{

    }
})

export default store;