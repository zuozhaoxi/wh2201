

// mixins 设置公共组件选项  到时候可以混入到需要的组件内部  
// 全局混入 所有的组件都拿到这些组件选项 
import {mapState,mapMutations} from 'vuex'
import { reg } from './validate'
import {AnnoTypeList}  from './index'
export const mixins = {
    data(){
        return {
            vSrc:"http://47.104.209.44/mp/brkw.mp4",
            reg:reg,
            AnnoTypeList
        }
    },
    computed:{
        ...mapState(['count','userInfo','collapse','xuekeList','banjiList'])
    },
    methods:{
        ...mapMutations(['changeCount','changecollapse','getXuekeList'])
    },
    watch:{

    },
    mounted(){
        // console.log("组件初始化载入了...")
    }
}