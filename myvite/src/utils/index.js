

// 公共的JS模块

export const roleList = [
    {
        id:1,
        value:1,
        label:"学员",
        color:"#f50",
        role:1
    },
    {
        id:2,
        value:2,
        label:"讲师",
        color:"#123456",
        role:2
    },
    {
        id:3,
        value:3,
        label:"管理员",
        color:"deeepink",
        role:3
    }
]


export const AnnoTypeList = [
    {
        id:1,
        value:1,
        label:"普通公告",
        color:"#f50",
        role:1
    },
    {
        id:2,
        value:2,
        label:"紧急公告",
        color:"#123456",
        role:2
    },
    {
        id:3,
        value:3,
        label:"重要公告",
        color:"deeepink",
        role:3
    },
    {
        id:4,
        value:4,
        label:"置顶公告",
        color:"deeepink",
        role:4
    }
]