

// 配置路由 4.0 
import {createRouter , createWebHashHistory , createWebHistory} from 'vue-router'

// 1. 定义路由组件
import Guide from '@/views/Guide/Guide.vue'
import ErrorRoute from '@/views/Errors/route'
import MainRoute from '@/views/Mains/route'
import LoginRoute from '@/views/Logins/route'

// 2. 定义routes 路由配置选项
const routes = [
    {
        path:"/",
        redirect:{
            name:"login"
        }
    },
    {
        path:"/guide",
        name:'guide',
        component:Guide 
    },
    {
        path:'/login',
        name:"login",
        component:()=>import('@/views/Logins/Login.vue') // 路由懒加载 
    },
    ...ErrorRoute,
    ...MainRoute,
    ...LoginRoute,
    {
        path:"/:pathMatch(.*)",
        redirect:{
            name:'404'
        }
    }
]

// 3. 定义路由对象 
const router = createRouter({
    history:createWebHashHistory(),    // history hash 
    routes:routes 
}) 

// 4 暴露挂载到根实例 
export default router
