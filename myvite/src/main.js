import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import { mixins } from './utils/mixins';

console.log("Vue 项目启动的主文件 ")
const app = createApp(App)     // 根实例


// 1.0
import "@/styles/index.scss"

// 2.0 Element-plus 
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
app.use(ElementPlus)

// app.component('SearchInput', SearchInputComponent)   注册组件
// app.directive('focus', FocusDirective)    注册指令  
// app.use(LocalePlugin)     使用模块 全局声明一个模块  
// Vue.use(router)    
// 全局过滤器
app.config.globalProperties.$filters = {
    prefix(url) {
        if (url && url.startsWith('http')) {
            return url
        } else {
            url = `http://backend-api-01.newbee.ltd${url}`
        return url
        }
    },
    toyear(value) {
       if(!value){
           return value 
       }
       return "20"+value;
    }
}

import LoginByName from '@/components/LoginByName.vue'
import LoginByPhone from '@/components/LoginByPhone.vue'

// 3.0 全局注册组件 
app.component("LoginByName",LoginByName)
app.component("LoginByPhone",LoginByPhone)


app.mixin(mixins)   // 全局混入 
app.use(router)    // 路由
app.use(store)     // vuex 
app.mount('#app')
