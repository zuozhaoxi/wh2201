
import {defineAsyncComponent} from 'vue'
import RoleRoute from '@/views/Role/route'
import AnnoRoute from '@/views/Anno/route'
import AdviseRoute from '@/views/Advise/route'
import EduRoute from '@/views/Edu/route'
import ProjectRoute from '@/views/Project/route'
import GradeRoute from '@/views/Grade/route'





export default [
    {
        path:"/main",
        name:"main",
        component:()=>import('./index.vue'),
        redirect:"/home",
        children:[
            {
                path:"/home",
                name:"home",
                component:()=>import('../Home/Home.vue'),
            },
            {
                path:"/mine",
                name:"mine",
                component:()=>import('../Mine/Mine.vue'),
            },
            ...RoleRoute,
            ...AnnoRoute,
            ...AdviseRoute,
            ...EduRoute,
            ...ProjectRoute,
            ...GradeRoute
        ]
    }
]