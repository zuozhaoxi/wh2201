

export default [
    {
        path:"/register",
        name:'register',
        component:()=>import('./Register.vue')
    },
    {
        path:"/findpass",
        name:'findpass',
        component:()=>import('./FindPass.vue')
    }
]