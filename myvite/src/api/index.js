import {get,axios,post} from './request'

export const Http = {
    testApi:(data)=>get("/api/test",data),

    register:(data)=>post("/api/register",data),

    login:(data)=>post("/api/login",data),

    sentcaptcha:(data)=>post("/api/sentcaptcha",data),   // 发送验证码
 
    verifycaptcha:(data)=>post("/api/verifycaptcha",data),  // 校验验证码

    getuserinfo:(data)=>post("/api/getuserinfo",data),

    changepass:(data)=>post("/api/changepass",data),

    uploadfiles:(data)=>post("/api/uploadfiles",data),  // 上传文件 

    changeuserinfo:(data)=>post("/api/changeuserinfo",data), 

    findpass:(data)=>post("/api/findpass",data), 

    // 学科
    xuekeadd:(data)=>post("/api/xuekeadd",data), 

    xuekelist:(data)=>post("/api/xuekelist",data), 

    xuekedel:(data)=>post("/api/xuekedel",data), 

    xuekeset:(data)=>post("/api/xuekeset",data), 

    // 班级
    banjiadd:(data)=>post("/api/banjiadd",data), 

    banjilist:(data)=>post("/api/banjilist",data), 

    banjidel:(data)=>post("/api/banjidel",data), 

    banjiset:(data)=>post("/api/banjiset",data), 

    // 公告
    annoadd:(data)=>post("/api/annoadd",data), 

    annolist:(data)=>post("/api/annolist",data), 




}