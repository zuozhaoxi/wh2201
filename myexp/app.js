var createError = require('http-errors');    // http异常
var express = require('express');  // express 
var path = require('path');    // 处理路径 
var cookieParser = require('cookie-parser');  // cookie编译 
var logger = require('morgan');  // 日志记录

// 路由 
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var apiRouter = require('./routes/api')

var cors = require("cors") // 解决跨域
var app = express();   // 执行express 函数  得到express方法和属性

require("./db/connect")  // 链接数据库


app.use(cors({
  // 设置用户名和密码 秘钥  
}))
// view engine setup
app.set('views', path.join(__dirname, 'views'));    // 设置视图路径
app.set('view engine', 'ejs');   // 设置模板引擎  

app.use(logger('dev'));   // dev  开发日志   prod  生产 
app.use(express.json());  //  获取 POST 提交的参数  req.body 
app.use(express.urlencoded({ extended: false }));  // req.body 
app.use(cookieParser());   // 处理日志
app.use(express.static(path.join(__dirname, 'public')));  // express 的静态文件 目录  __dirname 根目录 = / = 绝对路径 

// 添加路由    路由 = 路由别名+路径地址  
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRouter);


// 捕捉异常 
// catch 404 and forward to error handler
// next  进入下一个中间件  下一步操作 下一步函数  
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
