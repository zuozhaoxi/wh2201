exports.findOneFromTable = async function({model,res,msg='查询数据成功',query={},field={},callback}){
    let result = await model.findOne(query,field).catch(err=>{
        console.log(err)
        res.json({
            code:500,
            msg:"服务器异常",
            error:err 
        })
    })

    if(callback){
        callback(result)
    }else{
        res.json({
            code:200,
            msg,
            result 
        })
    }
}

exports.updateOneFromTable = async function({model,res,msg='修改成功',query={},data={},callback}){
    let result = await model.updateMany(query,{
        $set:data 
    }).catch(err=>{
        console.log(err)
        res.json({
            code:500,
            msg:"服务器异常",
            error:err 
        })
    })

    if(callback){
        callback(result)
    }else{
        res.json({
            code:200,
            msg,
            result 
        })
    }
}

exports.removeOneFromTable = async function({model,res,msg='删除成功',query={},callback}){
    let result = await model.remove(query).catch(err=>{
        console.log(err)
        res.json({
            code:500,
            msg:"服务器异常",
            error:err 
        })
    })

    if(callback){
        callback(result)
    }else{
        res.json({
            code:200,
            msg,
            result 
        })
    }
}


exports.insertOneFromTable = async function({model,res,msg='插入成功',data ={},callback}){
    let result = await model.insertMany(data).catch(err=>{
        console.log(err)
        res.json({
            code:500,
            msg:"服务器异常",
            error:err 
        })
    })

    if(callback){
        callback(result)
    }else{
        res.json({
            code:200,
            msg,
            result 
        })
    }
}


// 查询
exports.FindAllFromTable = async function({model,res,msg='查询成功',skip=0,sort={}, limit=0,field={},query ={},callback}){
    let result = await model.find(query,field)
        .limit(limit)
        .sort(sort)
        .skip(skip)
        .catch(err=>{
        console.log(err)
        res.json({
            code:500,
            msg:"服务器异常",
            error:err 
        })
    })

    if(callback){
        callback(result)
    }else{
        res.json({
            code:200,
            msg,
            result 
        })
    }
}