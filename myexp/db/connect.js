

// 链接数据库
const mongoose = require("mongoose")
const hostname = "0.0.0.0"
// const hostname = "localhost"

const port = 27017
const dbName = "wh2201"
const user = "?"  // 暂时不用
const pwd = "?"

const db_conn_url = `mongodb://${hostname}:${port}/${dbName}`

mongoose.connect(db_conn_url,(err)=>{
    if(err){
        console.log("数据库链接失败")
        console.log(err)
        throw err;  // 终止程序执行  
    }else{
        console.log("数据库链接成功")
    }
})

const conn = mongoose.connection   // 链接对象 
conn.on("open",()=>{
    console.log("open - 数据库已经链接 ")
})
conn.on('connected',()=>{
    console.log('connected 链接了')
})
conn.on('disconnected',()=>{
    console.log('disconnected 断开链接了')
})
conn.on("error",(err)=>{
    console.log("数据库链接失败 " + err)
})

