

const mongoose = require("mongoose")
const Schema = mongoose.Schema

// 表  
// username
// phone
// nickname
// password
// id
// age 

// 表结构  
// Schema
const user_schema = new Schema({
    username:String,
    phone:String,
    nickname:String,
    password:String,
    id:Number,
    age:Number,
    time:Date, 
    role:Number,  // 1 学员  2. 讲师 3.管理员 
    avatar:String // 头像 
})
// Model  表模型  操作数据库 
// 模块暴露 
exports.user_model = mongoose.model('users',user_schema)  // users 表名 必须是复数  英语复数  

// exports.user_model = mongoose.model('user1',user_schema)  // user1 

// exports.user_model = mongoose.model('user',user_schema)  // user = users 

// exports.user_model = mongoose.model('city',user_schema)  // city = cities  


const good_schema = new Schema({
    "id": String,
    "name": String,
    "price": Number,
    "discount": Number,
    "img":String,
    "type": Object 
})

exports.good_model = mongoose.model("goods",good_schema)


const xueke_schema = new Schema({
    id: Number,
    name:String,
    value:String,
})

exports.xueke_model = mongoose.model("xuekes",xueke_schema)


const banji_schema = new Schema({
    banji_year:String,   // 2022
    banji_no:String,     // 01 
    banji_xueke:String,   // html5
    banji_text:String,     // HTML5大前端WH2201
    banji_value:String,   // html52201
})

exports.banji_model = mongoose.model("banjis",banji_schema)


const anno_schema = new Schema({
    time:Date,
    title:String,
    content:String,
    type:Number,
    userInfo:Object ,
    desc:String 
})

exports.anno_model = mongoose.model("annos",anno_schema)

const comment_schema = new Schema({
    time:Date,
    title:String,
    content:String,
})

exports.comment_model = mongoose.model("comments",comment_schema)


const like_schema = new Schema({
    goodId:String,
    phone:String, 
    good:Object 
})

exports.like_model = mongoose.model("likes",like_schema)


const shou_schema = new Schema({
    goodId:String,
    phone:String, 
    good:Object 
})

exports.shou_model = mongoose.model("shoues",shou_schema)


const cart_schema = new Schema({
    goodId:String,
    phone:String, 
    good:Object,
    count:Number,
    check:Boolean
})

exports.cart_model = mongoose.model("cartes",cart_schema)