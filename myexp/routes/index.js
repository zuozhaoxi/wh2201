var express = require('express');
var router = express.Router();
var {user_model, good_model } = require("../db/model")
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// router 路由对象
// get 请求方式 (get post all)
// req  request 请求对象   前端发送给服务器端的数据请求  
// res  response  响应对象  服务器返回给前端的数据响应  
// res.send 响应字符串  文本 
// res.json  返回json对象 
router.get("/login",(req,res)=>{  
    res.send("登录成功")
})

router.get("/register",(req,res)=>{
  res.send("注册成功")
})

router.post("/demo",(req,res)=>{
  res.send("demo - DEmo - DEmo ")
})

router.post("/todo",(req,res)=>{
  res.json({
      msg:"查询成功 - JSON数据",
      code:200,
      result:null,
      body:req.body,
      query:req.query 
  })
})

router.get("/list",(req,res)=>{
  res.json({
    msg:"list - list - list 列表数据",
    code:200,
    result:[],
    query:req.query 
  })
})

// 凡 冒号:  必 Params 
// 凡 问号?  必 Query  
router.get("/love/:name/:uid",(req,res)=>{
    res.json({
      code:200,
      msg:"520 - 快乐",
      result:null,
      params:req.params  
    })
})

// all = 所有请求方式  
router.all("/allway",(req,res)=>{
  res.json({
      msg:"all = 所有请求方式",
      code:200,
      result:{}
  })
})


router.all("/last",(req,res)=>{
  res.json({
    code:200,
    msg:"Last - Last - Last",
    result:{
    }
  })
})


// 增删改查 
// 添加
router.all("/useradd",(req,res)=>{
  var body = req.body;
  user_model.insertMany(body)
  .then(result=>{
    res.json({
      code:200,
      msg:'添加成功',
      result
    })
  })
  .catch(err=>{
    console.log(err)
    res.json({
      code:500,
      msg:'服务器异常',
      err,
    })
  })
})

// 查询 
router.all("/userlist",async (req,res)=>{
  try{
    let result = await user_model.find({},{});
    console.log(result)
    res.json({
      code:200,
      msg:'查询成功',
      result
    })
  }catch(err){
    console.log(err)
    res.json({
      code:500,
      msg:'服务器异常',
      err,
    })
  }
})

// 删除
router.all("/userdel",async (req,res)=>{
    var body = req.body 
    let result = await user_model.remove(body).catch(err=>{
      console.log(err)
      res.json({
        code:500,
        msg:'服务器异常',
        err,
      })
    })

    res.json({
      code:200,
      msg:'删除成功',
      result
    })
})

// 修改  $set $inc  $or 或语句  
router.all("/userupdate",async (req,res)=>{
  var body = req.body   // POST 请求 
  let result = await user_model.updateMany({
    _id:body._id
  },{
    $set:body 
  }).catch(err=>{
    res.json({
      code:500,
      msg:'服务器异常',
      err,
    })
  })

  res.json({
    code:200,
    msg:'修改成功',
    result
  })
})


// 商品列表
router.all("/goodslist",async (req,res)=>{
  console.log(req.body)
    let result = await good_model.find({},{})
    .limit(req.query.limit || 0 )
    .catch(err=>{
      res.json({
        code:500,
        msg:'服务器异常',
        err,
      })
    })
    res.json({
      code:200,
      msg:'查询商品成功',
      result
    })
})

router.all("/goodone/:gid",async (req,res)=>{
  let result = await good_model.findOne({
    _id:req.params.gid 
  },{}).catch(err=>{
    res.json({
      code:500,
      msg:'服务器异常',
      err,
    })
  })
  res.json({
    code:200,
    msg:'查询成功',
    result
  })
})


module.exports = router;
