var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.all("/login",(req,res)=>{
   res.json({
     code:200,
     msg:"请先登录",
     result:null
   })
})

router.all("/register",(req,res)=>{
  res.json({
    code:200,
    msg:"请先注册"
  })
})

router.all("/demo",(req,res)=>{
  res.json({
    msg:"Demo - Hello world "
  })
})


module.exports = router;
