import Vue from "vue"
import {mapState,mapMutations} from "vuex"
import {randomString,debounce} from "./index"

Vue.mixin({
    data(){
        return {
            headbg:{
                backgroundImage:'linear-gradient(-90deg, #ff5500, #d43c33)',
            },
            ptop:{
                paddingTop:(2*this.nav_height)+'px'
            },
            
        }
    },
    computed:{
        ...mapState(['nav_height','num','userInfo']),
    },
    methods:{
        ...mapMutations(['changeHeight','changeNum','changeUserInfo']),
        debounce,
        gotothispage(url){
            console.log(url)
            uni.navigateTo({
                url
            })
        }
    },  
    mounted(){
        
    },
})