
//判断当前日期为当年第几周
export const  getYearWeek = function () {
    var time = new Date();
    var a = time.getFullYear()
    var b = time.getMonth() + 1;
    var c = time.getDate()
    
    //date1是当前日期
    //date2是当年第一天
    //d是当前日期是今年第多少天
    //用d + 当前年的第一天的周差距的和在除以7就是本年第几周
    var date1 = new Date(a, parseInt(b) - 1, c), date2 = new Date(a, 0, 1),
        d = Math.round((date1.valueOf() - date2.valueOf()) / 86400000);   // 24 * 3600  * 7 * 1000 
    return Math.ceil((d + ((date2.getDay() + 1) - 1)) / 7);
};

/*
生成 64 位的随机字符串 
*/ 
export function randomString(len) {
　　len = len || 64;
　　var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
　　var maxPos = $chars.length;
　　var str = '';
　　for (let i = 0; i < len; i++) {
		str += $chars.charAt(Math.floor(Math.random() * maxPos));
	}
　  return str;
}


/**
 * @returns {string}
 */
export function createUniqueString() {
	// const timestamp = +new Date() + ''
	const randomNum = parseInt((1 + Math.random()) * 65536) + ''
	// return (+(randomNum + timestamp)).toString(32)
	return (randomNum + new Date().getMilliseconds())
}

/**
 * @param {Array} arr1
 * @param {Array} arr2
 * @returns {Array}
 */
export function diffArary(arr1, arr2) {
	arr1 = uniqueArr(arr1)
	arr2 = uniqueArr(arr2)
	return arr1.concat(arr2).filter(arg => !(arr1.includes(arg) && arr2.includes(arg)))
}

export function uniqueArr(arr) {
  return Array.from(new Set(arr))
}

//BlobUrl转blob数据  
export function objectURLToBlob(url, callback) {
	var http = new XMLHttpRequest()
	http.open("GET", url, true)
	http.responseType = "blob"
	http.onload = function(e) {
		if (this.status == 200 || this.status === 0) {
			callback(this.response)
		}
	}
	http.send()
}

export function getRanNum(){
	return 100 + Math.floor(Math.random()* 400);
}

export function addDate() {
	let nowDate = new Date();
	let date = {
		year: nowDate.getFullYear(),
		month: nowDate.getMonth() + 1,
		date: nowDate.getDate(),
	}
	return date.year + '-'+ date.month + '-' + date.date
}

//订阅消息
export function authMsg(tmplIds) {
	//#ifdef MP-WEIXIN
    var that = this
    uni.requestSubscribeMessage({
      tmplIds: tmplIds,
      success(res) {
        switch (res[res]) {
          case 'reject':
            break;
          case 'ban':
            uni.showToast({
              title: '订阅失败：被后台封禁',
              icon: 'none',
              duration: 2000
            })
            break;
          case 'accept':
            break;
        }
      },
      fail(res) {
        uni.showToast({
          title: '错误码：' + res.errCode + ' 请联系管理员',
          icon: 'none',
          duration: 2000
        })
      }
    })
	//#endif
  }


//使用注释获取详细平台
export function getPlatform(){
	// #ifdef APP-PLUS
	return "APP"
	// #endif
	// #ifdef APP-PLUS-NVUE
	return "App nvue"
	// #endif
	// #ifdef H5
	return "H5"
	// #endif
	// #ifdef MP-WEIXIN
	return "miniWeixin"
	// #endif
	// #ifdef MP-ALIPAY
	return "miniAlipay"
	// #endif
	// #ifdef MP-BAIDU
	return "miniBaidu"
	// #endif
	// #ifdef MP-TOUTIAO
	return "miniToutiao"
	// #endif
	// #ifdef MP-QQ
	return "miniQq"
	// #endif
	// #ifdef MP-360
	return "mini360"
	// #endif
}
/**
 * 检测当前环境是否是微信浏览器访问
 */
export function micromessenger() {
	let ua = window.navigator.userAgent.toLowerCase()
	if(ua.match(/MicroMessenger/i)){
		if (ua.match(/MicroMessenger/i)[0] == 'micromessenger') {
			return true
		} else {
			return false
		}
	}else{
		return false
	}
	
}


// 防抖
export const debounce = (fn, t) => {
	let delay = t || 1000;
	let timer;
	return function () {
	let args = arguments;
	if(timer) {
		clearTimeout(timer);
	}
	timer = setTimeout(() => {
			timer = null;
			fn.apply(this, args);
	  	}, delay);
	};
}