import Vue from "vue"
// import moment from "moment"

Vue.filter("number_format",(value,max,min)=>{
    if(!value) return value;  
    max = max || 2;
    min = min || 2
    value = value + '';
    return parseFloat(value).toLocaleString('en', {minimumFractionDigits: max,maximumFractionDigits:min});
})

Vue.filter("tonumber",(value)=>{
    if(!value) return value;  
    // max = max || 2;
    // min = min || 2;
    return parseFloat(value).toLocaleString();
})

Vue.filter("phone_format",(value)=>{
    if(!value) return value;  
    value = value.split('')
    var one = value.slice(0,3)
    var two = value.slice(7,11);
    return one.join('') + "****" + two.join('');
})
Vue.filter("num_foramt",(value,n)=>{
    if(!value) return value;  
    n = n || 2;
    return (value * 1).toFixed(n)
})

// Vue.filter("date_format",(value,type)=>{
//     if(!value) return value;  
//     type = type || "YYYY-MM-DD HH:mm"
//     return moment(value).format(type)
// })

Vue.filter("time_format",(value,type)=>{
    if(!value) return value;  
    type = type || "YYYY-MM-DD HH:mm"
    var time  = new Date(value)
    var year = time.getFullYear()
    var month = time.getMonth() + 1
    var day = time.getDate()
    var hour = time.getHours()  
    var min = time.getMinutes()
    month = month < 10  ? '0'+month : month ;
    day = day < 10  ? '0'+day : day ;
    hour = hour < 10  ? '0'+hour : hour ;
    min = min < 10  ? '0'+min : min ;
    return `${year}-${month}-${day} ${hour}:${min}`
})

Vue.filter("time_format1",(value,type)=>{
    if(!value) return value;  
    var time  = new Date(value)
    var year = time.getFullYear()
    var month = time.getMonth() + 1
    var day = time.getDate()
    var hour = time.getHours()  
    var min = time.getMinutes()
    month = month < 10  ? '0'+month : month ;
    day = day < 10  ? '0'+day : day ;
    hour = hour < 10  ? '0'+hour : hour ;
    min = min < 10  ? '0'+min : min ;
    return `${month}-${day} ${hour}:${min}`
})

Vue.directive("shuibowen",{
    inserted(el,binding){
        el.style.overflow = "hidden"
        el.style.position = "relative"

        el.addEventListener("click",function(e){
            var e = e || window.event 
            var x = e.clientX  - el.offsetLeft ;
            var y = e.clientY  - el.offsetTop;
            var left = el.offsetLeft
            var top = el.offsetTop
            console.log(x,y)
            // console.log(left,top)
            var span = document.createElement("span")
            span.style.pointerEvents = 'none'
            span.style.position = "absolute"
            span.style.left = x +"px"
            span.style.top  = y +"px"
            span.style.borderRadius="50%"
            span.style.background = "rgba(0,0,0,.5)"
            el.append(span)

            var width = 0;
            var height = 0;
            var opacity  = 1;
            var timer = setInterval(()=>{
              width+=10
              height+=10
              opacity -= 0.02 

              
              span.style.left = x - span.clientWidth/2 + 'px'
              span.style.top = y - span.clientHeight/2 + 'px'

              span.style.width  = width +"px"
              span.style.height  = height +"px"
              span.style.opacity  = opacity

              if(span.style.opacity <=0){
                  span.remove()
                  clearInterval(timer)
              }

            },10)
        })
    }
})