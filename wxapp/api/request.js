
export const http = function({
  url,
  method,
  data ,
  header
}){
  return new Promise((resolve,reject)=>{
    wx.showLoading({
      title: '提交中',
    })
    wx.request({
      url: url,
      method:method,
      data:data ,
      header,
      success:res=>{
        console.log(res)
        resolve(res)
      },
      fail:err=>{
        reject(err)
      },
      complete(){
        wx.hideLoading()
      }
    })
  })
}