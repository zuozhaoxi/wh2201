import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

const store = new Vuex.Store({
  strict: debug,
  state:{
      num:1000,
      nav_height:"",
      userInfo:null,
     
  },
  mutations:{
        changeUserInfo(state,payload){
            state.userInfo = payload;
        },
        changeNum(state,payload){
            state.num+=payload 
        },
        changeHeight(state,payload){
            state.nav_height = payload; 
        },
    }
})
export default store
