import App from './App'

// Vuex
import store from './store/index'
App.store = store

// mixins 
import "./utils/mixins"

// 全局的指令过滤器 
import "./utils/golbal"

//引入uview
import uView from "uview-ui";
Vue.use(uView);




// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif